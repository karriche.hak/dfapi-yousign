import { LightningElement, api, track, wire } from "lwc";
import { subscribe, unsubscribe } from "lightning/empApi";
import { NavigationMixin } from "lightning/navigation";
import { ShowToastEvent } from "lightning/platformShowToastEvent";

import YousignLogo from "@salesforce/resourceUrl/Yousign_Logo";
import YousignPopupTitle from "@salesforce/label/c.Yousign_LC_Title";
import YousignContactLabel from "@salesforce/label/c.Yousign_Contact_Lookup_Label";
import YousignLookupPlaceholder from "@salesforce/label/c.Yousign_LC_Lookup_Placeholder";
import YousignOrderContacts from "@salesforce/label/c.Yousign_Order_Contacts";
import YousignNoContactSelected from "@salesforce/label/c.Yousign_At_Least_One_Signer_Message";
import YousignBackButton from "@salesforce/label/c.Yousign_Back_Button";
import labelYousignNextButton from "@salesforce/label/c.Yousign_Next_Button";
import YousignSubmitButton from "@salesforce/label/c.Yousign_Submit_Button";
import YousignDraftButton from "@salesforce/label/c.Yousign_Draft_Button";
import YousignSubmittedMessage from "@salesforce/label/c.Yousign_Submitted_message";
import YousignWait from "@salesforce/label/c.Yousign_Wait";
import YousignFatalError from "@salesforce/label/c.Yousign_FatalError";
import YousignCancelButton from "@salesforce/label/c.Yousign_Cancel_Button";
import YousignGotoProcedure from "@salesforce/label/c.Yousign_GotoProcedure";

import { assert, normalizeBoolean, normalizeArray } from "c/utils";

import getSettingsForThisObject from "@salesforce/apex/YousignContactAttachmentController.getSettingsForThisObject";
import getYousignProcedure from "@salesforce/apex/YousignContactAttachmentController.getYousignProcedure";
import cancelProcedure from "@salesforce/apex/YousignContactAttachmentController.cancelProcedure";
import createOrEditProcedure from "@salesforce/apex/YousignContactAttachmentController.createOrEditProcedure";
import getAttachments from "@salesforce/apex/YousignPicklistController.getAttachments";
import searchForContacts from "@salesforce/apex/YousignLookupController.searchForContacts";
import getRelatedContact from "@salesforce/apex/YousignLookupController.getRelatedContact";
import getYousignSettings from "@salesforce/apex/YousignApiKeyComponentController.getYousignSettings";
import procedureSubmitted from "@salesforce/apex/YousignContactAttachmentController.procedureSubmitted";
import getYousignTemplates from "@salesforce/apex/YousignContactAttachmentController.getYousignTemplates";
import getYousignMembers from "@salesforce/apex/YousignContactAttachmentController.getYousignMembers";

import PROCEDURE_OBJECT from "@salesforce/schema/Yousign_Procedure__c";

import FIELD_DOCUMENT_ID from "@salesforce/schema/Yousign_Submitted_Document__c.Document_Id__c";
import FIELD_DOCUMENT_NAME from "@salesforce/schema/Yousign_Submitted_Document__c.Document_Name__c";
import FIELD_PROCEDURE_ORDERED from "@salesforce/schema/Yousign_Procedure__c.Ordered__c";
import FIELD_PROCEDURE_ID from "@salesforce/schema/Yousign_Procedure__c.Yousign_Procedure_Id__c";
import FIELD_PROCEDURE_STATUS from "@salesforce/schema/Yousign_Procedure__c.Status__c";
import FIELD_SIGNATORY_CONTACTID from "@salesforce/schema/Yousign_Signatory__c.Contact__c";
import FIELD_CONTACT_NAME from "@salesforce/schema/Contact.Name";

import FIELD_CONTENTDOCUMENT_ID from "@salesforce/schema/ContentDocumentLink.ContentDocumentId";
import FIELD_CONTENTDOCUMENT_TITLE from "@salesforce/schema/ContentDocumentLink.ContentDocument.Title";

const channelName = "/event/ysign__Yousign_Procedure_Event__e";

export default class yousignSubmitPopup extends NavigationMixin(
  LightningElement
) {
  @api recordId;
  @api objectApiName;

  @api
  get disableFileSelection() {
    return this.settings.disableFileSelection;
  }
  set disableFileSelection(newValue) {
    this.settings.disableFileSelection = normalizeBoolean(newValue);
    assert(
      (this.settings.selectAllFiles && this.settings.disableFileSelection) ||
        !this.settings.disableFileSelection,
      `<c-yousign-submit-popup> If "disableFileSelection" is set to true, you must enable the "selectAllFilesByDefault" option.`
    );
  }
  @api isSubmitted = false;

  youSignLogoRsrc = YousignLogo;
  @track files = [];
  @track selectedFiles = [];
  @track selectedContacts = [];
  @track disableContactReordering = true;
  @track contactErrors = [];
  @track isDraft = false;
  @track isCancellable = false;
  @track isLoading = false;

  @track allFieldsAvailable = true;
  @track procedure = {};
  @track configLoaded = false;
  @track connectedCalled = false;

  @track showProcedureButtonEnabled = false;
  @track returnProcedureId;
  @track yousignProcedureId;

  @track _iframeDisplayed = false;
  @track _iframeDomain;
  @track _iframeHeight;

  @track _iframeComponent;

  @track templateOptions;
  @track selectedTemplate;
  @track selectedTemplateOption;
  @track isTemplateAvailable = false;
  @track roleContactsAssignementList = [];

  @track contactRoleOptions;

  @track settings = {
    selectAllFiles: false,
    disableFileSelection: false,
    disableContactSelection: false,
    selectRelatedContact: false,
    contactFieldName: "",
    hasFileRules: false,
    minRequiredFiles: 0,
    minFileErrorMessage: "",
    draftByDefault: false,
    closePopupOnSubmit: true,
    closePopupOnResult: false,
    useYousignIntegration: false
  };

  // Hide Order Contact & Draft option.
  @track hideAdvancedOptions = true;

  subscription = {};
  pointerToWindowMessageEvt = {};

  get computedContactOrderingEnabled() {
    return !this.disableContactReordering;
  }

  get label() {
    return {
      popupTitle: YousignPopupTitle,
      contactLabel: YousignContactLabel,
      lookupPlaceholder: YousignLookupPlaceholder,
      orderContacts: YousignOrderContacts,
      noContactSelected: YousignNoContactSelected,
      backButton: YousignBackButton,
      nextButton: labelYousignNextButton,
      submitButton: YousignSubmitButton,
      draftButton: YousignDraftButton,
      cancelButton: YousignCancelButton,
      submittedMessage: YousignSubmittedMessage,
      showProcedureButton: YousignGotoProcedure,
      loading: YousignWait,
      fatalError: YousignFatalError
    };
  }

  @wire(getSettingsForThisObject, { objectApiName: "$objectApiName" })
  settingsWiredFct(result) {
    console.log("settingsWiredFct");
    if (!this.objectApiName) {
      return;
    }
    this.configLoaded = true;
    if (result.data) {
      this.settings = result.data;
      assert(
        (this.settings.selectAllFiles && this.settings.disableFileSelection) ||
          !this.settings.disableFileSelection,
        `<c-yousign-submit-popup> If "disableFileSelection" is set to true, you must enable the "selectAllFilesByDefault" option.`
      );
    }
    this.finalizeInit();
  }

  renderedCallback() {
    console.log("renderedCallback - START");
    this.assertRequiredAttributes();
    if (this._iframeComponent === undefined && this._iframeDisplayed) {
      console.log("iframe displayed");
      this._iframeComponent = this.template.querySelector("iframe");
      this._iframeComponent.onload = () => {
        this._iframeComponent.src = this._iframeComponent.src;
      };
    }
  }
  selectionTemplateChangeHandler(event) {
    console.log(
      "selectionTemplateChangeHandler - SlectedTemplate : " + event.target.value
    );
    this.selectedTemplate = event.target.value;
    if (this.selectedTemplate) {
      getYousignMembers({ procedureId: this.selectedTemplate })
        .then((data) => {
          if (data.length > 0) {
            this.contactRoleOptions = data;
          } else {
            this.contactRoleOptions = [];
          }
        })
        .catch((error) => {
          console.log("Error : templateOptions : " + JSON.stringify(error));
        });
    } else {
      this.contactRoleOptions = [];
    }
  }
  connectedCallback() {
    console.log("connectedCallback - START");
    this.isLoading = true;
    getYousignTemplates()
      .then((data) => {
        if (data.length > 0) {
          this.isTemplateAvailable = true;
          this.templateOptions = data;
        }
        this.isLoading = false;
      })
      .catch((error) => {
        this.isLoading = false;
        console.log("Error : templateOptions");
      });
    this.connectedCalled = true;
    console.log("connectedCallback - finalizeInit - START");
    this.finalizeInit();
    console.log("connectedCallback - finalizeInit - END");
    const _self = this;

    this.pointerToWindowMessageEvt = (event) => {
      this.handleIframeMessage(event);
    };

    window.addEventListener(
      "message",
      this.pointerToWindowMessageEvt.bind(this)
    );

    const messageCallback = function (response) {
      // console.log('New message received : ', JSON.stringify(response));
      console.log("connectedCallback - messageCallback - START");
      const data = response.data;
      if (!data) {
        return;
      }

      const {
        ysign__Status__c,
        ysign__Procedure_Id__c,
        ysign__Object_Id__c,
        ysign__Yousign_Procedure_Id__c
      } = data.payload;

      if (
        !ysign__Status__c ||
        !ysign__Procedure_Id__c ||
        !ysign__Object_Id__c
      ) {
        return;
      }

      if (ysign__Object_Id__c !== _self.recordId) {
        return;
      }

      _self.isLoading = false;

      _self.returnProcedureId = ysign__Procedure_Id__c;
      if (_self.objectApiName !== "ysign__" + PROCEDURE_OBJECT.objectApiName) {
        _self.showProcedureButtonEnabled = true;
      }

      if (ysign__Status__c === "Failed") {
        _self.dispatchMessage("error", _self.label.fatalError);
        return;
      }

      _self.dispatchMessage("success", _self.label.submittedMessage);

      if (_self.settings.closePopupOnResult) {
        if (
          _self.objectApiName ===
          "ysign__" + PROCEDURE_OBJECT.objectApiName
        ) {
          window.history.back();
        } else {
          _self.dispatchEvent(new CustomEvent("close"));
        }
        _self.handleRedirect(_self.recordId, "_self");
      }

      if (_self.settings.useYousignIntegration) {
        if (!ysign__Yousign_Procedure_Id__c) {
          _self.dispatchMessage("error", _self.label.fatalError);
          return;
        }
        _self.yousignProcedureId = ysign__Yousign_Procedure_Id__c;
        if (
          _self.objectApiName !==
          "ysign__" + PROCEDURE_OBJECT.objectApiName
        ) {
          const availableHeight = window.innerHeight || 1080;
          _self._iframeHeight = ((availableHeight * 90) / 100).toFixed(0);

          _self.dispatchEvent(
            new CustomEvent("resizerequest", {
              detail: {
                size: "large",
                height: _self._iframeHeight
              }
            })
          );
        }
        getYousignSettings()
          .then((result) => {
            if (result && result.ysign__Environment__c === "PRODUCTION") {
              _self._iframeDomain = "https://webapp.yousign.com";
            } else {
              _self._iframeDomain = "https://staging-app.yousign.com";
            }
            _self._iframeDisplayed = true;
          })
          .catch(() => {
            _self.dispatchMessage("error", _self.label.fatalError);
          });
      }
    };

    // Invoke subscribe method of empApi. Pass reference to messageCallback
    subscribe(channelName, -1, messageCallback).then((response) => {
      // Response contains the subscription information on successful subscribe call
      // console.log('Successfully subscribed to : ', JSON.stringify(response.channel));
      this.subscription = response;
    });
    console.log("connectedCallback - END");
  }

  disconnectedCallback() {
    console.log("disconnectedCallback - START");
    unsubscribe(this.subscription, (response) => {
      // console.log('unsubscribe() response: ', JSON.stringify(response));
      // Response is true for successful unsubscribe
    });
    window.removeEventListener("message", this.pointerToWindowMessageEvt);
    console.log("disconnectedCallback - END");
  }

  finalizeInit() {
    console.log("finalizeInit - START");
    if (!this.connectedCalled || !this.configLoaded) {
      // prevent load procedure or attachments without config.
      return;
    }

    if (this.objectApiName === "ysign__" + PROCEDURE_OBJECT.objectApiName) {
      getYousignProcedure({ procedureId: this.recordId })
        .then((result) => {
          this.procedure = result;
          this.initializeProcedure();
        })
        .catch((error) => {
          this.allFieldsAvailable = false;
          this.dispatchError(error);
        });
    } else {
      getAttachments({ objectId: this.recordId })
        .then((result) => {
          this.initializeAttachments(result);
          this.validateContactInput();
        })
        .catch((error) => {
          const fileSelectorElement = this.template.querySelector(
            "c-yousign-file-selector"
          );
          if (fileSelectorElement) {
            let message = this.label.fatalError;
            if (Array.isArray(message) && message.length) {
              message = error[0].message || this.label.fatalError;
            }
            fileSelectorElement.setTableError(message, []);
          }
        });

      if (
        this.settings.selectRelatedContact &&
        this.selectedContacts.length === 0
      ) {
        getRelatedContact({
          objectApiName: this.objectApiName,
          recordId: this.recordId,
          fieldApiName: this.settings.contactFieldName
        })
          .then((result) => {
            if (result) {
              this.selectedContacts.push(result);
              this.validateContactInput();
            }
          })
          .catch((error) => {
            this.dispatchError(error);
          });
      }
      this.isDraft = this.settings.draftByDefault;
    }
    console.log("finalizeInit - END");
  }

  get computedDisabledContactSelection() {
    return this.settings.disableContactSelection || this.isSubmitted;
  }

  get computedIframeUrl() {
    console.log("computedIframeUrl - START");
    return `${this._iframeDomain}${this.yousignProcedureId}/addMembers`;
  }

  get computedIframeStyle() {
    console.log("computedIframeStyle - START");
    return `width:100%;height:${this._iframeHeight}px;border:none;`;
  }

  get computedSubmitButtonLabel() {
    if (this.settings && this.settings.useYousignIntegration) {
      return this.label.nextButton;
    }
    return this.label.submitButton;
  }

  initializeProcedure() {
    const selectedDocuments = normalizeArray(
      this.procedure.ysign__Yousign_Subitted_Documents__r
    );
    this.selectedFiles = selectedDocuments.map(
      (selDoc) => selDoc["ysign__" + FIELD_DOCUMENT_ID.fieldApiName]
    );
    this.files = selectedDocuments.map((selDoc) => {
      const file = {
        ContentDocumentTitle:
          selDoc["ysign__" + FIELD_DOCUMENT_NAME.fieldApiName],
        isSelected: true,
        isDisabled: true
      };
      file[FIELD_CONTENTDOCUMENT_ID.fieldApiName] =
        selDoc["ysign__" + FIELD_DOCUMENT_ID.fieldApiName];
      return file;
    });
    const selectedContacts = normalizeArray(
      this.procedure.ysign__Yousign_Signatories__r
    );
    this.selectedContacts = selectedContacts.map((selContact) => {
      const currContact = selContact.ysign__Contact__r;
      return {
        id: selContact["ysign__" + FIELD_SIGNATORY_CONTACTID.fieldApiName],
        sObjectType: "Contact",
        icon: "standard:contact",
        title: currContact[FIELD_CONTACT_NAME.fieldApiName],
        subtitle: "",
        sObjectValues: currContact
      };
    });
    const currentStatus = this.procedure[
      "ysign__" + FIELD_PROCEDURE_STATUS.fieldApiName
    ];
    this.isCancellable =
      this.procedure["ysign__" + FIELD_PROCEDURE_ID.fieldApiName] !==
        undefined && currentStatus === "Submitted";
    this.disableContactReordering = !this.procedure[
      "ysign__" + FIELD_PROCEDURE_ORDERED.fieldApiName
    ];
    this.isSubmitted =
      currentStatus === "Created" ||
      currentStatus === "Submitted" ||
      currentStatus === "Pending" ||
      currentStatus === "Failed" ||
      currentStatus === "Rejected" ||
      currentStatus === "Signed" ||
      currentStatus === "Cancelled";
    this.isDraft = currentStatus === "Draft";
    const initIframe = currentStatus === "Created";
    if (initIframe) {
      const availableHeight = window.innerHeight || 1080;
      this._iframeHeight = ((availableHeight * 90) / 100).toFixed(0);

      this.yousignProcedureId = this.procedure[
        "ysign__" + FIELD_PROCEDURE_ID.fieldApiName
      ];
      getYousignSettings()
        .then((result) => {
          if (result && result.ysign__Environment__c === "PRODUCTION") {
            this._iframeDomain = "https://webapp.yousign.com";
          } else {
            this._iframeDomain = "https://staging-app.yousign.com";
          }
          this._iframeDisplayed = true;
        })
        .catch(() => {
          this.dispatchMessage("error", this.label.fatalError);
        });
    }

    this.validateContactInput();
  }

  initializeAttachments(result) {
    const _files = [];
    result.forEach((contentDocLink) => {
      const contentDocId =
        contentDocLink[FIELD_CONTENTDOCUMENT_ID.fieldApiName];
      let contentDocTitle = "";
      if (!contentDocId) {
        // continue if there is no content document id
        return;
      }
      if (this.settings.selectAllFiles) {
        this.selectedFiles.push(contentDocId);
      }
      const rowIndexIfSelected = this.selectedFiles.indexOf(contentDocId);
      const contentDocumentFields = FIELD_CONTENTDOCUMENT_TITLE.fieldApiName.split(
        "."
      );
      const contentDocument = contentDocLink[contentDocumentFields[0]];
      if (contentDocument && contentDocumentFields.length === 2) {
        contentDocTitle = contentDocument[contentDocumentFields[1]] || "";
      }
      const file = {
        ContentDocumentTitle: contentDocTitle,
        isSelected: rowIndexIfSelected >= 0,
        isDisabled: this.settings.disableFileSelection || this.isSubmitted
      };
      file[FIELD_CONTENTDOCUMENT_ID.fieldApiName] = contentDocId;
      // push the file in our temporary made array.
      _files.push(file);
    });
    this.files = _files;

    if (this.settings.hasFileRules && this.settings.minRequiredFiles > 0) {
      if (this.files.length < this.settings.minRequiredFiles) {
        this.dispatchError([{ message: this.settings.minFileErrorMessage }]);
        // Read Only !
        this.isSubmitted = true;
      }
    }
  }

  dispatchMessage(type, message) {
    if (this.objectApiName !== "ysign__" + PROCEDURE_OBJECT.objectApiName) {
      this.dispatchEvent(
        new ShowToastEvent({
          title: "",
          message: message,
          variant: String(type || "error").toLowerCase()
        })
      );
    } else {
      this.dispatchEvent(
        new CustomEvent("custommessage", {
          detail: {
            type: String(type || "ERROR").toUpperCase(),
            message: message
          }
        })
      );
    }
  }

  dispatchError(error) {
    let message = "Error";
    if (Array.isArray(error) && error.length) {
      message = error[0].message || "Error";
    } else if (error.body) {
      message = error.body.message || error.statusText || "Error";
    }
    this.dispatchMessage("error", message);
  }

  handleSearchContact(event) {
    const target = event.target;
    searchForContacts(event.detail)
      .then((results) => {
        target.setSearchResults(results);
      })
      .catch((error) => {
        this.dispatchError(error);
      });
  }

  handleRoleAssignement(event) {
    console.log(
      "handleRoleAssignement - yousignContactId : " +
        event.detail.yousignContactId
    );
    console.log(
      "handleRoleAssignement - contactId : " + event.detail.contactId
    );
    if (event.detail.yousignContactId) {
      this.roleContactsAssignementList.push(
        event.detail.contactId + "*" + event.detail.yousignContactId
      );
    }
  }

  handleSelectContact(event) {
    const selection = event.target.getSelection();
    this.selectedContacts = selection;
    this.validateContactInput();
  }

  handleContactOrderingToggle(event) {
    this.disableContactReordering = !event.detail.checked;
  }

  handleDraftToggle(event) {
    this.isDraft = event.detail.checked;
  }

  handleChangeContactOrder(event) {
    const { index, moveUp } = event.detail;
    if (index < 0 || index >= this.selectedContacts.length) {
      return;
    }
    if (moveUp && index > 0) {
      this.swapOptions(index, index - 1, this.selectedContacts);
    } else if (index < this.selectedContacts.length - 1) {
      this.swapOptions(index, index + 1, this.selectedContacts);
    }
  }

  handleDeleteContact(event) {
    if (this.computedDisabledContactSelection) {
      // Prevent contact deletion.
      return;
    }
    const { index } = event.detail;
    if (index < 0 || index >= this.selectedContacts.length) {
      return;
    }
    // using simple splice method doesn't work, because lwc doesn't detect changes.
    const tArray = [...this.selectedContacts];
    tArray.splice(index, 1);
    this.selectedContacts = tArray;
    this.validateContactInput();
  }

  swapOptions(i, j, array) {
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }

  validateContactInput() {
    const contactErrors = [];
    if (this.selectedContacts.length === 0) {
      contactErrors.push({
        id: "contact-err-1",
        message: this.label.noContactSelected
      });
    }
    this.contactErrors = contactErrors;
  }

  handleClickBack() {
    console.log("handleClickBack - START");
    if (this.objectApiName === "ysign__" + PROCEDURE_OBJECT.objectApiName) {
      this.handleCloseBackVF(this.recordId);
    } else {
      this.dispatchEvent(new CustomEvent("close"));
    }
  }

  handleClickSubmit() {
    console.log("handleClickSubmit - START");
    if (!this.isSubmitted) {
      const fileSelectorElement = this.template.querySelector(
        "c-yousign-file-selector"
      );
      fileSelectorElement.validateInput();
      if (!fileSelectorElement.hasAnyErrors) {
        return;
      }
      const _selectedFiles = fileSelectorElement.selectedFiles;
      if (
        this.settings.hasFileRules &&
        this.settings.minRequiredFiles > 0 &&
        _selectedFiles.length < this.settings.minRequiredFiles
      ) {
        fileSelectorElement.setTableError(
          this.settings.minFileErrorMessage,
          []
        );
        return;
      }
      this.selectedFiles = _selectedFiles;
      this.validateContactInput();
      if (this.contactErrors.length) {
        return;
      }
      if (!this.settings.closePopupOnSubmit) {
        // Set the loading state when we do not close the popup right now.
        this.isLoading = true;
      }
      createOrEditProcedure({
        selectedContactsList: this.selectedContacts.map(
          (selContact) => selContact.id
        ),
        selectedDocumentsList: this.selectedFiles,
        sourceObjectId: this.recordId,
        targetProcedureId: this.procedure ? this.procedure.Id : null,
        isOrdered: !this.disableContactReordering,
        isDraft: this.isDraft,
        reminderNbDays: 1,
        submitProcedureNotStarted: this.settings.useYousignIntegration,
        templateId: this.selectedTemplate,
        roleContactsAssignementList: this.roleContactsAssignementList
      });
      this.isSubmitted = true;
      this.isCancellable = false;
    }
    if (this.settings.closePopupOnSubmit) {
      if (this.objectApiName === "ysign__" + PROCEDURE_OBJECT.objectApiName) {
        window.history.back();
      } else {
        this.dispatchEvent(new CustomEvent("close"));
      }
    }
  }

  handleClickCancel() {
    if (this.isCancellable) {
      this.isLoading = true;
      cancelProcedure({
        procedureId: this.procedure.Id
      })
        .then(() => {
          this.isLoading = false;
          this.handleCloseBackVF(this.procedure.Id);
        })
        .catch(() => {
          this.isLoading = false;
          this.handleCloseBackVF(this.procedure.Id);
        });
    } else {
      this.handleCloseBackVF(this.procedure.Id);
    }
  }

  handleCloseBackVF(recId) {
    if (this._iframeDisplayed) {
      this._iframeDisplayed = false;
      // eslint-disable-next-line @lwc/lwc/no-async-operation
      setTimeout(() => {
        // there is 2 Edit pages in history (because of iframe ??)
        window.history.go(-3);
      }, 250);
      return;
    }
    location.href = "/" + recId;
    // this.handleRedirect(this.recordId, '_self');
  }

  handleClickShowProcedure() {
    this.handleRedirect(this.returnProcedureId, "_blank");
  }

  handleRedirect(recordId, target) {
    this[NavigationMixin.GenerateUrl]({
      type: "standard__recordPage",
      attributes: {
        recordId: recordId,
        actionName: "view"
      }
    })
      .then((url) => {
        window.open(url, target || "_blank");
      })
      .catch(() => {
        window.open("/" + recordId, target || "_blank");
      });
  }

  _gotProcedureCreatedMessage = false;

  handleIframeMessage(event) {
    if (event.origin !== this._iframeDomain) {
      return;
    }
    const data = event.data;
    console.log('### data : '+data);
    console.log('### gotProcedureCreatedMessage : '+this._gotProcedureCreatedMessage);
    if (data === "procedure.isCreated" && !this._gotProcedureCreatedMessage) {
      this._gotProcedureCreatedMessage = true;
      procedureSubmitted({
        procedureId: this.returnProcedureId || this.procedure.Id
      })
        .then(() => {
          if (
            this.objectApiName ===
            "ysign__" + PROCEDURE_OBJECT.objectApiName
          ) {
            this.handleCloseBackVF(this.recordId);
          } else {
            // eslint-disable-next-line @lwc/lwc/no-async-operation
            // setTimeout(() => {
            this.dispatchEvent(new CustomEvent("close"));
            // }, 250);
          }
        })
        .catch(() => {
          this.dispatchMessage("error", this.label.fatalError);
        });
    }
  }

  assertRequiredAttributes() {
    assert(
      !!this.recordId,
      `<c-yousign-submit-popup> Missing required "record-id" attribute.`
    );
    assert(
      !!this.objectApiName,
      `<c-yousign-submit-popup> Missing required "object-api-name" attribute.`
    );
  }
}