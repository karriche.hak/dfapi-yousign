import { LightningElement, api, track, wire } from 'lwc';

import getYousignProcedure from '@salesforce/apex/YousignContactAttachmentController.getYousignProcedure';
// import { getRecord } from 'lightning/uiRecordApi';

import getSettingsForThisObject from '@salesforce/apex/YousignContactAttachmentController.getSettingsForThisObject'

import PROCEDURE_OBJECT from '@salesforce/schema/Yousign_Procedure__c';

import FIELD_DOCUMENT_ID from '@salesforce/schema/Yousign_Submitted_Document__c.Document_Id__c';
import FIELD_SIGNATORY_CONTACTID from '@salesforce/schema/Yousign_Signatory__c.Contact__c';
import FIELD_CONTACT_NAME from '@salesforce/schema/Contact.Name';
// import FIELD_CONTACT_FIRSTNAME from '@salesforce/schema/Contact.FirstName';
// import FIELD_CONTACT_LASTNAME from '@salesforce/schema/Contact.LastName';
// import FIELD_CONTACT_EMAIL from '@salesforce/schema/Contact.Email';
// import FIELD_CONTACT_PHONE from '@salesforce/schema/Contact.Phone';
// import FIELD_CONTACT_MOBILEPHONE from '@salesforce/schema/Contact.MobilePhone';

//import FIELD_OBJSETTINGS_DISABLEFILESELECTION from '@salesforce/schema/Yousign_Object_Settings__c.Disable_file_selection__c';
//import FIELD_OBJSETTINGS_SELECTALLFILES from '@salesforce/schema/Yousign_Object_Settings__c.Select_all_files__c';

import { normalizeArray } from 'c/utils';

export default class yousignProcedure extends LightningElement {
    @api recordId
    @api objectApiName;

    @track allFieldsAvailable = true;
    @track procedure = {};
    @track selectedDocuments = [];
    @track selectedContacts = [];

    @track settings = {};

    @wire(getSettingsForThisObject, { objectApiName: '$objectApiName'})
    settingsWiredFct(result) {
        if (!this.objectApiName) {
            // not yet loaded.
            return;
        }
        this.isLoading = false;
        if(result.data) {
            this.settings = result.data;
        }
    }

    @track
    isLoading = true;

    get disableFileSelection() {
        return (!this.settings ? false : this.settings[FIELD_OBJSETTINGS_DISABLEFILESELECTION.fieldApiName]);
    }

    get selectAllFiles() {
        return (!this.settings ? false : this.settings[FIELD_OBJSETTINGS_SELECTALLFILES.fieldApiName]);
    }

    connectedCallback() {
        console.log(this.objectApiName);
        console.log(JSON.stringify(PROCEDURE_OBJECT));
        if (this.objectApiName === 'ysign__' + PROCEDURE_OBJECT.objectApiName) {
            console.log('1-- init Procedure LWC');
            getYousignProcedure({ procedureId: this.recordId })
                .then(result => {
                    console.log('GET OK');
                    this.procedure = result;
                    this.initialiseProcedure();
                })
                .catch(error => {
                    console.log('GET ERR');
                    this.allFieldsAvailable = false;
                    console.error(error);
                })
        }
    }

    initialiseProcedure() {
        console.log('2-- init Procedure LWC');
        const selectedDocuments = normalizeArray(this.procedure.ysign__Yousign_Subitted_Documents__r);
        this.selectedDocuments = selectedDocuments.map(selDoc => selDoc['ysign__' + FIELD_DOCUMENT_ID.fieldApiName]);
        const selectedContacts = normalizeArray(this.procedure.ysign__Yousign_Signatories__r);
        this.selectedContacts = selectedContacts.map(selContact => {
            const currContact = selContact.ysign__Contact__r;
            return {
                id: selContact['ysign__' + FIELD_SIGNATORY_CONTACTID.fieldApiName],
                sObjectType: 'Contact',
                icon: 'standard:contact',
                title: currContact[FIELD_CONTACT_NAME.fieldApiName],
                subtitle: '',
                sObjectValues: currContact
            }
        });
    }
}