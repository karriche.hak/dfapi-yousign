({
    init: function (cmp, evt, helper) {
        var myPageRef = cmp.get("v.pageReference");
        var members = myPageRef.state.c__members;
        var signatureUi = myPageRef.state.c__signatureUi;
        if (!members) {
            return;
        }
        cmp.set("v.members", members);
        cmp.set("v.loaded", false);
        if (signatureUi) {
            cmp.set("v.signatureUi", signatureUi);
        }
        var action = cmp.get('c.getYousignSettings');
        action.setCallback(this, function (response) {
            var responseState = response.getState();
            if (responseState === 'SUCCESS') {
                var settings = response.getReturnValue();
                var iframeUrl = 'https://staging-app.yousign.com/';
                if (settings && settings.Environment__c && settings.Environment__c === 'PRODUCTION') {
                    iframeUrl = 'https://webapp.yousign.com/';
                }
                iframeUrl += 'sign?members=/members/' + members;
                if (signatureUi) {
                    iframeUrl += '&signatureUi=/signature_uis/' + signatureUi;
                }
                cmp.set('v.iFrameUrl', iframeUrl);
                cmp.set("v.loaded", true);
            }
        });
    }
});