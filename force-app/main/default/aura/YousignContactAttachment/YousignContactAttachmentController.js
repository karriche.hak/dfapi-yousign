({
    back: function (component, event, helper) {
        if (component.get('v.sObjectName') ===
            'ysign__Yousign_Procedure__c') {
            window.history.back();
            return false;
        }
        else {
            component.set('v.showLWC', false);
            var closeEvent_1 = $A.get('e.force:closeQuickAction');
            setTimeout(function () {
                closeEvent_1.fire();
            }, 250);
        }
    },
    submitProcedure: function (component, event, helper) {
        var lookupRes = component.find('lookupId');
        component.set('v.containerId', 'lookupContainer');
        var attribute1 = component.get('v.containerId');
        lookupRes.hideLookupResult(attribute1);
        var recordsCount = component.get('v.selectedRecords').length;
        var filesCount = component.get('v.selectedFiles').length;
        if (recordsCount == 0 || filesCount == 0) {
            helper.markNotFilledFields(component, event, recordsCount, filesCount);
        }
        else {
            helper.createYousignProcedure(component, event, 'submit', $A.get('$Label.c.Yousign_Request_Success_Message'));
        }
    },
    cancelProcedure: function (component, event, helper) {
        var lookupRes = component.find('lookupId');
        component.set('v.containerId', 'lookupContainer');
        var attribute1 = component.get('v.containerId');
        lookupRes.hideLookupResult(attribute1);
        helper.cancelYousignProcedure(component, event);
        helper.getSelectedInfo(component, event);
    },
    draftProcedure: function (component, event, helper) {
        var lookupRes = component.find('lookupId');
        component.set('v.containerId', 'lookupContainer');
        var attribute1 = component.get('v.containerId');
        lookupRes.hideLookupResult(attribute1);
        var recordsCount = component.get('v.selectedRecords').length;
        var filesCount = component.get('v.selectedFiles').length;
        if (recordsCount == 0 || filesCount == 0) {
            helper.markNotFilledFields(component, event, recordsCount, filesCount);
        }
        else {
            helper.createYousignProcedure(component, event, 'draft', $A.get('$Label.c.Yousign_Draft_Success_Message'));
        }
    },
     init: function (component, event, helper) {
        component.set("v.isLoading", true);
        var action = component.get("c.isYouSignUserByEmail");
        action.setCallback(this, function (response) {
        	var state = response.getState();
            var value = response.getReturnValue();
            console.log("state : " + state);
            console.log("log value ok : " + JSON.stringify(value));
            if (value) {
                component.set("v.isLoading", false);
               	component.set('v.cssStyle', '<style>.slds-modal__container { width:70%; max-width: 75rem; min-width: 40rem }</style>');
            } else {
                var errors = response.getError();
                console.log("errors : " + JSON.stringify(errors));
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                           "message": "L'utilisateur n'existe pas côté YouSign",
                            "type": "error",
                            "title": "Erreur utilisateur"
                        });
                toastEvent.fire();
                component.set("v.showLWC", false);
                component.set("v.isLoading", false);
        }
        });
        $A.enqueueAction(action);
        
    },
    handleComponentMessage: function (component, event, helper) {
        var type = event.getParam('type');
        var message = event.getParam('message');
        helper.showMessage(component, event, type, message);
    },
    handleResizeRequest: function (component, event, helper) {
        var size = event.getParam('size');
        var height = event.getParam('height') || 650;
        if (size === 'large') {
            component.set('v.cssStyle', "<style>.slds-modal__container { width:90%; max-width: none;  min-width: 40rem } .slds-modal__content { height: " + height + "px !important; max-height: " + height + "px !important; }</style>");
        }
        else {
            component.set('v.cssStyle', "<style>.slds-modal__container { width:70%; max-width: 75rem; min-width: 40rem } .slds-modal__content { height: " + height + "px !important; max-height: " + height + "px !important; }</style>");
        }
    }
});