({
    showMessage: function (component, event, type, message) {
        if (component.get('v.sObjectName') !== 'ysign__Yousign_Procedure__c') {
            var toastEvent = $A.get('e.force:showToast');
            toastEvent.setParams({
                title: $A.get('$Label.c.Yousign_Request_Success_Title'),
                message: message,
                type: type
            });
            toastEvent.fire();
            this.reloadObject(component, event);
        }
        else {
            var showMessageEvent = $A.get('e.c:ShowMessage');
            if (showMessageEvent) {
                showMessageEvent.setParams({ type: type, message: message });
                showMessageEvent.fire();
            }
            else {
                var messageComponent = component.find("messageComponent");
                if (messageComponent) {
                    messageComponent.setMessage(type, message);
                }
            }
        }
    },
    cancelYousignProcedure: function (component, event) {
        var action = component.get('c.cancel');
        action.setParams({
            procedure: component.get('v.procedure'),
            contacts: component.get('v.selectedRecords'),
            selectedSignatories: component.get('v.selectedSignatories'),
            submittedDocuments: component.get('v.submittedDocuments')
        });
        this.sendCallback(component, event, action, $A.get('$Label.c.Yousign_Cancel_Success_Message'));
    },
    createYousignProcedure: function (component, event, functionName, successMessage) {
        var action = component.get('c.' + functionName);
        var contacts = component.get('v.selectedRecords');
        var contactsExterne = [];
        var lengthContacts = contacts.length;
        for (var i = lengthContacts - 1; i >= 0; i--) {
            if (contacts[i].Id.includes('externe')) {
                contactsExterne.push(contacts[i]);
                contacts.splice(i, 1);
            }
        }
        component.set('v.selectedRecords', contacts);
        action.setParams({
            contacts: component.get('v.selectedRecords'),
            contactsExterne: JSON.stringify(contactsExterne),
            contentDocumentIds: component.get('v.selectedFiles'),
            objectId: component.get('v.objectId'),
            procedure: component.get('v.procedure'),
            submittedDocuments: component.get('v.submittedDocuments'),
            selectedSignatories: component.get('v.selectedSignatories'),
            isOrdered: component.get('v.isOrdered'),
            reminderNbDays: component.get('v.reminderNbDays')
        });
        this.sendCallback(component, event, action, successMessage);
    },
    sendCallback: function (component, event, action, successMessage) {
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var storeResponse = response.getReturnValue();
                this.showMessage(component, event, 'INFO', successMessage);
                if (component.get('v.sObjectType') ===
                    'ysign__Yousign_Procedure__c') {
                    this.getSelectedInfo(component, event);
                }
            }
            else {
                console.log('ERROR = ');
                console.log(response.getError()[0]);
                this.showMessage(component, event, 'ERROR', response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
        if (component.get('v.currentTheme') == 'Theme3') {
            var objectId = component.get('v.objectId');
            window.location.href = '/' + objectId;
        }
    },
    getSelectedInfo: function (component, event) {
        var action = component.get('c.getYousignProcedure');
        action.setParams({
            procedureId: component.get('v.recordId')
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                component.set('v.areAllFieldsAccessible', true);
                this.setValues(component, event, response.getReturnValue());
            }
            else {
                component.set('v.areAllFieldsAccessible', false);
                this.showMessage(component, event, 'ERROR', response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
    },
    setIsNotOnlySelectedRecord: function (component, event, selectedRecords) {
        if (selectedRecords.length > 1) {
            component.set('v.isNotOnlyOneSelectedRecord', true);
        }
        else {
            component.set('v.isNotOnlyOneSelectedRecord', false);
        }
    },
    reloadObject: function (component, event) {
        var navEvt = $A.get('e.force:navigateToSObject');
        navEvt.setParams({
            isredirect: 'true',
            recordId: component.get('v.objectId'),
            slideDevName: 'detail'
        });
        navEvt.fire();
    },
    setValues: function (component, event, storeResponse) {
        component.set('v.procedure', storeResponse);
        component.set('v.submittedDocuments', storeResponse.ysign__Yousign_Submitted_Documents__r);
        component.set('v.objectId', storeResponse.ysign__ObjectReference__c);
        component.set('v.submittedDocument', storeResponse.ysign__Yousign_Submitted_Documents__r[0]);
        component.set('v.selectedSignatories', storeResponse.ysign__Yousign_Signatories__r);
        component.set('v.isOrdered', storeResponse.ysign__Ordered__c);
        component.set('v.reminderNbDays', storeResponse.ysign__ReminderNbDays__c);
        if (component.get('v.reminderNbDays') != null) {
            component.set('v.reminder', true);
        }
        if (component.get('v.procedure').ysign__Yousign_Procedure_Id__c !=
            null &&
            storeResponse.ysign__Status__c == 'Submitted') {
            component.set('v.isCancelable', true);
        }
        else {
            component.set('v.isCancelable', false);
        }
        this.setIsNotOnlySelectedRecord(component, event, component.get('v.selectedSignatories'));
        if (storeResponse.ysign__Status__c == 'Submitted' ||
            storeResponse.ysign__Status__c == 'Pending' ||
            storeResponse.ysign__Status__c == 'Failed' ||
            storeResponse.ysign__Status__c == 'Rejected' ||
            storeResponse.ysign__Status__c == 'Signed' ||
            storeResponse.ysign__Status__c == 'Cancelled') {
            component.set('v.isSubmitted', true);
        }
        else {
            component.set('v.isSubmitted', false);
        }
        var newSelectedRecords = [];
        var tempContact;
        for (var i = 0; i < storeResponse.ysign__Yousign_Signatories__r.length; i++) {
            tempContact =
                storeResponse.ysign__Yousign_Signatories__r[i]
                    .ysign__Contact__r;
            if (tempContact.MobilePhone == null) {
                tempContact.MobilePhone = tempContact.Phone;
            }
            newSelectedRecords.push(tempContact);
        }
        var newSelectedFiles = [];
        for (var i = 0; i < storeResponse.ysign__Yousign_Submitted_Documents__r.length; i++) {
            newSelectedFiles.push(storeResponse.ysign__Yousign_Submitted_Documents__r[i]
                .ysign__Document_Id__c);
        }
        component.set('v.selectedFiles', newSelectedFiles);
        component.set('v.selectedRecords', newSelectedRecords);
    }
});