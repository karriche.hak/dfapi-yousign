public with sharing class YousignContactAttachmentController {
    @AuraEnabled
    public static void createOrEditProcedure(
        List<Id> selectedContactsList,
        List<Id> selectedDocumentsList,
        Id sourceObjectId,
        Id targetProcedureId,
        Boolean isOrdered,
        Boolean isDraft,
        Integer reminderNbDays,
        Boolean submitProcedureNotStarted,
        String templateId,
        List<String> roleContactsAssignementList
    ) {
        try {
            Set<Id> selectedDocuments = new Set<Id>(selectedDocumentsList);
            Set<Id> selectedContacts = new Set<Id>(selectedContactsList);
            YousignConnectionService.checkFilesWeight(selectedDocuments);
            List<Yousign_Submitted_Document__c> ysSubmittedDocuments = null;
            List<Yousign_Submitted_Document__c> currentDocuments = null;
            List<Yousign_Submitted_Document__c> oldDocuments = null;
            List<Yousign_Signatory__c> oldSelectedSignatories = null;
            Yousign_Procedure__c procedure = null;
            
            Map<String, String> roleContactsAssignement = new Map<String, String>();
            for (String roleAssignement : roleContactsAssignementList) {
                roleContactsAssignement.put(
                    roleAssignement.split('\\*')[0],
                    roleAssignement.split('\\*')[1] == 'na'
                    ? ''
                    : roleAssignement.split('\\*')[1]
                );
            }
            if (targetProcedureId != null) {
                // In case we are editing the procedure, check FLS
                DMLManager dmlManager = new DMLManager();
                List<Schema.SObjectField> yousignSubmittedDocumentFields = new List<Schema.SObjectField>{
                    Yousign_Submitted_Document__c.Yousign_Procedure__c,
                        Yousign_Submitted_Document__c.Document_Id__c
                        };
                            List<Schema.SObjectField> yousignSignatoryFields = new List<Schema.SObjectField>{
                                Yousign_Signatory__c.Contact__c,
                                    Yousign_Signatory__c.Order__c,
                                    Yousign_Signatory__c.Yousign_Procedure__c
                                    };
                                        DMLManager.FLSCheckResultKeeper yousignSubmittedDocumentFLSCheckResult = dmlManager.getFLSCheckResult(
                                            yousignSubmittedDocumentFields,
                                            Yousign_Submitted_Document__c.getSObjectType()
                                        );
                DMLManager.FLSCheckResultKeeper yousignSignatoryFLSCheckResult = dmlManager.getFLSCheckResult(
                    yousignSignatoryFields,
                    Yousign_Signatory__c.getSObjectType()
                );
                if (
                    !yousignSubmittedDocumentFLSCheckResult.areAllSelectedFieldsAvailable
                ) {
                    throw new AuraHandledException(
                        yousignSubmittedDocumentFLSCheckResult.errorText
                    );
                }
                if (!yousignSignatoryFLSCheckResult.areAllSelectedFieldsAvailable) {
                    throw new AuraHandledException(
                        yousignSignatoryFLSCheckResult.errorText
                    );
                }
                
                procedure = new Yousign_Procedure__c(Id = targetProcedureId);
                
                oldDocuments = [
                    SELECT Id, Document_Id__c, Yousign_Procedure__c
                    FROM Yousign_Submitted_Document__c
                    WHERE Yousign_Procedure__c = :targetProcedureId
                ];
                oldSelectedSignatories = [
                    SELECT Id, Contact__c, Order__c, Yousign_Procedure__c
                    FROM Yousign_Signatory__c
                    WHERE Yousign_Procedure__c = :targetProcedureId
                ];
                ysSubmittedDocuments = getSelectedYousignSubmittedDocuments(
                    selectedDocuments,
                    oldDocuments
                );
            } else {
                ysSubmittedDocuments = getSelectedYousignSubmittedDocuments(
                    selectedDocuments,
                    new List<Yousign_Submitted_Document__c>()
                );
            }
            
            currentDocuments = makeSubmittedDocumentsWithTitles(
                selectedDocuments,
                ysSubmittedDocuments
            );
            
            List<Yousign_Signatory__c> selectedSignatories = new List<Yousign_Signatory__c>();
            for (Id contactId : selectedContacts) {
                String yousignRoleId;
                if (roleContactsAssignement.get(contactId) != null) {
                    selectedSignatories.add(
                        new Yousign_Signatory__c(
                            Contact__c = contactId,
                            ysign__TemplateRole__c = roleContactsAssignement.get(contactId)
                        )
                    );
                } else {
                    selectedSignatories.add(
                        new Yousign_Signatory__c(Contact__c = contactId)
                    );
                }
            }
            if (isOrdered) {
                selectedSignatories = fillOrderFieldForSignatories(selectedSignatories);
            }
            
            if (targetProcedureId == null) {
                procedure = new Yousign_Procedure__c();
                procedure.Name =
                    UserInfo.getName() +
                    ' - ' +
                    DateTime.now()
                    .addSeconds(
                        UserInfo.getTimeZone().getOffset(DateTime.now()) / 1000
                    );
            }
            procedure.Ordered__c = isOrdered;
            procedure.ReminderNbDays__c = String.valueOf(reminderNbDays);
            procedure.Template_id__c=templateId;
            String procedureStatus = (isDraft) ? 'Draft' : 'Pending';
            YousignConnectionService.createYousignProcedure(
                sourceObjectId,
                procedure,
                currentDocuments,
                selectedSignatories,
                procedureStatus,
                (isDraft) ? null : DateTime.now()
            );
            if (targetProcedureId != null) {
                List<Yousign_Signatory__c> selectedSignatoriesNoExt = YousignConnectionService.splitFilterYousignSignatory(
                    selectedSignatories,
                    'noExt'
                );
                processDeletedSignatories(
                    selectedSignatoriesNoExt,
                    oldSelectedSignatories
                );
                processDeletedDocuments(currentDocuments, oldDocuments);
            }
            if (!isDraft) {
                List<Yousign_Signatory__c> selectedSignatoriesExt = YousignConnectionService.splitFilterYousignSignatory(
                    selectedSignatories,
                    'Ext'
                );
                if (String.isBlank(templateId)) {
                    System.enqueueJob(
                        new YousignSubmitSignProcess(
                            procedure,
                            selectedSignatoriesExt,
                            '',
                            submitProcedureNotStarted
                        )
                    );
                } else {
                    System.enqueueJob(
                        new YousignSubmitDuplicateProcess(
                            procedure,
                            selectedSignatoriesExt,
                            '',
                            submitProcedureNotStarted,
                            templateId,
                            false
                        )
                    );
                }
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static void procedureSubmitted(Id procedureId) {
        ysign__Yousign_Procedure__c procedure = getYousignProcedure(procedureId);
        if (procedure != null && procedure.ysign__Status__c == 'Created') {
            ysign__Yousign_Procedure__c updatedProcedure = new ysign__Yousign_Procedure__c(
                Id = procedureId,
                ysign__Status__c = 'Submitted'
            );
            update updatedProcedure;
        }
    }
    
    @AuraEnabled
    public static List<Map<String, String>> getYousignTemplates() {
        YousignApiService yss = new YousignApiService();
        YousignApiService.TemplateListSubmission ts = new YousignApiService.TemplateListSubmission();
        yss.getTemplates(ts);
        List<Map<String, String>> listTemplates = new List<Map<String, String>>();
        for (YousignApiModel.ProcedureOutput po : ts.listProcedureOutput) {
            Map<String, String> values = new Map<String, String>{
                'label' => po.name,
                    'value' => po.id
                    };
                        listTemplates.add(values);
        }
        return listTemplates;
    }
    
    @AuraEnabled
    public static List<Map<String, String>> getYousignMembers(
        String procedureId
    ) {
        YousignApiService yss = new YousignApiService();
        YousignApiService.ProcedureReadSubmission prs = new YousignApiService.ProcedureReadSubmission(
            procedureId
        );
        yss.getProcedure(prs);
        List<Map<String, String>> listMembers = new List<Map<String, String>>();
        if (
            prs.procedureOutput.members != null &&
            prs.procedureOutput.members.size() > 0
        ) {
            for (YousignApiModel.MemberOutput member : prs.procedureOutput.members) {
                System.debug('#### MemberOutput : ' + member);
                Map<String, String> values = new Map<String, String>{
                    'label' => member.firstName +
                        ' ' +
                        member.lastName,
                        'value' => member.id
                        };
                            listMembers.add(values);
            }
        }
        System.debug('### listMembers : ' + listMembers);
        return listMembers;
    }
    
    @AuraEnabled
    public static void cancelProcedure(Id procedureId) {
        Yousign_Procedure__c procedure = getYousignProcedure(procedureId);
        
        if (procedure == null) {
            return;
        }
        
        YousignApiService service = getEnvironment();
        YousignApiService.CancellationSubmission cancellationSubmission = new YousignApiService.CancellationSubmission(
            procedure
        );
        YousignConnectionService.sendCancelSubmission(
            service,
            cancellationSubmission
        );
        YousignConnectionService.cancelProcedure(
            procedure,
            procedure.ysign__Yousign_Signatories__r,
            procedure.ysign__Yousign_Subitted_Documents__r
        );
    }
    
    @AuraEnabled(Cacheable=true)
    public static YousignSettingsWrapper getSettingsForThisObject(
        String objectApiName
    ) {
        if (String.isBlank(objectApiName)) {
            return null;
        }
        
        // ysign__Object__c is Unique, so it should return 1 item or 0
        ysign__Yousign_Object_Settings__mdt[] settings = [
            SELECT
            Id,
            MasterLabel,
            ysign__Object__c,
            ysign__Object__r.QualifiedApiName,
            ysign__Disable_file_selection__c,
            ysign__Select_all_files__c,
            ysign__Autoselect_contact__c,
            ysign__Contact_Field__c,
            ysign__Contact_Field__r.QualifiedApiName,
            ysign__Disable_contact_selection__c,
            ysign__Has_files_rules__c,
            ysign__Min_required_files__c,
            ysign__Draft_by_default__c,
            ysign__Close_popup_on_submit__c,
            ysign__Close_popup_on_result__c,
            ysign__Use_Yousign_Integration__c
            FROM ysign__Yousign_Object_Settings__mdt
            WHERE ysign__Object__r.QualifiedApiName = :objectApiName
        ];
        
        if (settings.isEmpty()) {
            return null;
        }
        
        YousignSettingsWrapper result = new YousignSettingsWrapper(settings.get(0));
        return result;
    }
    
    private static void processDeletedSignatories(
        List<Yousign_Signatory__c> signatoriesToKeep,
        List<Yousign_Signatory__c> oldSignatories
    ) {
        Set<Yousign_Signatory__c> signatories = new Set<Yousign_Signatory__c>(
            signatoriesToKeep
        );
        List<Yousign_Signatory__c> signatoriesToDelete = new List<Yousign_Signatory__c>();
        for (Yousign_Signatory__c signatory : oldSignatories) {
            if (!signatories.contains(signatory)) {
                signatoriesToDelete.add(signatory);
            }
        }
        new DMLManager().deleteAsUser(signatoriesToDelete);
    }
    
    private static void processDeletedDocuments(
        List<Yousign_Submitted_Document__c> selectedDocuments,
        List<Yousign_Submitted_Document__c> documents
    ) {
        Set<Yousign_Submitted_Document__c> selectedDocumentsSet = new Set<Yousign_Submitted_Document__c>(
            selectedDocuments
        );
        List<Yousign_Submitted_Document__c> submittedDocumentsToDelete = new List<Yousign_Submitted_Document__c>();
        for (Yousign_Submitted_Document__c document : documents) {
            if (!selectedDocumentsSet.contains(document)) {
                submittedDocumentsToDelete.add(document);
            }
        }
        new DMLManager().deleteAsUser(submittedDocumentsToDelete);
    }
    
    private static List<Yousign_Submitted_Document__c> makeSubmittedDocumentsWithTitles(
        Set<Id> contentDocumentIds,
        List<Yousign_Submitted_Document__c> selectedDocuments
    ) {
        List<Schema.SObjectField> contentDocumentFields = new List<Schema.SObjectField>{
            ContentDocument.Id,
                ContentDocument.Title
                };
                    DMLManager.FLSCheckResultKeeper contentDocumentFLSCheckResult = new DMLManager()
                    .getFLSCheckResult(
                        contentDocumentFields,
                        ContentDocument.getSObjectType()
                    );
        if (!contentDocumentFLSCheckResult.areAllSelectedFieldsAvailable) {
            throw new AuraHandledException(contentDocumentFLSCheckResult.errorText);
        }
        List<ContentDocument> contentDocuments = [
            SELECT Id, Title
            FROM ContentDocument
            WHERE Id IN :contentDocumentIds
        ];
        
        Map<Id, ContentDocument> contentDocumentsByIds = new Map<Id, ContentDocument>(
            contentDocuments
        );
        for (Yousign_Submitted_Document__c selectedDocument : selectedDocuments) {
            selectedDocument.Document_Name__c = contentDocumentsByIds.get(
                selectedDocument.Document_Id__c
            )
                .Title;
        }
        return selectedDocuments;
    }
    
    private static List<Yousign_Submitted_Document__c> getSelectedYousignSubmittedDocuments(
        Set<Id> contentDocumentIds,
        List<Yousign_Submitted_Document__c> documents
    ) {
        List<Yousign_Submitted_Document__c> selectedDocuments = new List<Yousign_Submitted_Document__c>();
        if (documents.isEmpty()) {
            for (Id documentId : contentDocumentIds) {
                Yousign_Submitted_Document__c selectedDocument = new Yousign_Submitted_Document__c();
                selectedDocument.Document_Id__c = documentId;
                selectedDocuments.add(selectedDocument);
            }
        } else {
            for (Id documentId : contentDocumentIds) {
                Boolean check = true;
                for (Yousign_Submitted_Document__c document : documents) {
                    if (documentId == document.Document_Id__c) {
                        selectedDocuments.add(document);
                        check = false;
                        break;
                    }
                }
                if (check) {
                    Yousign_Submitted_Document__c selectedDocument = new Yousign_Submitted_Document__c();
                    selectedDocument.Document_Id__c = documentId;
                    selectedDocuments.add(selectedDocument);
                }
            }
        }
        return selectedDocuments;
    }
    
    private static List<Yousign_Signatory__c> fillOrderFieldForSignatories(
        List<Yousign_Signatory__c> signatories
    ) {
        List<Yousign_Signatory__c> signatoriesWithOrder = new List<Yousign_Signatory__c>();
        for (Integer i = 0; i < signatories.size(); i++) {
            signatories.get(i).Order__c = i + 1;
            signatoriesWithOrder.add(signatories.get(i));
        }
        return signatoriesWithOrder;
    }
    
    private static List<Yousign_Signatory__c> makeSignatoryItems(
        List<Contact> contacts,
        List<Yousign_Signatory__c> selectedSignatories
    ) {
        Set<Yousign_Signatory__c> signatoriesToKeep = new Set<Yousign_Signatory__c>();
        for (Contact contact : contacts) {
            for (Yousign_Signatory__c signatory : selectedSignatories) {
                if (signatory.Contact__c == contact.Id) {
                    signatoriesToKeep.add(signatory);
                }
            }
        }
        Set<Contact> contactsToDelete = new Set<Contact>();
        for (Contact contact : contacts) {
            for (Yousign_Signatory__c signatory : signatoriesToKeep) {
                if (contact.Id == signatory.Contact__c) {
                    contactsToDelete.add(Contact);
                }
            }
        }
        Set<Contact> incommingContacts = new Set<Contact>(contacts);
        incommingContacts.removeAll(contactsToDelete);
        for (Contact contact : incommingContacts) {
            Yousign_Signatory__c newSignatory = new Yousign_Signatory__c();
            newSignatory.Contact__c = contact.Id;
            signatoriesToKeep.add(newSignatory);
        }
        List<Yousign_Signatory__c> signatoriesToReturn = new List<Yousign_Signatory__c>();
        for (Contact contact : contacts) {
            for (Yousign_Signatory__c signatory : signatoriesToKeep) {
                if (contact.Id == signatory.Contact__c) {
                    signatoriesToReturn.add(signatory);
                }
            }
        }
        return signatoriesToReturn;
    }
    
    @AuraEnabled
    public static Yousign_Procedure__c getYousignProcedure(Id procedureId) {
        DMLManager dmlManager = new DMLManager();
        List<Schema.SObjectField> yousignProcedureFields = new List<Schema.SObjectField>{
            Yousign_Procedure__c.ObjectReference__c,
                Yousign_Procedure__c.Status__c,
                Yousign_Procedure__c.Ordered__c,
                Yousign_Procedure__c.ReminderNbDays__c,
                Yousign_Procedure__c.Yousign_Procedure_Id__c
                };
                    List<Schema.SObjectField> contactFields = new List<Schema.SObjectField>{
                        Contact.Phone,
                            Contact.MobilePhone,
                            Contact.Email,
                            Contact.FirstName,
                            Contact.LastName
                            };
                                List<Schema.SObjectField> yousignSignatoryFields = new List<Schema.SObjectField>{
                                    Yousign_Signatory__c.Contact__c,
                                        Yousign_Signatory__c.Role__c,
                                        Yousign_Signatory__c.Order__c,
                                        Yousign_Signatory__c.Status__c,
                                        Yousign_Signatory__c.Yousign_Procedure__c
                                        };
                                            List<Schema.SObjectField> yousignSubmittedDocumnetFields = new List<Schema.SObjectField>{
                                                Yousign_Submitted_Document__c.Yousign_Procedure__c,
                                                    Yousign_Submitted_Document__c.Document_Id__c
                                                    };
                                                        DMLManager.FLSCheckResultKeeper yousignProcedureFLSCheckResult = dmlManager.getFLSCheckResult(
                                                            yousignProcedureFields,
                                                            Yousign_Procedure__c.getSObjectType()
                                                        );
        DMLManager.FLSCheckResultKeeper contactFLSCheckResult = dmlManager.getFLSCheckResult(
            contactFields,
            Contact.getSObjectType()
        );
        DMLManager.FLSCheckResultKeeper yousignSignatoryFLSCheckResult = dmlManager.getFLSCheckResult(
            yousignSignatoryFields,
            Yousign_Signatory__c.getSObjectType()
        );
        DMLManager.FLSCheckResultKeeper yousignSubmittedDocumentFLSCheckResult = dmlManager.getFLSCheckResult(
            yousignSubmittedDocumnetFields,
            Yousign_Submitted_Document__c.getSObjectType()
        );
        if (!yousignProcedureFLSCheckResult.areAllSelectedFieldsAvailable) {
            throw new AuraHandledException(yousignProcedureFLSCheckResult.errorText);
        }
        if (!contactFLSCheckResult.areAllSelectedFieldsAvailable) {
            throw new AuraHandledException(contactFLSCheckResult.errorText);
        }
        if (!yousignSignatoryFLSCheckResult.areAllSelectedFieldsAvailable) {
            throw new AuraHandledException(yousignSignatoryFLSCheckResult.errorText);
        }
        if (!yousignSubmittedDocumentFLSCheckResult.areAllSelectedFieldsAvailable) {
            throw new AuraHandledException(
                yousignSubmittedDocumentFLSCheckResult.errorText
            );
        }
        
        List<Yousign_Procedure__c> procedures = [
            SELECT
            Id,
            ObjectReference__c,
            Ordered__c,
            ReminderNbDays__c,
            Status__c,
            Yousign_Procedure_Id__c,
            (
                SELECT
                Id,
                Contact__c,
                Role__c,
                Order__c,
                Status__c,
                Yousign_Procedure__c,
                Contact__r.Name,
                Contact__r.Phone,
                Contact__r.MobilePhone,
                Contact__r.Email,
                Contact__r.FirstName,
                Contact__r.LastName
                FROM Yousign_Signatories__r
                ORDER BY Order__c ASC
            ),
            (
                SELECT Id, Yousign_Procedure__c, Document_Id__c, Document_Name__c
                FROM Yousign_Subitted_Documents__r
            )
            FROM Yousign_Procedure__c
            WHERE Id = :procedureId
        ];
        return procedures.isEmpty() ? null : procedures.get(0);
    }
    
    public static YousignApiService getEnvironment() {
        YousignApiService service;
        Yousign_setting__c settings = Yousign_setting__c.getOrgDefaults();
        if (settings != null && !String.isEmpty(settings.Environment__c)) {
            if (settings.Environment__c == 'PRODUCTION') {
                service = new YousignApiService(
                    YousignApiService.Environment.PRODUCTION
                );
            } else {
                service = new YousignApiService(YousignApiService.Environment.STAGING);
            }
        } else {
            service = new YousignApiService(YousignApiService.Environment.STAGING);
        }
        
        return service;
    }
    
    @future(callout=true)
    public static void updateUserWorkspace(String userId){
        User activeUser = [
            SELECT Email,ysign__WorkspaceName__c,ysign__WorkspaceId__c
            FROM User
            WHERE Id =: userId
            LIMIT 1
        ];
        if(activeUser.ysign__WorkspaceName__c!=null && !String.isEmpty(activeUser.ysign__WorkspaceName__c)){
            String userWorkspaceName = activeUser.ysign__WorkspaceName__c;
            String userWorkspaceId ;
            String apiKey = Yousign_setting__c.getOrgDefaults().Api_key__c;
            String environment = Yousign_setting__c.getOrgDefaults().Environment__c;
            String youSignEndPoint;
            if (environment == 'PRODUCTION') {
                youSignEndPoint = '';
            } else {
                youSignEndPoint = 'staging-';
            }
            HTTP http = new HTTP();
            HTTPRequest workspaceReq = new HTTPRequest();
            HttpResponse workspaceResp = new HttpResponse();
            workspaceReq.setMethod('GET');
            workspaceReq.setHeader('Content-Type', 'application/json');
            workspaceReq.setHeader('Authorization', 'Bearer ' + apiKey);
            workspaceReq.setEndpoint(
                'https://' +
                youSignEndPoint +
                'api.yousign.com' +
                '/workspaces' 
            );
            workspaceResp = http.send(workspaceReq);
            System.debug('## workspaceResp ' +workspaceResp.getBody());
            if(workspaceResp.getStatusCode() == 200){
                try{
                    List<WRP001_Workspace> lWorkspaces =  (List<WRP001_Workspace>)JSON.deserialize(workspaceResp.getBody(), List<WRP001_Workspace>.class);
                    for(WRP001_Workspace workspace : lWorkspaces){
                        if(workspace.name ==  userWorkspaceName){
                            userWorkspaceId = workspace.id;
                        } 
                    }
                }catch(Exception e){
                    userWorkspaceId = null;
                }
            }
            activeUser.ysign__WorkspaceId__c = userWorkspaceId;
            update activeUser;
        }
    }
    @AuraEnabled
    public static Boolean isYouSignUserByEmail() {
        String userName = UserInfo.getUserName();
        User activeUser = [
            SELECT Email,ysign__WorkspaceId__c
            FROM User
            WHERE Username = :userName
            LIMIT 1
        ];
        String userEmail = activeUser.Email;
        String userWorkspaceId=activeUser.ysign__WorkspaceId__c ;
        String apiKey = Yousign_setting__c.getOrgDefaults().Api_key__c;
        String environment = Yousign_setting__c.getOrgDefaults().Environment__c;
        String youSignEndPoint;
        
        if (environment == 'PRODUCTION') {
            youSignEndPoint = '';
        } else {
            youSignEndPoint = 'staging-';
        }
        
        HTTP h = new HTTP();
        HTTPRequest req = new HTTPRequest();
        HttpResponse resp = new HttpResponse();
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization', 'Bearer ' + apiKey);
        if(userWorkspaceId!=null && !String.isEmpty(userWorkspaceId)){
            req.setHeader('workspace', userWorkspaceId);
        }
        req.setEndpoint(
            'https://' +
            youSignEndPoint +
            'api.yousign.com' +
            '/users?email=' +
            userEmail
        );
        resp = h.send(req);
        return resp.getBody().length() > 2;
    }
}