public with sharing class YousignSubmitSignProcess implements Queueable, Database.AllowsCallouts {
    private Yousign_Procedure__c yousignProcedure;
    private List<Yousign_Signatory__c> selectedSignatoriesExt;
    private String contactsExterne;
    private List<Contact> listContactsExterne;
    private transient YousignApiService service;
    private YousignFileSettingsMatcher fileSettingsMatcher = new YousignFileSettingsMatcher();
    private Boolean submitProcedureNotStarted = false;
    // dernière position de la signature
    // MODIF IKS
    //private String lastPosition = '28,42,134,101';
    private String lastPosition = '42,711,192,791';
    // numero de l'emplacement de la signature
    private Integer positionNumber = 1;
    private YousignContactFieldHelper contactFieldHelper = new YousignContactFieldHelper();

    public YousignSubmitSignProcess(
        Yousign_Procedure__c yousignProcedure,
        List<Yousign_Signatory__c> selectedSignatoriesExt,
        String contactsExterne,
        Boolean submitProcedureNotStarted
    ) {
        this.yousignProcedure = yousignProcedure;
        this.selectedSignatoriesExt = selectedSignatoriesExt;
        this.contactsExterne = contactsExterne;
        if (contactsExterne != '') {
            this.listContactsExterne = (List<Contact>)System.JSON.deserialize(contactsExterne, List<Contact>.class);
        } else{
            this.listContactsExterne = null;
        }
        this.submitProcedureNotStarted = submitProcedureNotStarted;
    }

    public YousignSubmitSignProcess(Yousign_Procedure__c yousignProcedure) {
        this.yousignProcedure = yousignProcedure;
    }

    public void execute(QueueableContext context) {
        try {
            this.service = getEnvironment();
            List<Yousign_Submitted_Document__c> relatedDocuments = getNotSentDocuments();
            if (relatedDocuments.isEmpty()) {
                relatedDocuments = getSentDocuments();
                this.sendSignSubmission(relatedDocuments);
                ysign__Yousign_Procedure_Event__e okEvent = new ysign__Yousign_Procedure_Event__e(
                    ysign__Object_Id__c = yousignProcedure.ysign__ObjectReference__c,
                    ysign__Procedure_Id__c = yousignProcedure.Id,
                    ysign__Status__c = yousignProcedure.Status__c,
                    ysign__Yousign_Procedure_Id__c = yousignProcedure.Yousign_Procedure_Id__c
                );
                Database.SaveResult eventSaveResult = Eventbus.publish(okEvent);
            } else {
                this.sendFiles(relatedDocuments);
                if (!Test.isRunningTest()) {
                    System.enqueueJob(new YousignSubmitSignProcess(this.yousignProcedure,this.selectedSignatoriesExt,this.contactsExterne, submitProcedureNotStarted));
                }
            }
        } catch (Exception e) {
            this.setProcedureFailed(e.getMessage());
            System.debug(LoggingLevel.ERROR, 'ERREUR : ' + e.getMessage());
            System.debug(LoggingLevel.ERROR, 'ERREUR : ' + e.getStackTraceString());
        }
    }

    private void setProcedureFailed(String message) {
        this.yousignProcedure.Error_Message__c = message;
        this.yousignProcedure.Status__c = 'Failed';
        new DMLManager().updateAsUser(this.yousignProcedure);

        this.setSignatoriesFailed(this.yousignProcedure);

        ysign__Yousign_Procedure_Event__e failedEvent = new ysign__Yousign_Procedure_Event__e(
            ysign__Object_Id__c = yousignProcedure.ysign__ObjectReference__c,
            ysign__Procedure_Id__c = yousignProcedure.Id,
            ysign__Status__c = yousignProcedure.Status__c
        );
        Database.SaveResult eventSaveResult = Eventbus.publish(failedEvent);
    }

    private void setSignatoriesFailed(Yousign_Procedure__c procedure) {
        List<Yousign_Signatory__c> signatories = this.getSignatories(procedure);
        for (Yousign_Signatory__c signatory : signatories) {
            signatory.Status__c = 'Failed';
        }

        new DMLManager().updateAsUser(signatories);
    }

    private List<Yousign_Signatory__c> getSignatories(Yousign_Procedure__c procedure){
        List<Schema.SObjectField> signatoriesFields = new List<SObjectField> {Yousign_Signatory__c.Status__c};
        DMLManager.FLSCheckResultKeeper flsCheckResult = new DMLManager().getFLSCheckResult(signatoriesFields, Yousign_Signatory__c.getSObjectType());
        if (!flsCheckResult.areAllSelectedFieldsAvailable) {
            System.debug(LoggingLevel.ERROR, 'getSignatories - !flsCheckResult.areAllSelectedFieldsAvailable');
            throw new YousignSubmitSignProcessException(flsCheckResult.errorText);
        }

        List<Yousign_Signatory__c> signatories = [
                SELECT Id
                    , Status__c
                FROM Yousign_Signatory__c
                WHERE Yousign_Procedure__c = :procedure.Id
        ];

        return signatories;
    }

    private List<Yousign_Submitted_Document__c> getNotSentDocuments() {
        List<Schema.SObjectField> yousignSubmittedDocumentFields = new List<Schema.SObjectField>{Yousign_Submitted_Document__c.Document_Id__c, Yousign_Submitted_Document__c.Yousign_Document_Id__c, Yousign_Submitted_Document__c.Document_Name__c};
        DMLManager.FLSCheckResultKeeper yousignSubmittedDocumentFLSCheckResult = new DMLManager().getFLSCheckResult(yousignSubmittedDocumentFields , Yousign_Submitted_Document__c.getSObjectType());
        if (!yousignSubmittedDocumentFLSCheckResult.areAllSelectedFieldsAvailable) {
            System.debug(LoggingLevel.ERROR, 'getNotSentDocuments - !yousignSubmittedDocumentFLSCheckResult.areAllSelectedFieldsAvailable');
            throw new YousignSubmitSignProcessException(yousignSubmittedDocumentFLSCheckResult.errorText);
        }
        List<Yousign_Submitted_Document__c> relatedDocuments = [
                SELECT Id,
                    Document_Id__c,
                    Document_Name__c,
                    Yousign_Document_Id__c
                FROM Yousign_Submitted_Document__c
                WHERE Yousign_Document_Id__c = null
                    AND Yousign_Procedure__c = :this.yousignProcedure.Id
        ];

        return relatedDocuments;
    }

    private List<Yousign_Submitted_Document__c> getSentDocuments() {
        List<Schema.SObjectField> yousignSubmittedDocumentFields = new List<Schema.SObjectField>{Yousign_Submitted_Document__c.Document_Id__c, Yousign_Submitted_Document__c.Yousign_Document_Id__c, Yousign_Submitted_Document__c.Document_Name__c};
        DMLManager.FLSCheckResultKeeper yousignSubmittedDocumentFLSCheckResult = new DMLManager().getFLSCheckResult(yousignSubmittedDocumentFields , Yousign_Submitted_Document__c.getSObjectType());
        if (!yousignSubmittedDocumentFLSCheckResult.areAllSelectedFieldsAvailable) {
            System.debug(LoggingLevel.ERROR, 'getSentDocuments - !yousignSubmittedDocumentFLSCheckResult.areAllSelectedFieldsAvailable');
            throw new YousignSubmitSignProcessException(yousignSubmittedDocumentFLSCheckResult.errorText);
        }
        List<Yousign_Submitted_Document__c> relatedDocuments = [
                SELECT Id,
                    Document_Id__c,
                    Document_Name__c,
                    Yousign_Document_Id__c
                FROM Yousign_Submitted_Document__c
                WHERE Yousign_Procedure__c = :this.yousignProcedure.Id
        ];

        return relatedDocuments;
    }

    private void sendFiles(List<Yousign_Submitted_Document__c> submittedDocuments) {
        Set<Id> documentIds = new Set<Id>();
        for (Yousign_Submitted_Document__c document : submittedDocuments) {
            documentIds.add(document.ysign__Document_Id__c);
        }
        Map<Id, ContentVersion> mapLatestContentVersion =
                YousignConnectionService.getLatestContentVersions(documentIds);
        for (Yousign_Submitted_Document__c document : submittedDocuments) {
            ContentVersion latestContentVersion = mapLatestContentVersion.get(document.Document_Id__c);
            if (latestContentVersion == null) {
                continue;
            }
            YousignApiService.FileSubmission fileToInsert = new YousignApiService.FileSubmission(latestContentVersion, yousignProcedure.ysign__Yousign_Procedure_Id__c);
            this.loadFile(document, fileToInsert);
            if (document.Status__c != 'Sent') {
                throw new YousignSubmitSignProcessException('File upload error');
            }
        }
        new DMLManager().updateAsUser(submittedDocuments);
    }

    private void loadFile(Yousign_Submitted_Document__c submittedDocument, YousignApiService.FileSubmission fileToInsert) {

        try {
            YousignConnectionService.loadFile(this.service, fileToInsert);
            submittedDocument.Yousign_Document_Id__c = fileToInsert.fileOutput.id;
            submittedDocument.Status__c = 'Sent';
        } catch (YousignConnectionService.YousignConnectionException e) {
            System.debug(LoggingLevel.ERROR, 'ERREUR loadFile : ' + e);
            submittedDocument.Error_Message__c = e.getMessage();
            submittedDocument.Status__c = 'Failed';
        }

        submittedDocument.Response__c = fileToInsert.getBody();
    }

    private void sendSignSubmission(List<Yousign_Submitted_Document__c> submittedDocuments) {
        List<Yousign_Signatory__c> relatedSignatories = getRelatedSignatories();
        YousignApiService.ProcedureSubmission submission = new YousignApiService.ProcedureSubmission(this.yousignProcedure.Id);
        if (submitProcedureNotStarted) {
            submission.getProcedureInput().start = false;
        }
        this.addMembersToSubmission(submission, relatedSignatories, submittedDocuments);
        this.sendSubmission(submission);
        if (submitProcedureNotStarted && yousignProcedure.ysign__Yousign_Procedure_Id__c != null && !submittedDocuments.isEmpty()) {
            this.sendFiles(submittedDocuments);
        }
        this.populateSignatoriesIds(relatedSignatories, submission);
        this.updateProcedureAndSignatories(relatedSignatories);
    }

    private List<Yousign_Signatory__c> getRelatedSignatories() {
        List<Schema.SObjectField> yousignSignatoryFields = new List<Schema.SObjectField>{Yousign_Signatory__c.Contact__c, Yousign_Signatory__c.Order__c, Yousign_Signatory__c.Role__c, Yousign_Signatory__c.Status__c};
        DMLManager.FLSCheckResultKeeper yousignSignatoryFLSCheckResult = new DMLManager().getFLSCheckResult(yousignSignatoryFields , Yousign_Signatory__c.getSObjectType());
        if (!yousignSignatoryFLSCheckResult.areAllSelectedFieldsAvailable) {
            throw new YousignSubmitSignProcessException(yousignSignatoryFLSCheckResult.errorText);
        }
        List<Yousign_Signatory__c> relatedSignatories = [
                SELECT Id
                    , Contact__c
                    , Order__c
                    , Role__c
                    , Status__c
                FROM Yousign_Signatory__c
                WHERE Yousign_Procedure__c = :this.yousignProcedure.Id
                ORDER BY Order__c
        ];

        return relatedSignatories;
    }

    private void addMembersToSubmission(YousignApiService.ProcedureSubmission submission,
            List<Yousign_Signatory__c> signatories, List<Yousign_Submitted_Document__c> submittedDocuments) {

        List<Contact> selectedContacts = this.getContacts(signatories);
        // JOIN external contacts with selectedContacts
        if (listContactsExterne != null){
            for(Integer i=0; i<this.listContactsExterne.size(); i++){
                selectedContacts.add(this.listContactsExterne[i]);
            }
        }

        // JOIN external signatories with selectedsignatories
        List<Yousign_Signatory__c> allSignatories = signatories.clone();
        if (selectedSignatoriesExt != null){
            for(Integer i=0; i<this.selectedSignatoriesExt.size(); i++){
                allSignatories.add(this.selectedSignatoriesExt[i]);
            }
        }

        Map<Id, Contact> selectedContactsByIds = new Map<Id, Contact>(selectedContacts);
        List<YousignApiService.FileToSign> filesToSign;
        submission.getProcedureInput().ordered = this.yousignProcedure.Ordered__c;

        for (Yousign_Signatory__c currentSignatory : allSignatories) {
            if (!submitProcedureNotStarted) {
                filesToSign = makeFilesToSignList(submittedDocuments);
            } else {
                filesToSign = new List<YousignApiService.FileToSign>();
            }
            System.debug('filesToSign = '+filesToSign);
            submission.addMember(selectedContactsByIds.get(currentSignatory.Contact__c), filesToSign);
        }
    }

    private List<Contact> getContacts(List<Yousign_Signatory__c> signatoryItems) {
        Set<Id> contactIds = makeContactIds(signatoryItems);
        return contactFieldHelper.getContactsFields(contactIds);
    }

    private Set<Id> makeContactIds(List<Yousign_Signatory__c> signatoryItems) {
        Set<Id> contactIds = new Set<Id>();
        for (Yousign_Signatory__c signatory : signatoryItems) {
            contactIds.add(signatory.Contact__c);
        }

        return contactIds;
    }

    private List<YousignApiService.FileToSign> makeFilesToSignList(List<Yousign_Submitted_Document__c> submittedDocuments) {
        List<YousignApiService.FileToSign> resultList = new List<YousignApiService.FileToSign>();
        for (Yousign_Submitted_Document__c submittedDocument : submittedDocuments) {
            List<ysign__Yousign_File_Settings__mdt> filesSettings = fileSettingsMatcher.getMatchingRules(submittedDocument.ysign__Document_Name__c);
            if (filesSettings.isEmpty()) {
                // keep the prior behaviour except if procedure is sent without start
                YousignApiService.FileToSign fileToSignTempo = new YousignApiService.FileToSign(submittedDocument.Yousign_Document_Id__c);

                if (positionNumber != 1) {
                    fileToSignTempo.position = fileToSignTempo.setPosition(lastPosition);
                    lastPosition = fileToSignTempo.position;
                } else {
                    fileToSignTempo.position = lastPosition;
                }
                // Modif IKS
                //fileToSignTempo.page = 1;
                positionNumber++;
                System.debug('position = '+fileToSignTempo.position);

                resultList.add(fileToSignTempo);
            } else {
                for (ysign__Yousign_File_Settings__mdt fSetting : filesSettings) {
                    YousignApiService.FileToSign fileToSignTempo = new YousignApiService.FileToSign(
                        submittedDocument.Yousign_Document_Id__c,
                        (Integer)fSetting.ysign__Page__c,
                        fSetting.ysign__Position__c,
                        fSetting.ysign__PDF_field_name__c,   // fieldName ??
                        fSetting.ysign__Extra_text_1__c,
                        fSetting.ysign__Extra_text_2__c,
                        (Integer)fSetting.ysign__Font_size__c
                    );
                    fileToSignTempo.type = fSetting.ysign__FieldType__c;
                    fileToSignTempo.contentRequired = fSetting.ysign__Content_required__c;
                    fileToSignTempo.content = fSetting.ysign__Placeholder__c;
                    resultList.add(fileToSignTempo);
                }
            }
        }
        System.debug(LoggingLevel.DEBUG, JSON.serializePretty(resultList));
        return resultList;
    }

    private void sendSubmission(YousignApiService.ProcedureSubmission submission) {

        try {
            YousignConnectionService.sendSubmission(this.service, submission);
            if (submitProcedureNotStarted) {
                this.yousignProcedure.Status__c = 'Created';
            } else {
                this.yousignProcedure.Status__c = 'Submitted';
            }
            this.yousignProcedure.Yousign_Procedure_Id__c = submission.procedureOutput.id;

        } catch (YousignConnectionService.YousignConnectionException e) {
            this.yousignProcedure.Response__c = submission.getBody();
            throw e;
        }
        this.yousignProcedure.Response__c = submission.getBody();
    }

    private void populateSignatoriesIds(List<Yousign_Signatory__c> signatories,
                                                YousignApiService.ProcedureSubmission submission) {
        // JOIN external contacts with selectedContacts
        List<Yousign_Signatory__c> allSignatories = signatories.clone();
        if (selectedSignatoriesExt != null){
            for(Integer i=0; i<this.selectedSignatoriesExt.size(); i++){
                    allSignatories.add(this.selectedSignatoriesExt[i]);
            }
        }
        for (Integer i = 0; i < allSignatories.size(); i++) {
            allSignatories.get(i).Yousign_Signatory_Id__c = submission.procedureOutput.members.get(i).id;
            allSignatories.get(i).Status__c = 'Submitted';
        }
    }

    private void updateProcedureAndSignatories(List<Yousign_Signatory__c> signatories) {
        DMLManager dmlManager = new DMLManager();
        Savepoint procedureSignatoryUpdate = Database.setSavepoint();
        try {
            dmlManager.updateAsUser(this.yousignProcedure);
            dmlManager.updateAsUser(signatories);
        } catch (Exception e) {
            Database.rollback(procedureSignatoryUpdate);
            throw e;
        }
    }

    public static YousignApiService getEnvironment(){
        YousignApiService service;
        Yousign_setting__c settings = Yousign_setting__c.getOrgDefaults();
        if (settings != null && !String.isEmpty(settings.Environment__c)) {
                if (settings.Environment__c == 'PRODUCTION'){
                    service = new YousignApiService(YousignApiService.Environment.PRODUCTION);
                }else {
                    service = new YousignApiService(YousignApiService.Environment.STAGING);
                }
        } else {
            service = new YousignApiService(YousignApiService.Environment.STAGING);
        }

        return service;
    }

    public class YousignSubmitSignProcessException extends Exception {}
}