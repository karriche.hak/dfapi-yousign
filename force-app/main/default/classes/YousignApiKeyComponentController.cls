public with sharing class YousignApiKeyComponentController {

    @AuraEnabled
    public static ysign__Yousign_setting__c getYousignSettings() {
        system.debug('### = ysign__Yousign_setting__c.getOrgDefaults() = ' + ysign__Yousign_setting__c.getOrgDefaults());
        return ysign__Yousign_setting__c.getOrgDefaults();
    }

    @AuraEnabled
    public static void updateYousignSettings(ysign__Yousign_setting__c settings) {
        upsert settings;
    }

    @AuraEnabled
    public static List<String> getLabels(){
        List<String> labels = new List<String>();
        labels.add(SObjectType.Yousign_setting__c.fields.Api_key__c.getLabel());
        labels.add(SObjectType.Yousign_setting__c.fields.Public_Site_Link__c.getLabel());
        labels.add(SObjectType.Yousign_setting__c.fields.Environment__c.getLabel());
        labels.add(SObjectType.Yousign_setting__c.fields.Company_Name__c.getLabel());
        labels.add(SObjectType.Yousign_setting__c.fields.Sender_Name__c.getLabel());
        return labels;
    }
}