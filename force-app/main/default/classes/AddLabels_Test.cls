@isTest
public class AddLabels_Test{
    
    public static testmethod void testAddLabels(){
        String Yousign_All_Files_Weight_Error_Message = AddLabels.Yousign_All_Files_Weight_Error_Message;
        String Yousign_Api_Key_Component_Cancel_Button = AddLabels.Yousign_Api_Key_Component_Cancel_Button;
        String Yousign_Api_Key_Component_Edit_Button = AddLabels.Yousign_Api_Key_Component_Edit_Button;
        String Yousign_Api_Key_Component_Field_Is_Required_Message = AddLabels.Yousign_Api_Key_Component_Field_Is_Required_Message;
        String Yousign_Api_Key_Component_Save_Button = AddLabels.Yousign_Api_Key_Component_Save_Button;
        String Yousign_Api_Key_Not_Specified = AddLabels.Yousign_Api_Key_Not_Specified;
        String Yousign_Api_Key_Page_Blank_Apikey_Error = AddLabels.Yousign_Api_Key_Page_Blank_Apikey_Error;
        String Yousign_Api_Key_Page_Blank_Site_Url_Error = AddLabels.Yousign_Api_Key_Page_Blank_Site_Url_Error;
        String Yousign_Api_Key_Page_Cancel_Button = AddLabels.Yousign_Api_Key_Page_Cancel_Button;
        String Yousign_Api_Key_Page_Edit_Button = AddLabels.Yousign_Api_Key_Page_Edit_Button;
        String Yousign_Api_Key_Page_Save_Button = AddLabels.Yousign_Api_Key_Page_Save_Button;
        String Yousign_Api_Key_Page_Subtitle = AddLabels.Yousign_Api_Key_Page_Subtitle;
        String Yousign_Api_Key_Page_Success_Message = AddLabels.Yousign_Api_Key_Page_Success_Message;
        String Yousign_Api_Key_Page_Title = AddLabels.Yousign_Api_Key_Page_Title;
        String Yousign_Api_Key_Page_Value_Not_Inputed_Error = AddLabels.Yousign_Api_Key_Page_Value_Not_Inputed_Error;
        String Yousign_At_Least_One_Signer_Message = AddLabels.Yousign_At_Least_One_Signer_Message;
        String Yousign_Back_Button = AddLabels.Yousign_Back_Button;
        String Yousign_Cancel_Button = AddLabels.Yousign_Cancel_Button;
        String Yousign_Cancel_Success_Message = AddLabels.Yousign_Cancel_Success_Message;
        String Yousign_Contact_Lookup_Label = AddLabels.Yousign_Contact_Lookup_Label;
        String Yousign_Document_Id_not_specified = AddLabels.Yousign_Document_Id_not_specified;
        String Yousign_Document_Page_Message = AddLabels.Yousign_Document_Page_Message;
        String Yousign_Draft_Button = AddLabels.Yousign_Draft_Button;
        String Yousign_Draft_Success_Message = AddLabels.Yousign_Draft_Success_Message;
        String Yousign_Event_not_supported = AddLabels.Yousign_Event_not_supported;
        String Yousign_File_Not_Selected_Message = AddLabels.Yousign_File_Not_Selected_Message;
        String Yousign_File_Picklist_Label = AddLabels.Yousign_File_Picklist_Label;
        String Yousign_Form_Button_Add_Signatory = AddLabels.Yousign_Form_Button_Add_Signatory;
        String Yousign_Form_Email = AddLabels.Yousign_Form_Email;
        String Yousign_Form_Example_Email = AddLabels.Yousign_Form_Example_Email;
        String Yousign_Form_Example_Phone = AddLabels.Yousign_Form_Example_Phone;
        String Yousign_Form_First_Name = AddLabels.Yousign_Form_First_Name;
        String Yousign_Form_Last_Name = AddLabels.Yousign_Form_Last_Name;
        String Yousign_Form_Phone = AddLabels.Yousign_Form_Phone;
        String Yousign_LC_Lookup_No_Results_Found = AddLabels.Yousign_LC_Lookup_No_Results_Found;
        String Yousign_LC_Lookup_Placeholder = AddLabels.Yousign_LC_Lookup_Placeholder;
        String Yousign_LC_Title = AddLabels.Yousign_LC_Title;
        String Yousign_One_File_Weight_Error_Message = AddLabels.Yousign_One_File_Weight_Error_Message;
        String Yousign_Order_Contacts = AddLabels.Yousign_Order_Contacts;
        String Yousign_Procedure_Creation = AddLabels.Yousign_Procedure_Creation;
        String Yousign_Procedure_Id_not_specified = AddLabels.Yousign_Procedure_Id_not_specified;
        String Yousign_Procedure_Not_Found = AddLabels.Yousign_Procedure_Not_Found;
        String Yousign_Public_Site_Link_Not_Specified = AddLabels.Yousign_Public_Site_Link_Not_Specified;
        String Yousign_Received_file_is_empty = AddLabels.Yousign_Received_file_is_empty;
        String Yousign_Reminder_Nb_Days = AddLabels.Yousign_Reminder_Nb_Days;
        String Yousign_Reminder_Procedure = AddLabels.Yousign_Reminder_Procedure;
        String Yousign_Request_Success_Message = AddLabels.Yousign_Request_Success_Message;
        String Yousign_Request_Success_Title = AddLabels.Yousign_Request_Success_Title;
        String Yousign_Signatory_not_found = AddLabels.Yousign_Signatory_not_found;
        String Yousign_Signatory_Page_Message = AddLabels.Yousign_Signatory_Page_Message;
        String Yousign_Submit_Button = AddLabels.Yousign_Submit_Button;
        String Yousign_Test_DML_Name = AddLabels.Yousign_Test_DML_Name;
        String Yousign_VFPage_Add_New_External_Contact_button = AddLabels.Yousign_VFPage_Add_New_External_Contact_button;
        String Yousign_VFPage_Add_New_Signer_button = AddLabels.Yousign_VFPage_Add_New_Signer_button;
        String Yousign_VFPage_Already_Chosen_Signer_Message = AddLabels.Yousign_VFPage_Already_Chosen_Signer_Message;
        String Yousign_VFPage_Attacment_Not_Selected_Error = AddLabels.Yousign_VFPage_Attacment_Not_Selected_Error;
        String Yousign_Wait = AddLabels.Yousign_Wait;
    }
}