public with sharing class YousignExternalFileLoaderHakim  {
    
    private Yousign_Procedure__c yousignProcedure;
    private YousignApiModel.ProcedureFileOutput[] files;
    private transient YousignApiService service;
    private Static String Yousign_Received_file_is_empty = Label.Yousign_Received_file_is_empty;

    public YousignExternalFileLoaderHakim(Yousign_Procedure__c yousignProcedure,YousignApiModel.ProcedureFileOutput[] files) {
        this.yousignProcedure = yousignProcedure;
        this.files = files;
        
        
    }

    public void execute(QueueableContext context) {
     
        try {
            if (files==null || files.isEmpty()) return;
            this.service = getEnvironment();

            List<Yousign_Submitted_Document__c> nonReceivedDocuments = this.getNonReceivedDocuments();
           
            
            if (nonReceivedDocuments.isEmpty()) this.retrieveDocumentExternal(createSubmitedDocument(this.files[0]));
         
            this.files.remove(0);
   
            if (!Test.isRunningTest()) System.enqueueJob(new YousignExternalFileLoaderHakim(this.yousignProcedure,this.files));
        } catch (Exception e) {
            System.debug('execute catch  SLM RG');
            this.yousignProcedure.Error_Message__c = e.getMessage();
            new DMLManager().updateAsUser(this.yousignProcedure);
        }
    }
    
    private Yousign_Submitted_Document__c createSubmitedDocument(YousignApiModel.ProcedureFileOutput ffile) {
            
          
         

        Yousign_Submitted_Document__c Submitted_Document=new  Yousign_Submitted_Document__c(
                    Yousign_Document_Id__c=ffile.id
                    , ysign__Document_Name__c=ffile.name
                    , Document_Id__c=null
                    , Yousign_Procedure__c=yousignProcedure.Id
                    , Status__c='Sent');
        //insert  Submitted_Document;
        return Submitted_Document;
    }
    private List<Yousign_Submitted_Document__c> getNonReceivedDocuments() {
        List<Schema.SObjectField> yousignSubmittedDocumentFields = new List<Schema.SObjectField>{Yousign_Submitted_Document__c.Yousign_Document_Id__c, Yousign_Submitted_Document__c.Document_Id__c, Yousign_Submitted_Document__c.Yousign_Procedure__c};
        DMLManager.FLSCheckResultKeeper yousignSignatoryFLSCheckResult = new DMLManager().getFLSCheckResult(yousignSubmittedDocumentFields , Yousign_Submitted_Document__c.getSObjectType());
        if (!yousignSignatoryFLSCheckResult.areAllSelectedFieldsAvailable) {
            throw new YousignFileLoaderException(yousignSignatoryFLSCheckResult.errorText);
        }
        List<Yousign_Submitted_Document__c> nonReceivedDocuments = [
                SELECT Id
                    , Yousign_Document_Id__c
                    , ysign__Document_Name__c
                    , Document_Id__c
                    , Yousign_Procedure__c
                FROM Yousign_Submitted_Document__c
                WHERE Yousign_Procedure__c = :yousignProcedure.Id
                    AND Yousign_Document_Id__c=:files[0].id
        ];

        return nonReceivedDocuments;
    }

    private void retrieveDocument(Yousign_Submitted_Document__c submittedDocument) {
        YousignApiService.FileDownloadSubmission downloadSubmission = new YousignApiService.FileDownloadSubmission(submittedDocument);
        this.downloadFile(submittedDocument, downloadSubmission);
        //new DMLManager().updateAsUser(submittedDocument);
        update submittedDocument;
        //insert submittedDocument;
        
    }

    private void retrieveDocumentExternal(Yousign_Submitted_Document__c submittedDocument) {
        YousignApiService.FileDownloadSubmission downloadSubmission = new YousignApiService.FileDownloadSubmission(submittedDocument);
        this.downloadFileExternal(submittedDocument, downloadSubmission);

       /* ContentVersion ContVerFile = new ContentVersion();
        ContVerFile.VersionData = blob.valueOf('ffile');
  
         ContVerFile.Title = ffile.name; 
         

        ContVerFile.FirstPublishLocationId  = yousignProcedure.ysign__ObjectReference__c;
         
         ContVerFile.ContentLocation= 's';
         ContVerFile.PathOnClient=ffile.name+'.pdf';
 
          insert ContVerFile;
 
         Id DocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:ContVerFile.Id].ContentDocumentId;
        //new DMLManager().updateAsUser(submittedDocument);
        //update submittedDocument;
        submittedDocument.Document_Id__c=DocId;*/
        insert submittedDocument;
        
    }

    private void downloadFileExternal(Yousign_Submitted_Document__c submittedDocument,
            YousignApiService.FileDownloadSubmission downloadSubmission) {
        try {
        YousignConnectionService.downloadFile(this.service, downloadSubmission);
        ContentVersionCreator creator = new ContentVersionCreator();
        creator.createNewContentVersion(downloadSubmission, submittedDocument);
        submittedDocument.Status__c = 'sent';
        } catch (Exception e) {
        submittedDocument.Status__c = 'Failed---Massi';
        System.debug(LoggingLevel.ERROR, 'vta error : ' + e);
        submittedDocument.Error_Message__c = e.getMessage();
        }
}
   
    private void downloadFile(Yousign_Submitted_Document__c submittedDocument,
                                        YousignApiService.FileDownloadSubmission downloadSubmission) {
        try {
            YousignConnectionService.downloadFile(this.service, downloadSubmission);
            ContentVersionCreatorExternal creator = new ContentVersionCreatorExternal();
            creator.createNewContentVersion(downloadSubmission, submittedDocument);
            submittedDocument.Status__c = 'Received';
        } catch (Exception e) {
            submittedDocument.Status__c = 'Failed';
            System.debug(LoggingLevel.ERROR, 'vta error : ' + e);
            submittedDocument.Error_Message__c = e.getMessage();
        }
    }

    private without sharing class ContentVersionCreatorExternal {

        private ContentVersion createNewContentVersion(YousignApiService.FileDownloadSubmission downloadSubmission,
        Yousign_Submitted_Document__c submiteddocument) {

            if (String.isEmpty(downloadSubmission.getEncodedFileData()))
                    throw new YousignFileLoaderException(Yousign_Received_file_is_empty);

            ContentVersion receivedFile = new ContentVersion();
            receivedFile.ContentDocumentId = submiteddocument.Document_Id__c;
            receivedFile.VersionData = EncodingUtil.base64Decode(downloadSubmission.getEncodedFileData());
            receivedFile.PathOnClient = 'signedFile.pdf';

            new DMLManager().insertAsUser(receivedFile);

            ContentDocument contentDoc=new ContentDocument();
            contentDoc.id=submiteddocument.Document_Id__c ;
            contentDoc.Title=submiteddocument.ysign__Document_Name__c; 
            update contentDoc;
            return receivedFile;
        }
    }




    private without sharing class ContentVersionCreator {

        private ContentVersion createNewContentVersion(YousignApiService.FileDownloadSubmission downloadSubmission,
        Yousign_Submitted_Document__c submiteddocument) {

            if (String.isEmpty(downloadSubmission.getEncodedFileData()))
                    throw new YousignFileLoaderException(Yousign_Received_file_is_empty);

            ContentVersion receivedFile = new ContentVersion();
            receivedFile.ContentDocumentId = submiteddocument.Document_Id__c;
            receivedFile.VersionData = EncodingUtil.base64Decode(downloadSubmission.getEncodedFileData());
            receivedFile.PathOnClient = 'signedFile.pdf';

            new DMLManager().insertAsUser(receivedFile);

            ContentDocument contentDoc=new ContentDocument();
            contentDoc.id=submiteddocument.Document_Id__c ;
            contentDoc.Title=submiteddocument.ysign__Document_Name__c+'- signed - '+System.now().format() ; 
            update contentDoc;
            return receivedFile;
        }
    }

    public static YousignApiService getEnvironment(){
        YousignApiService service;
        Yousign_setting__c settings = Yousign_setting__c.getOrgDefaults();
        if (settings != null && !String.isEmpty(settings.Environment__c)) {
                if (settings.Environment__c == 'PRODUCTION'){
                    service = new YousignApiService(YousignApiService.Environment.PRODUCTION);
                }else {
                    service = new YousignApiService(YousignApiService.Environment.STAGING);
                } 
        } else {
            service = new YousignApiService(YousignApiService.Environment.STAGING);
        }

        return service;
    }

    public class YousignFileLoaderException extends Exception {}
}