public with sharing class YousignPicklistController {

    @AuraEnabled
    public static List<ContentDocumentLink> getAttachments(String objectId) {
        List<ContentDocumentLink> contentDocumentLinks;
        try {
            contentDocumentLinks = YousignConnectionService.getContentDocumentLinks(objectId);
        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
        return contentDocumentLinks;
    }
}