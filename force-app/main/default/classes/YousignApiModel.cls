public class YousignApiModel {

    public abstract class YousignApiObject {
        public String toJson() {
            String serializedJSON = JSON.serializePretty(this);
            System.debug('****rgLoggingLevel.DEBUG, serializedJSON');
            System.debug(LoggingLevel.DEBUG, serializedJSON);
            return serializedJSON;
        }
    }

    public abstract class Procedure extends YousignApiObject {
    }

    /*===============================================================*/
    /* Procedure Input */
    /*===============================================================*/
    public virtual class ProcedureUpdate extends Procedure {
        public Boolean start {get; set;}
        public String id {get; set;}
        public ProcedureUpdate(Boolean start, String id) {
            this.start=start;
            this.id=id;
            Yousign_setting__c settings = Yousign_setting__c.getOrgDefaults();
            Boolean isConfigOverride = false;
            if (settings != null && String.isNotBlank(settings.ysign__ApexClass_ProcedureConfig__c)) {
                System.Type customImplType = Type.forName(settings.ysign__ApexClass_ProcedureConfig__c);
                if (customImplType != null) {
                    try {
                        this.config = (IProcedureConfig)customImplType.newInstance();
                        isConfigOverride = true;
                    } catch(Exception ex) {
                        System.debug(LoggingLevel.ERROR, 'Something went wrong while using User defined IProcedureConfig implementation. Caused by:' + ex.getCause());
                    }
                }
            }
            if (!isConfigOverride) {
                // Load the default configuration
                this.config = new ysign.YousignDefaultConfigProvider(id);
              //YousignDefaultConfigProvider  initWebhookConfig= new ysign.YousignDefaultConfigProvider(id);
              //this.ProcedureOutput.webhook=initWebhookConfig.initializeWebhooks();
            }
        }
        public IProcedureConfig config {get; set;}
    }
    public virtual class ProcedureInput extends Procedure {

        public String name {get; set {
            name = String.isEmpty(value) ? '' : value;
        }}
        public String description {get; set {
            description = String.isEmpty(value) ? '' : value;
        }}

        public String expiresAt {get; set {
            expiresAt = String.isEmpty(value) ? '' : value;
        }}

        public Boolean template {get; set {
            template = value == null ? false : value;
        }}
        public Boolean ordered {get; set {
            ordered = value == null ? false : value;
        }}

        public String parent {get; set {
            parent = String.isEmpty(value) ? '' : value;
        }}

        public IProcedureConfig config {get; set;}
        public List<MemberInput> members {get; set;}

        public Boolean start {get; set;}

        public ProcedureInput(String procedureId) {
            this(new List<Contact>(), new List<FileObjectInput>(), procedureId);
        }

        public ProcedureInput(List<Contact> contacts, List<FileObjectInput> fileObjects,String procedureId) {
            this.name = 'Procedure du ' + DateTime.now().addSeconds(UserInfo.getTimeZone().getOffset(DateTime.now()) / 1000);
            this.description = 'Procedure created from Salesforce.';
            this.expiresAt = this.constructExpiresAt();
            this.template = false;
            this.ordered = false;
            this.parent = null;
            Yousign_setting__c settings = Yousign_setting__c.getOrgDefaults();
            Boolean isConfigOverride = false;
            if (settings != null && String.isNotBlank(settings.ysign__ApexClass_ProcedureConfig__c)) {
                System.Type customImplType = Type.forName(settings.ysign__ApexClass_ProcedureConfig__c);
                if (customImplType != null) {
                    try {
                        this.config = (IProcedureConfig)customImplType.newInstance();
                        isConfigOverride = true;
                    } catch(Exception ex) {
                        System.debug(LoggingLevel.ERROR, 'Something went wrong while using User defined IProcedureConfig implementation. Caused by:' + ex.getCause());
                    }
                }
            }
            if (!isConfigOverride) {
                // Load the default configuration
                this.config = new ysign.YousignDefaultConfigProvider(procedureId);
               // YousignDefaultConfigProvider  initWebhookConfig= new ysign.YousignDefaultConfigProvider(procedureId);
              //  this.webhook=initWebhookConfig.initializeWebhooks();
            }
            this.members = new List<MemberInput>();
            this.start = true;
            Integer i = 0;
            for (Contact contact : contacts) {
                this.members.add(new MemberInput(i++, contact, fileObjects));
            }
        }

        public void addMemeber(Integer pos, Contact contact, List<FileObjectInput> fileObjects) {
            this.members.add(new MemberInput(pos, contact, fileObjects));
        }

        private String constructExpiresAt() {
            return Datetime.now().addDays(7).format('yyyy-MM-dd\'T\'HH:mm:ss.SS\'Z\'');
        }
    }

    /*===============================================================*/
    /* Procedure Output */
    /*===============================================================*/
    public class ProcedureOutput extends Procedure {

        public String id {get; set;}
        public String name {get; set;}
        public String description {get; set;}

        public String createdAt {get; set;}
        public String updatedAt {get; set;}
        public String expiresAt {get; set;}

        public Boolean deleted {get; set;}
        public String deletedAt {get; set;}

        public String status {get; set;}

        public String creator {get; set;}
        public String company {get; set;}

        public String template {get; set;}
        public Boolean ordered {get; set;}
        public String parent {get; set;}

        public List<MemberOutput> members {get; set;}
        public Map<String,List<ysign.YousignDataModel.ConfigWebhookTemplate>>  webhook {get; set;}
        // add by RG 
        public ysign.YousignDataModel.ProcedureConfig config {get; set;}
        //end RG
        public ProcedureFileOutput[] files {get; set;}
    }

    /*===============================================================*/
    /* Procedure File Class */
    /*===============================================================*/
    public abstract class ProcedureFile extends YousignApiObject {

    }

    public class ProcedureFileInput extends ProcedureFile {
        public String name {get; set{
            name = String.isEmpty(value) ? '' : value;
        }}
        public String type {get; set{
            type = String.isEmpty(value) ? '' : value;
        }}
        public String contentType {get; set{
            contentType = String.isEmpty(value) ? '' : value;
        }}
        public String description {get; set{
            description = String.isEmpty(value) ? '' : value;
        }}
        public String[] metadata {get; set{
            metadata = value == null ? new List<String>() : value;
        }}
        public String company {get; set{
            company = String.isEmpty(value) ? '' : value;
        }}
        public String creator {get; set{
            creator = String.isEmpty(value) ? '' : value;
        }}
    }

    public class ProcedureFileOutput extends ProcedureFile {
        public String id {get; set;}
        public String name {get; set;}
        public String type {get; set;}
        public String contentType {get; set;}
        public String description {get; set;}
        public String createdAt {get; set;}
        public String updatedAt {get; set;}
        public String[] metadata {get; set;}
        public String company {get; set;}
        public String creator {get; set;}
    }

    /*===============================================================*/
    /* Members */
    /*===============================================================*/
    public abstract class Member extends YousignApiObject{

    }
    public class MemberUpdateInput extends Member {

        public String procedure {get; set {
            procedure = String.isEmpty(value) ? '' : value;
        }}
        public String firstname {get; set {
            firstname = String.isEmpty(value) ? '' : value;
        }}
        public String lastname {get; set {
            lastName = String.isEmpty(value) ? '' : value;
        }}
        public String email {get; set {
            email = String.isEmpty(value) ? '' : value;
        }}
        public String phone {get; set {
            phone = String.isEmpty(value) ? '' : value;
        }}
        public MemberUpdateInput() {
        }
    }
    public class MemberInput extends Member {

        public String type {get; set {
            if (!new Set<String>{'signer','validator'}.contains(value)) {
                throw new MemberTypeException();
            }
            type = value;
        }}

        public String procedure {get; set {
            procedure = String.isEmpty(value) ? '' : value;
        }}
        public String firstname {get; set {
            firstname = String.isEmpty(value) ? '' : value;
        }}
        public String lastname {get; set {
            lastName = String.isEmpty(value) ? '' : value;
        }}
        public String email {get; set {
            email = String.isEmpty(value) ? '' : value;
        }}
        public String phone {get; set {
            phone = String.isEmpty(value) ? '' : value;
        }}
        public Integer position {get; set {
            position = value == null ? 0 : value;
        }}

        public List<FileObjectInput> fileObjects {get; set;}

        public MemberInput() {
        }

        public MemberInput(Contact contact) {
            this(0, contact, new List<FileObjectInput>());
        }

        public MemberInput(Integer pos, Contact contact, List<FileObjectInput> fileObjects) {
            this.type = 'signer';
            this.firstname = contact.FirstName;
            this.lastname = contact.LastName;
            this.email = contact.Email;
            this.phone = String.isEmpty(contact.MobilePhone) ? contact.Phone : contact.MobilePhone;
            this.position = pos;
            this.fileObjects = fileObjects;
        }
        public MemberInput(Integer pos, Contact contact, List<FileObjectInput> fileObjects, String procedure) {
            this.type = 'signer';
            this.firstname = contact.FirstName;
            this.lastname = contact.LastName;
            this.email = contact.Email;
            this.phone = String.isEmpty(contact.MobilePhone) ? contact.Phone : contact.MobilePhone;
            this.position = pos;
            this.fileObjects = fileObjects;
            this.procedure=procedure;
        }
    }

    public class MemberUpdateOutput extends Member {
        public String id {get; set;}
        public String user {get; set;}
        public String type {get; set;}
        public String firstName {get; set;}
        public String lastName {get; set;}
        public String email {get; set;}
        public String phone {get; set;}
        public String position  {get; set;}
        public String updatedAt {get; set;}
        public String createdAt {get; set;}
        public String status {get; set;}
        public String comment {get; set;}
    }

    public class MemberOutput extends Member {
        public String id {get; set;}
        public String user {get; set;}
        public String type {get; set;}

        public String firstName {get; set;}
        public String lastName {get; set;}
        public String email {get; set;}
        public String phone {get; set;}
        public String position  {get; set;}

        public String updatedAt {get; set;}
        public String createdAt {get; set;}

        public String status {get; set;}
        public String parent {get; set;}

        public List<FileObjectOutput> fileObjects {get; set;}

        public String comment {get; set;}
    }

    /*===============================================================*/
    /* File Object */
    /*===============================================================*/
    public abstract class FileObject {

    }

    public virtual class FileObjectInput extends FileObject {
        /// type can be signature or text, if not specified, type is signature
        public String type {
            get;
            set {
                type = String.isBlank(value) ? 'signature' :
                    (value.equalsIgnoreCase('text') ? 'text' : 'signature');
            }
        }
        /// true by default
        public Boolean contentRequired {
            get;
            set {
                contentRequired = (value == null ? true : value);
            }
        }
        public String content { get; set; }
        public String file {get; set {
            file = String.isEmpty(value) ? '' : value;
        }}
        public Integer page {get; set {
            page = value == null ? 1 : value;
        }}
        public String position {get; set {
            position = String.isEmpty(value) ? '' : value;
        }}
        public String fieldName {get; set; }
        public String mention {get; set {
            mention = String.isEmpty(value) ? '' : value;
        }}
        public String mention2 {get; set {
            mention2 = String.isEmpty(value) ? '' : value;
        }}
        public Integer fontSize {get; set; }

        public FileObjectInput () {
            type = 'signature';
            contentRequired = true;
        }
    }

    public virtual class FileObjectOutput extends FileObject {
        public String id {get; set;}
        public ProcedureFileOutput file {get; set;}
        public Integer page {get; set;}
        public String position {get; set;}
        public String fieldName {get; set;}
        public String mention {get; set;}
        public String createdAt {get; set;}
        public String updatedAt {get; set;}
        public String executedAt {get; set;}

        public FileObjectOutput () {
        }
    }

    /*===============================================================*/
    /* File */
    /* Used for new files submission */
    /*===============================================================*/
    public abstract class File extends YousignApiObject {

    }

    public class FileInput extends File {
        public String name {get; set;}
        public String password {get; set {
            password = String.isEmpty(value) ? '' : value;
        }}
        public String description {get; set {
            description = String.isEmpty(value) ? '' : value;
        }}
        public List<String> metadata {get; set {
            metadata = value == null ? new List<String>() : value;
        }}
        public String content {get; set;}
        public String procedure { get; set; }
    }

    public class FileOutput extends File {
        public String id {get; set;}
        public String name {get; set;}
        public String password {get; set;}
        public String description {get; set;}
        public String createdAt {get; set;}
        public String updatedAt {get; set;}
        public String dataRoutes {get; set;}
        public String content {get; set;}
    }

    /*===============================================================*/
    /* Exceptions */
    /*===============================================================*/
    public abstract class YousignApiModelException extends Exception {}

    public class InvalidProcedureStatusException extends YousignApiModelException {}

    public class MemberTypeException extends YousignApiModelException {}

    public class YousignConfigurationException extends YousignApiModelException {}
    
}