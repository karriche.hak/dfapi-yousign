public with sharing class YousignSubmitDuplicateProcess implements Queueable, Database.AllowsCallouts {
    private Yousign_Procedure__c yousignProcedure;
    private List<Yousign_Signatory__c> selectedSignatoriesExt;
    private String contactsExterne;
    private String yousignTemplateId;
    private Boolean sendFiles;
    private List<Contact> listContactsExterne;
    private transient YousignApiService service;
    private YousignFileSettingsMatcher fileSettingsMatcher = new YousignFileSettingsMatcher();
    private Boolean submitProcedureNotStarted = false;
    private String lastPosition = '42,711,192,791';
    private Integer positionNumber = 1;
    private YousignContactFieldHelper contactFieldHelper = new YousignContactFieldHelper();
    
    public YousignSubmitDuplicateProcess(
        Yousign_Procedure__c yousignProcedure,
        List<Yousign_Signatory__c> selectedSignatoriesExt,
        String contactsExterne,
        Boolean submitProcedureNotStarted,
        String yousignTemplateId,
        Boolean sendFiles
    ) {
        this.sendFiles = sendFiles;
        this.yousignProcedure = yousignProcedure;
        this.yousignTemplateId = yousignTemplateId;
        this.selectedSignatoriesExt = selectedSignatoriesExt;
        this.contactsExterne = contactsExterne;
        if (contactsExterne != '') {
            this.listContactsExterne = (List<Contact>) System.JSON.deserialize(
                contactsExterne,
                List<Contact>.class
            );
        } else {
            this.listContactsExterne = null;
        }
        this.submitProcedureNotStarted = submitProcedureNotStarted;
    }
    public void execute(QueueableContext context) {
        this.service = getEnvironment();
        if (this.sendFiles) {
            List<Yousign_Submitted_Document__c> relatedDocuments = getNotSentDocuments();
            this.sendFiles(relatedDocuments);
            YousignApiService.ProcedureUpdate updateProcedure = new YousignApiService.ProcedureUpdate(
                this.yousignProcedure.Id,
                this.yousignProcedure.Yousign_Procedure_Id__c
            );
            if (!this.submitProcedureNotStarted) {
                updateProcedure.getProcedureInput().start = true;
            }else{
                updateProcedure.getProcedureInput().start = false;
            }
            this.service.updateProcedure(updateProcedure);
            if (!this.submitProcedureNotStarted) {
                update new Yousign_Procedure__c(
                    Id = this.yousignProcedure.Id,
                    Status__c = 'Submitted'
                );
            }
            update relatedDocuments;
            ysign__Yousign_Procedure_Event__e okEvent = new ysign__Yousign_Procedure_Event__e(
                ysign__Object_Id__c = yousignProcedure.ysign__ObjectReference__c,
                ysign__Procedure_Id__c = yousignProcedure.Id,
                ysign__Status__c = yousignProcedure.Status__c,
                ysign__Yousign_Procedure_Id__c = yousignProcedure.Yousign_Procedure_Id__c
            );
            Database.SaveResult eventSaveResult = Eventbus.publish(okEvent);
        } else {
            this.sendSignSubmission();
        }
    }
    private void sendSignSubmission() {
        YousignApiService.DuplicateSubmission duplicate = new YousignApiService.DuplicateSubmission(
            this.yousignProcedure.Id,
            this.yousignTemplateId
        );
        duplicate.getProcedureInput().start = false;
        this.service.duplicateProcedure(duplicate);
        List<Yousign_Signatory__c> listSignatories = [
            SELECT
            Id,
            Email__c,
            Contact__r.FirstName,
            Contact__r.LastName,
            Phone__c,
            TemplateRole__c,
            Yousign_Signatory_Id__c
            FROM Yousign_Signatory__c
            WHERE Yousign_Procedure__c = :this.yousignProcedure.Id
        ];
        for (Yousign_Signatory__c yss : listSignatories) {
            Boolean isCreate = true;
            if(duplicate.procedureOutput.members != null ){
                for (
                    YousignApiModel.MemberOutput mo : duplicate.procedureOutput.members
                ) {
                    system.debug('yss===>'+yss);
                    system.debug('yss===>'+yss.TemplateRole__c);
                    if (yss.TemplateRole__c != null && yss.TemplateRole__c == mo.parent) {
                        isCreate = false;
                        YousignApiModel.MemberUpdateInput mi = new YousignApiModel.MemberUpdateInput();
                        mi.procedure = duplicate.procedureOutput.id;
                        mi.firstname = yss.Contact__r.FirstName;
                        mi.lastname = yss.Contact__r.LastName;
                        mi.email = yss.Email__c;
                        mi.phone = yss.Phone__c;
                        YousignApiService.UpdateMemberSubmission ums = new YousignApiService.UpdateMemberSubmission(
                            mi,
                            mo.id
                        );
                        this.service.updateMemberSubmission(ums);
                        yss.Yousign_Signatory_Id__c = mo.id;
                    }
                }
            }
            if (isCreate) {
                YousignApiModel.MemberUpdateInput mi = new YousignApiModel.MemberUpdateInput();
                mi.procedure = duplicate.procedureOutput.id;
                mi.firstname = yss.Contact__r.FirstName;
                mi.lastname = yss.Contact__r.LastName;
                mi.email = yss.Email__c;
                mi.phone = yss.Phone__c;
                YousignApiService.CreateMemberSubmission cms = new YousignApiService.CreateMemberSubmission(
                    mi
                );
                this.service.createMemberSubmission(cms);
                yss.Yousign_Signatory_Id__c = cms.memberOutput.id;
            }
        }
        update listSignatories;
        this.yousignProcedure.Yousign_Procedure_Id__c=duplicate.procedureOutput.id;
        update new Yousign_Procedure__c(
            Id = this.yousignProcedure.Id,
            Status__c='Created',
            Yousign_Procedure_Id__c = duplicate.procedureOutput.id
        );
        if (!Test.isRunningTest()) {
            System.enqueueJob(
                new YousignSubmitDuplicateProcess(
                    this.yousignProcedure,
                    this.selectedSignatoriesExt,
                    '',
                    this.submitProcedureNotStarted,
                    this.yousignTemplateId,
                    true
                )
            );
        }
    }
    private List<Yousign_Submitted_Document__c> getNotSentDocuments() {
        List<Schema.SObjectField> yousignSubmittedDocumentFields = new List<Schema.SObjectField>{
            Yousign_Submitted_Document__c.Document_Id__c,
                Yousign_Submitted_Document__c.Yousign_Document_Id__c,
                Yousign_Submitted_Document__c.Document_Name__c
                };
                    DMLManager.FLSCheckResultKeeper yousignSubmittedDocumentFLSCheckResult = new DMLManager()
                    .getFLSCheckResult(
                        yousignSubmittedDocumentFields,
                        Yousign_Submitted_Document__c.getSObjectType()
                    );
        if (!yousignSubmittedDocumentFLSCheckResult.areAllSelectedFieldsAvailable) {
            System.debug(
                LoggingLevel.ERROR,
                'getNotSentDocuments - yousignSubmittedDocumentFLSCheckResult.areAllSelectedFieldsAvailable'
            );
        }
        List<Yousign_Submitted_Document__c> relatedDocuments = [
            SELECT Id, Document_Id__c, Document_Name__c, Yousign_Document_Id__c
            FROM Yousign_Submitted_Document__c
            WHERE
            Yousign_Document_Id__c = NULL
            AND Yousign_Procedure__c = :this.yousignProcedure.Id
        ];
        
        return relatedDocuments;
    }
    private void sendFiles(
        List<Yousign_Submitted_Document__c> submittedDocuments
    ) {
        Set<Id> documentIds = new Set<Id>();
        for (Yousign_Submitted_Document__c document : submittedDocuments) {
            documentIds.add(document.ysign__Document_Id__c);
        }
        Map<Id, ContentVersion> mapLatestContentVersion = YousignConnectionService.getLatestContentVersions(
            documentIds
        );
        for (Yousign_Submitted_Document__c document : submittedDocuments) {
            ContentVersion latestContentVersion = mapLatestContentVersion.get(
                document.Document_Id__c
            );
            if (latestContentVersion == null) {
                continue;
            }
            YousignApiService.FileSubmission fileToInsert = new YousignApiService.FileSubmission(
                latestContentVersion,
                this.yousignProcedure.ysign__Yousign_Procedure_Id__c
            );
            this.loadFile(document, fileToInsert);
        }
    }
    private void loadFile(
        Yousign_Submitted_Document__c submittedDocument,
        YousignApiService.FileSubmission fileToInsert
    ) {
        try {
            YousignConnectionService.loadFile(this.service, fileToInsert);
            submittedDocument.Yousign_Document_Id__c = fileToInsert.fileOutput.id;
            submittedDocument.Status__c = 'Sent';
        } catch (YousignConnectionService.YousignConnectionException e) {
            submittedDocument.Error_Message__c = e.getMessage();
            submittedDocument.Status__c = 'Failed';
        }
        submittedDocument.Response__c = fileToInsert.getBody();
    }
    public static YousignApiService getEnvironment() {
        YousignApiService service;
        Yousign_setting__c settings = Yousign_setting__c.getOrgDefaults();
        if (settings != null && !String.isEmpty(settings.Environment__c)) {
            if (settings.Environment__c == 'PRODUCTION') {
                service = new YousignApiService(
                    YousignApiService.Environment.PRODUCTION
                );
            } else {
                service = new YousignApiService(YousignApiService.Environment.STAGING);
            }
        } else {
            service = new YousignApiService(YousignApiService.Environment.STAGING);
        }
        
        return service;
    }
}