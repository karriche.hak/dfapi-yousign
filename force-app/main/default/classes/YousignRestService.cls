@RestResource(urlMapping='/YousignProcedure/*')
global without sharing class YousignRestService {
//RG 24.04.2021 
    private static final Map<String, RequestProcessor> eventProcessorMapping = new Map<String, RequestProcessor>();
    private static String Yousign_Event_not_supported = Label.Yousign_Event_not_supported;
    private Static String Yousign_Procedure_Not_Found = Label.Yousign_Procedure_Not_Found;
    private static String Yousign_Signatory_not_found = Label.Yousign_Signatory_not_found;
    
    static {
        eventProcessorMapping.put('procedure.finished', new ProcedureFinishedRequest());
        eventProcessorMapping.put('procedure.refused', new ProcedureRefusedRequest());
        eventProcessorMapping.put('member.finished', new MemberFinishedRequest());
        eventProcessorMapping.put('procedure.started', new ProcedureStartedRequest());
    }

    @HttpPost
    global static void doPost() {
            YousignRequest request =
                    (YousignRequest) JSON.deserialize(RestContext.request.requestBody.toString(), YousignRequest.class);
            RequestProcessor processor = getProcessor(request);
            processor.process(request);
    }
    
    @HttpGet
    global static void doGet() {
        
    }

    public without sharing class YousignRequest {
        private YousignApiModel.ProcedureOutput procedure;
        private YousignApiModel.MemberOutput member;
        private String comment;
        private String eventName;

        public YousignApiModel.ProcedureOutput getProcedure() {
            return this.procedure;
        }

        public YousignApiModel.MemberOutput getMember() {
            return this.member;
        }

        public String getComment() {
            return this.comment;
        }

        public String getEventName() {
            return this.eventName;
        }

        public YousignRequest() {}

    }

    private static RequestProcessor getProcessor(YousignRequest request) {
        RequestProcessor processor;
        
        if (eventProcessorMapping.containsKey(request.eventName)) {
            processor = eventProcessorMapping.get(request.eventName);
        } else {
            throw new YousignRestServiceException(Yousign_Event_not_supported);
        }

        return processor;
    }

    private abstract without sharing class RequestProcessor {
        protected DMLManager dmlManager = new DMLManager();

        public RequestProcessor() {}

        protected String getProcedureId(YousignApiModel.ProcedureOutput procedure) {
            String procedureId = procedure.id;
            if (!procedureId.contains('procedures')) {
                procedureId = '/procedures/' + procedureId;
            }

            return procedureId;
        }

        protected Yousign_Procedure__c getRelatedProcedure(String externalProcedureId) {
            List<Schema.SObjectField> yousignProcedeureFields = new List<Schema.SObjectField>{Yousign_Procedure__c.Status__c};
            DMLManager.FLSCheckResultKeeper yousignSignatoryFLSCheckResult = new DMLManager().getFLSCheckResult(yousignProcedeureFields, Yousign_Procedure__c.getSObjectType());
            if (!yousignSignatoryFLSCheckResult.areAllSelectedFieldsAvailable) {
                throw new YousignRestServiceException(yousignSignatoryFLSCheckResult.errorText);
            }

            List<Yousign_Procedure__c> procedures = [
                    SELECT Id
                        , Status__c
                        , ObjectReference__c
                    FROM Yousign_Procedure__c
                    WHERE Yousign_Procedure_Id__c = :externalProcedureId
                    LIMIT 1
            ];

            if (procedures.isEmpty()) throw new YousignRestServiceException(Yousign_Procedure_Not_Found);

            return procedures.get(0);
        }

        protected Yousign_Procedure__c changeProcedureStatus(YousignRequest request, String status) {
            String procedureExternalId = this.getProcedureId(request.getProcedure());
            Yousign_Procedure__c procedure = this.getRelatedProcedure(procedureExternalId);
            procedure.Status__c = status;

            return procedure;
        }

        public abstract void process(YousignRequest request);
    }
    
    private without sharing class ProcedureStartedRequest extends RequestProcessor {

        public ProcedureStartedRequest() {
            super();
        }

        override public void process(YousignRequest request) {
            
            
            String procedureExternalId = this.getProcedureId(request.getProcedure());
            Yousign_Procedure__c procedure = this.getRelatedProcedure(procedureExternalId);
            YousignApiModel.ProcedureFileOutput[] files=request.getProcedure().files;
          
           
            YousignExternalFileLoader fileLoader = new YousignExternalFileLoader(procedure,files);
            System.enqueueJob(fileLoader);
            
        }
    }
    
    private without sharing class ProcedureFinishedRequest extends RequestProcessor {

        public ProcedureFinishedRequest() {
            super();
        }

        override public void process(YousignRequest request) {
            
            Yousign_Procedure__c procedure = this.changeProcedureStatus(request, 'Signed');
            procedure.Date_of_signature__c = Date.today();
           
            //load signed files
            YousignFileLoader fileLoader = new YousignFileLoader(procedure);
            System.enqueueJob(fileLoader);
            
            this.dmlManager.updateAsUser(procedure);
        }
    }

    private without sharing class ProcedureRefusedRequest extends RequestProcessor {

        public ProcedureRefusedRequest() {
            super();
        }

        override public void process(YousignRequest request) {
            Yousign_Procedure__c procedure = this.changeProcedureStatus(request, 'Rejected');

            this.dmlManager.updateAsUser(procedure);
        }
    }

    private without sharing class MemberFinishedRequest extends RequestProcessor {

        public MemberFinishedRequest() {
            super();
        }
        
        override public void process(YousignRequest request) {
            String procedureExternalId = this.getProcedureId(request.getProcedure());
            Yousign_Procedure__c procedure = this.getRelatedProcedure(procedureExternalId);
            String signatoryId = this.getMemberId(request.getMember());
            List<Yousign_Signatory__c> signatories = getSignatory(procedure.Id, signatoryId);
            if (signatories.isEmpty()) CreateContactSignatory(procedure.Id,request.getMember(),signatoryId);
            else {
                Yousign_Signatory__c signatory=signatories.get(0);
                signatory.Status__c = 'Signed';
                this.dmlManager.updateAsUser(signatory);
            }
        }
        
        private void CreateContactSignatory(Id procedureId,YousignApiModel.MemberOutput member,String signatoryId) {
            Contact cont=CreateContact(member);
            createSignatorySigned(procedureId,cont,signatoryId,member);
        }
        private Contact CreateContact(YousignApiModel.MemberOutput member) {
            Contact cont=new Contact(FirstName=member.firstName ,LastName=member.lastName , Email=member.email , Phone=member.phone);
            Insert cont;
            return cont;
        }
        
        private void createSignatorySigned(Id procedureId,Contact cont,String signatoryId,YousignApiModel.MemberOutput member){
            Yousign_Signatory__c Signatory=new Yousign_Signatory__c(Yousign_Procedure__c=procedureId,Contact__c=cont.id
                                        ,ysign__Order__c=integer.valueof(member.position),Yousign_Signatory_Id__c=signatoryId, 
                                        Role__c='Signatory',Status__c='Signed');
            Insert Signatory;
            
        }
        private List<Yousign_Signatory__c> getSignatory(Id procedureId, String signatoryId) {
            List<Schema.SObjectField> yousignSignatoryFields = new List<Schema.SObjectField>{Yousign_Signatory__c.Status__c};
            DMLManager.FLSCheckResultKeeper yousignSignatoryFLSCheckResult = new DMLManager().getFLSCheckResult(yousignSignatoryFields , Yousign_Signatory__c.getSObjectType());
            if (!yousignSignatoryFLSCheckResult.areAllSelectedFieldsAvailable) {
                throw new YousignRestServiceException(yousignSignatoryFLSCheckResult.errorText);
            }
            List<Yousign_Signatory__c> signatories = [
                    SELECT Id
                        , Status__c
                    FROM Yousign_Signatory__c
                    WHERE Yousign_Procedure__c = :procedureId
                        AND Yousign_Signatory_Id__c = :signatoryId
                    LIMIT 1
            ];

            //if (signatories.isEmpty()) throw new YousignRestServiceException(Yousign_Signatory_not_found);

            //return signatories.get(0);
            return signatories;
        }

        private String getMemberId(YousignApiModel.MemberOutput member) {
            String memberId = member.id;
            if (!memberId.contains('members')) {
                memberId = '/members/' + memberId;
            }

            return memberId;
        }
    }

    public class YousignRestServiceException extends Exception {}
}