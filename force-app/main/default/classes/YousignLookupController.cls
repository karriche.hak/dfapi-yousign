public with sharing class YousignLookupController {
    @AuraEnabled
    public static List<sObject> fetchLookUpValues(String searchKeyWord, String objectName, List<sObject> selectedRecords) {
        String searchKey = '%' + searchKeyWord + '%';
        List <sObject> returnList = new List <sObject> ();
        Set<Id> selectedRecordsIds = new Set<Id>();
        for(sObject selectedRecord : selectedRecords){
            selectedRecordsIds.add(selectedRecord.Id);
        }
        List<Schema.SObjectField> contactFields = new List<Schema.SObjectField>{Contact.Id, Contact.FirstName, Contact.Name, Contact.LastName, Contact.Email, Contact.Phone, Contact.MobilePhone};
        DMLManager.FLSCheckResultKeeper contactFLSCheckResult = new DMLManager().getFLSCheckResult(contactFields , Contact.getSObjectType());
        if (!contactFLSCheckResult.areAllSelectedFieldsAvailable) {
            throw new AuraHandledException(contactFLSCheckResult.errorText);
        }

        String query =  'SELECT id';
        query += ', FirstName';
        query += ', Name';
        query += ', LastName';
        query += ', Email';
        query += ', Phone';
        query += ', MobilePhone';
        query += ' FROM ' + objectName;
        query += ' WHERE Name LIKE: searchKey';
        query += ' AND Id NOT IN :selectedRecordsIds ';
        query += ' AND Email != null ';
        query += ' AND (Phone != null OR MobilePhone != null)';
        query += ' ORDER BY createdDate DESC ';
        query += ' LIMIT 5 ';
        returnList = Database.query(query);
        return returnList;
    }

    private final static Integer MAX_RESULTS = 5;

    @AuraEnabled(Cacheable=true)
    public static List<LookupSearchResult> searchForContacts(String searchTerm, List<String> selectedIds) {
        YousignContactFieldHelper contactFieldHelper = new YousignContactFieldHelper();
        return contactFieldHelper.searchForContact(searchTerm, selectedIds);
    }

    @AuraEnabled
    public static LookupSearchResult getRelatedContact(String objectApiName, Id recordId, String fieldApiName) {
        if (String.isBlank(objectApiName) || recordId == null) {
            return null;
        }

        YousignContactFieldHelper contactFieldHelper = new YousignContactFieldHelper();
        Contact contactResult = null;

        if (objectApiName != 'Opportunity') {
            if (String.isBlank(fieldApiName)) {
                return null;
            }
            SObjectType objectType = Schema.getGlobalDescribe().get(objectApiName);
            if (objectType == null) {
                // Our object doesn't exist
                return null;
            }
            Map<String,Schema.SObjectField> mfields = objectType.getDescribe().fields.getMap();
            Schema.SObjectField lookupContactField = mfields.get(fieldApiName);
            if (lookupContactField == null) {
                // Our object doesn't contain our field
                return null;
            }
            DescribeFieldResult lookupContactFieldDescribeResult = lookupContactField.getDescribe();
            List<SObjectType> refObjs = lookupContactFieldDescribeResult.getReferenceTo();
            Boolean isLookupToContact = false;
            for (SObjectType sObjType : refObjs) {
                if (sObjType == Contact.getSObjectType()) {
                    isLookupToContact = true;
                    break;
                }
            }
            if (!isLookupToContact) {
                // We have to check that our referenced field is really a lookup to Contact SObject.
                return null;
            }

            if (!lookupContactFieldDescribeResult.isAccessible()) {
                throw new AuraHandledException('Specified contact lookup field isn\'t accessible.');
            }

            String lookupRelationShipName = lookupContactFieldDescribeResult.getRelationshipName();

            contactResult = contactFieldHelper.getSpecificContact(recordId, lookupRelationShipName, objectType.getDescribe().getName(), false);
        } else {
            DMLManager dmlManager = new DMLManager();
            // In case we have to load the Opportunity Contact (Principal), we have to query OpportunityContactRole
            List<Schema.SObjectField> oppContactFields = new List<Schema.SObjectField>{OpportunityContactRole.ContactId, OpportunityContactRole.OpportunityId, OpportunityContactRole.IsPrimary };
            DMLManager.FLSCheckResultKeeper oppContactFLSCheckResult = dmlManager.getFLSCheckResult(oppContactFields , OpportunityContactRole.getSObjectType());
            if (!oppContactFLSCheckResult.areAllSelectedFieldsAvailable) {
                throw new AuraHandledException(oppContactFLSCheckResult.errorText);
            }

            List<OpportunityContactRole> contactsFromOpportunity = [
                SELECT ContactId
                FROM OpportunityContactRole
                WHERE IsPrimary = true
                AND OpportunityId = :recordId
            ];
            
            if (contactsFromOpportunity.isEmpty()) {
                return null;
            }
            
            contactResult = contactFieldHelper.getSpecificContact(contactsFromOpportunity.get(0).ContactId, null, null, null);
        }
        
        String validPhone = (String.isNotBlank(contactResult.Phone) ? contactResult.Phone : contactResult.MobilePhone);

        if (
            String.isBlank(contactResult.Email) ||
            String.isBlank(validPhone)
        ) {
            throw new AuraHandledException(Label.Yousign_MissingContactInformation);
        }

        return new LookupSearchResult(
            contactResult.Id,
            Contact.getSObjectType().getDescribe().getName(),
            'standard:contact',
            contactResult.Name, contactResult.Email + ' - ' + validPhone,
            contactResult.getPopulatedFieldsAsMap()
        );
    }
}