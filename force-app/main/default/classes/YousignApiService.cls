public with sharing class YousignApiService {

    public enum Environment {
        PRODUCTION,
        STAGING
    }

    private final YousignApiCore yousignApi;
    private final ApiKeyManager apiKeyManager;
    private static String Yousign_Procedure_Id_not_specified = Label.Yousign_Procedure_Id_not_specified;
    private static String Yousign_Document_Id_not_specified = Label.Yousign_Document_Id_not_specified;
    private static String Yousign_Api_Key_Not_Specified = Label.Yousign_Api_Key_Not_Specified;

    /*===============================================================*/
    /* Public Interface */
    /*===============================================================*/
    public YousignApiService () {
        system.debug(System.Label.Yousign_All_Files_Weight_Error_Message
                            + System.Label.Yousign_Api_Key_Component_Cancel_Button
                            + System.Label.Yousign_Api_Key_Component_Edit_Button
                            + System.Label.Yousign_Api_Key_Component_Field_Is_Required_Message
                            + System.Label.Yousign_Api_Key_Component_Save_Button
                            + System.Label.Yousign_Api_Key_Not_Specified
                            + System.Label.Yousign_Api_Key_Page_Blank_Apikey_Error
                            + System.Label.Yousign_Api_Key_Page_Blank_Site_Url_Error
                            + System.Label.Yousign_Api_Key_Page_Cancel_Button
                            + System.Label.Yousign_Api_Key_Page_Edit_Button
                            + System.Label.Yousign_Api_Key_Page_Save_Button
                            + System.Label.Yousign_Api_Key_Page_Subtitle
                            + System.Label.Yousign_Api_Key_Page_Success_Message
                            + System.Label.Yousign_Api_Key_Page_Title
                            + System.Label.Yousign_Api_Key_Page_Value_Not_Inputed_Error
                            + System.Label.Yousign_At_Least_One_Signer_Message
                            + System.Label.Yousign_Back_Button
                            + System.Label.Yousign_Cancel_Button
                            + System.Label.Yousign_Cancel_Success_Message
                            + System.Label.Yousign_Contact_Lookup_Label
                            + System.Label.Yousign_Document_Id_not_specified
                            + System.Label.Yousign_Document_Page_Message
                            + System.Label.Yousign_Draft_Button
                            + System.Label.Yousign_Draft_Success_Message
                            + System.Label.Yousign_Event_not_supported
                            + System.Label.Yousign_File_Not_Selected_Message
                            + System.Label.Yousign_File_Picklist_Label
                            + System.Label.Yousign_Form_Button_Add_Signatory
                            + System.Label.Yousign_Form_Email
                            + System.Label.Yousign_Form_Example_Email
                            + System.Label.Yousign_Form_Example_Phone
                            + System.Label.Yousign_Form_First_Name
                            + System.Label.Yousign_Form_Last_Name
                            + System.Label.Yousign_Form_Phone
                            + System.Label.Yousign_LC_Lookup_No_Results_Found
                            + System.Label.Yousign_LC_Lookup_Placeholder
                            + System.Label.Yousign_LC_Title
                            + System.Label.Yousign_One_File_Weight_Error_Message
                            + System.Label.Yousign_Order_Contacts
                            + System.Label.Yousign_Procedure_Creation
                            + System.Label.Yousign_Procedure_Id_not_specified
                            + System.Label.Yousign_Procedure_Not_Found
                            + System.Label.Yousign_Public_Site_Link_Not_Specified
                            + System.Label.Yousign_Received_file_is_empty
                            + System.Label.Yousign_Reminder_Nb_Days
                            + System.Label.Yousign_Reminder_Procedure
                            + System.Label.Yousign_Request_Success_Message
                            + System.Label.Yousign_Request_Success_Title
                            + System.Label.Yousign_Signatory_not_found
                            + System.Label.Yousign_Signatory_Page_Message
                            + System.Label.Yousign_Submit_Button
                            + System.Label.Yousign_Test_DML_Name
                            + System.Label.Yousign_VFPage_Add_New_External_Contact_button
                            + System.Label.Yousign_VFPage_Add_New_Signer_button
                            + System.Label.Yousign_VFPage_Already_Chosen_Signer_Message
                            + System.Label.Yousign_VFPage_Attacment_Not_Selected_Error
                            + System.Label.Yousign_Wait);

        Yousign_setting__c settings = Yousign_setting__c.getOrgDefaults();
        if (settings != null && !String.isEmpty(settings.Environment__c)) {
                if (settings.Environment__c == 'PRODUCTION'){
                    this(Environment.PRODUCTION);
                }else {
                    this(Environment.STAGING);
                }
        } else {
            this(Environment.STAGING);
        }
    }

    public YousignApiService (Environment environment) {
        this.apiKeyManager = new ApiKeyManager();
        String apiKey = apiKeyManager.getApiKey();
        this.yousignApi = new YousignApiCore(apiKey, environment);
    }

    /* submission methods */
    public void createProcedure(ProcedureSubmission procedureSubmission) {
        this.yousignApi.createProcedure(procedureSubmission);
    }

    public void updateProcedure(ProcedureUpdate procedureSubmission) {
        this.yousignApi.updateProcedure(procedureSubmission);
    }

    public void getProcedure(ProcedureReadSubmission procedureReadSubmission) {
        this.yousignApi.getProcedure(procedureReadSubmission);
    }

    public void duplicateProcedure(DuplicateSubmission duplicateProcedure) {
        this.yousignApi.duplicateProcedure(duplicateProcedure);
    }
    
    public void updateMemberSubmission(UpdateMemberSubmission updateMember){
        this.yousignApi.updateMemberSubmission(updateMember);
    }

    public void createMemberSubmission(CreateMemberSubmission createMember){
        this.yousignApi.createMemberSubmission(createMember);
    }

    public void getTemplates(TemplateListSubmission templateSubmission) {
        this.yousignApi.getTemplates(templateSubmission);
    }

    public void createFile(FileSubmission fileSubmission) {
        this.yousignApi.createFile(fileSubmission);
    }

    public void cancelProcedure(CancellationSubmission cancellationSubmission) {
        this.yousignApi.cancelProcedure(cancellationSubmission);
    }

    public void loadFile(FileDownloadSubmission fileDownloadSubmission) {
        this.yousignApi.loadFile(fileDownloadSubmission);
    }

    /*===============================================================*/
    /* Submission wrappers */
    /*===============================================================*/
    /* Basic class, holds response */
    public abstract class Submission {

        public final String endpointPostfix {
            get; protected set;
        }
        public final String requestMethod {
            get; protected set;
        }
        public YousignApiModel.YousignApiObject apiInputObject {
            get; protected set;
        }
        protected HttpResponse response;

        /*public interface : response and status getters*/
        public Integer getStatusCode() {
            return response.getStatusCode();
        }

        public String getStatus() {
            return response.getStatus();
        }

        public virtual String getRequestBody() {
            return this.apiInputObject.toJson();
        }

        public String getBody() {
            System.debug('******RG response.getBody() : '+response.getBody());
            System.debug('******RG response.getBody() .replaceAll: '+response.getBody().replaceAll('"limit":','"limite":'));
            return response.getBody().replaceAll('"limit":','"limite":');
        }

        public Boolean isSuccess() {
            return response != null
                    && (
                    response.getStatusCode() == 200
                            || response.getStatusCode() == 201
                            || response.getStatusCode() == 202
                            || response.getStatusCode() == 203
                            || response.getStatusCode() == 204
            );
        }

        /*public interface : response setter*/
        public void setResponse(HttpResponse response) {
            this.response = response;
            if (!this.isSuccess()) {
                return;
            }
            this.parseResponse();
        }

        /*abstract interface*/
        public abstract void parseResponse();
    }

    /* Wrapper for procedures read */
    public class ProcedureReadSubmission extends Submission {
        public YousignApiModel.ProcedureOutput procedureOutput {get; private set;}
        public ProcedureReadSubmission(String procedureId) {
            this.endpointPostfix = procedureId;
            this.requestMethod = 'GET';
        }
        public override String getRequestBody() {
            return null;
        }
        public override void parseResponse() {
            this.procedureOutput = (YousignApiModel.ProcedureOutput) JSON.deserialize(this.getBody(), YousignApiModel.ProcedureOutput.class);
        }
    }

    /* Wrapper for procedures template */
    public class TemplateListSubmission extends Submission {
        public List<YousignApiModel.ProcedureOutput> listProcedureOutput {get; private set;}
        public TemplateListSubmission() {
            this.endpointPostfix = '/procedures?template=true';
            this.requestMethod = 'GET';
        }
        public override String getRequestBody() {
            return null;
        }
        public override void parseResponse() {
            this.listProcedureOutput = (List<YousignApiModel.ProcedureOutput>) JSON.deserialize(this.getBody(), List<YousignApiModel.ProcedureOutput>.class);
        }
    }
    public class UpdateMemberSubmission extends Submission {
        public YousignApiModel.MemberUpdateInput memberOutput {get; private set;}
        public UpdateMemberSubmission(YousignApiModel.MemberUpdateInput memberInput, String memberId) {
            this.endpointPostfix = memberId;
            this.requestMethod = 'PUT';
            this.apiInputObject = memberInput;
        }
        public override void parseResponse() {
            this.memberOutput = (YousignApiModel.MemberUpdateInput) JSON.deserialize(this.getBody(), YousignApiModel.MemberUpdateInput.class);
        }
    }
    public class CreateMemberSubmission extends Submission {
        public YousignApiModel.MemberOutput memberOutput {get; private set;}
        public CreateMemberSubmission(YousignApiModel.MemberUpdateInput memberInput) {
            this.endpointPostfix = '/members';
            this.requestMethod = 'POST';
            this.apiInputObject = memberInput;
        }
        public override void parseResponse() {
            this.memberOutput = (YousignApiModel.MemberOutput) JSON.deserialize(this.getBody(), YousignApiModel.MemberOutput.class);
        }
    }
    /* Wrapper for duplicate submission */
    public class DuplicateSubmission extends Submission {
        public YousignApiModel.ProcedureOutput procedureOutput {get; private set;}
        private Integer memberCounter;
        public DuplicateSubmission(String procedureId, String templateId) {
            this.endpointPostfix = templateId+'/duplicate';
            this.requestMethod = 'POST';
            this.apiInputObject = new YousignApiModel.ProcedureInput(procedureId);
        }
        public void addMember(Contact contact) {
            this.addMember(contact, new List<FileToSign>{ });
        }
        public void addMember(Contact contact, FileToSign fileToSign) {
            this.addMember(contact, new List<FileToSign>{fileToSign});
        }
        public void addMember(Contact contact, List<FileToSign> filesToSign) {
            this.getProcedureInput().addMemeber(0, contact, filesToSign);
        }
        public void addMember(Contact contact, Integer pos, FileToSign fileToSign) {
            this.getProcedureInput().addMemeber(pos, contact, new List<FileToSign>{fileToSign});
        }
        public override void parseResponse() {
            this.procedureOutput = (YousignApiModel.ProcedureOutput) JSON.deserialize(this.getBody(), YousignApiModel.ProcedureOutput.class);
        }
        public YousignApiModel.ProcedureInput getProcedureInput() {
            return (YousignApiModel.ProcedureInput)this.apiInputObject;
        }
    }

    /* Wrapper for procedures submission */
    public class ProcedureSubmission extends Submission {

        public YousignApiModel.ProcedureOutput procedureOutput {get; private set;}
        private Integer memberCounter;

        public ProcedureSubmission(String procedureId) {
            this.endpointPostfix = '/procedures';
            this.requestMethod = 'POST';
            this.apiInputObject = new YousignApiModel.ProcedureInput(procedureId);
        }

        public void addMember(Contact contact) {
            this.addMember(contact, new List<FileToSign>{ });
        }

        public void addMember(Contact contact, FileToSign fileToSign) {
            this.addMember(contact, new List<FileToSign>{fileToSign});
        }

        public void addMember(Contact contact, List<FileToSign> filesToSign) {
            this.getProcedureInput().addMemeber(0, contact, filesToSign);
        }

        public void addMember(Contact contact, Integer pos, FileToSign fileToSign) {
            this.getProcedureInput().addMemeber(pos, contact, new List<FileToSign>{fileToSign});
        }

        public override void parseResponse() {
            this.procedureOutput = (YousignApiModel.ProcedureOutput) JSON.deserialize(this.getBody(), YousignApiModel.ProcedureOutput.class);
        }

        public YousignApiModel.ProcedureInput getProcedureInput() {
            return (YousignApiModel.ProcedureInput)this.apiInputObject;
        }
    }
    /* Wrapper for procedures submission */
    public class ProcedureUpdate extends Submission {

        public YousignApiModel.ProcedureOutput procedureOutput {get; private set;}
        private Integer memberCounter;

        public ProcedureUpdate(String procedureId, String yousignProcedureId) {
            this.endpointPostfix = yousignProcedureId;
            this.requestMethod = 'PUT';
            this.apiInputObject = new YousignApiModel.ProcedureUpdate(true,procedureId);
        }
        public override void parseResponse() {
            this.procedureOutput = (YousignApiModel.ProcedureOutput) JSON.deserialize(this.getBody(), YousignApiModel.ProcedureOutput.class);
        }
        public YousignApiModel.ProcedureUpdate getProcedureInput() {
            return (YousignApiModel.ProcedureUpdate)this.apiInputObject;
        }
    }

    /* Procedures helper wrapper, which holds a file
       that should be submitted as a part of the process
    */
    public class FileToSign extends YousignApiModel.FileObjectInput {

        public FileToSign() {

        }

        public FileToSign (FileSubmission file, Integer page, String position) {
            this(file.fileOutput.Id, page, position, null, null);
        }

        public FileToSign (FileSubmission file, Integer page, String position, String fieldName) {
            this(file.fileOutput.Id, page, position, fieldName, null);
        }

        public FileToSign (FileSubmission file, Integer page, String position, String fieldName, String mention) {
            this(file.fileOutput.Id, page, position, fieldName, mention);
        }

        public FileToSign (String file, Integer page, String position) {
            this(file, page, position, null, null);
        }

        public FileToSign (String file) {
            this(file, 1, null, null, null);
        }

        public FileToSign (String file, Integer page, String position, String fieldName) {
            this(file, page, position, fieldName, null);
        }

        public FileToSign (String file, Integer page, String position, String fieldName, String mention) {
            this(file, page, position, fieldName, null, null);
        }

        public FileToSign (String file, Integer page, String position, String fieldName, String mention, String mention2) {
            this(file, page, position, fieldName, null, null, null);
        }

        public FileToSign (String file, Integer page, String position, String fieldName, String mention, String mention2, Integer fontSize) {
            this.file       = file;
            this.page       = page;
            this.position   = position;
            this.fieldName  = fieldName;
            this.mention    = mention;
            this.mention2   = mention2;
            this.fontSize   = fontSize;
        }

        public String setPosition(String lastPosition) {
            System.debug('last position = '+lastPosition);
            List<String> listlastPosition = lastPosition.split(',');
            Integer llx = Integer.valueOf(listlastPosition[0]);
            Integer lly = Integer.valueOf(listlastPosition[1]);
            Integer urx = Integer.valueOf(listlastPosition[2]);
            Integer ury = Integer.valueOf(listlastPosition[3]);
            if (urx>450){
                urx = 192;
                llx = 42;
                lly -= 110;
                ury -= 110;
            } else {
                urx += 180;
                llx += 180;
            }
            return (llx+','+lly+','+urx+','+ury);
        }
    }

    /* Wrapper for submitting new files */
    public class FileSubmission extends Submission {

        public final ContentVersion file {get; set;}
        public YousignApiModel.FileOutput fileOutput {get; private set;}

        public FileSubmission(ContentVersion file) {
            this(file, null, null, null);
        }

        public FileSubmission(ContentVersion file, String procedure) {
            this(file, null, null, procedure);
        }

        public FileSubmission(ContentVersion file, String password, String[] metadata, String procedure) {
            this.endpointPostfix = '/files';
            this.requestMethod = 'POST';
            this.file = file;
            this.apiInputObject = this.buildFileInput(password, metadata, procedure);
        }

        public override void parseResponse() {
            this.fileOutput = (YousignApiModel.FileOutput) JSON.deserialize(this.getBody(), YousignApiModel.FileOutput.class);
        }

        private YousignApiModel.FileInput buildFileInput(String password, String[] metadata, String procedure) {
            YousignApiModel.FileInput result = new YousignApiModel.FileInput();
            result.name = file.Title + (String.isNotBlank(file.FileExtension) ? '.' + file.FileExtension : '');
            result.password = password;
            result.description = file.Description;
            result.metadata = metadata;
            System.debug('file = ');
            System.debug(file.VersionData);
            result.content = EncodingUtil.base64Encode(file.VersionData);
            result.procedure = procedure;
            return result;
        }
    }

    public class CancellationSubmission extends Submission {

        private Yousign_Procedure__c yousignProcedure;

        public CancellationSubmission(Yousign_Procedure__c yousignProcedure) {
            if (String.isEmpty(yousignProcedure.Yousign_Procedure_Id__c) ||
                    !yousignProcedure.Yousign_Procedure_Id__c.contains('/procedures/')) {

                throw new YousignApiServiceException(Yousign_Procedure_Id_not_specified);
            }
            this.yousignProcedure = yousignProcedure;
            this.endpointPostfix = yousignProcedure.Yousign_Procedure_Id__c;
            this.requestMethod = 'DELETE';
        }

        public override void parseResponse() {}

        public override String getRequestBody() {
            return null;
        }
    }

    public class FileDownloadSubmission extends Submission {

        private Yousign_Submitted_Document__c yousignSubmittedDocument;
        private String encodedFileData;

        public String getEncodedFileData() {
            Integer i = this.getBody().length() - 1;
            return this.getBody().substring(1, i).replace('\\', '');
        }

        public FileDownloadSubmission(Yousign_Submitted_Document__c yousignSubmittedDocument) {
            if (String.isEmpty(yousignSubmittedDocument.Yousign_Document_Id__c) ||
                    !yousignSubmittedDocument.Yousign_Document_Id__c.contains('/files/')) {

                throw new YousignApiServiceException(Yousign_Document_Id_not_specified);
            }
            this.yousignSubmittedDocument = yousignSubmittedDocument;
            this.endpointPostfix = this.yousignSubmittedDocument.Yousign_Document_Id__c + '/download';
            this.requestMethod = 'GET';
        }

        public override void parseResponse() {}

        public override String getRequestBody() {
            return null;
        }
    }

    /*===============================================================*/
    /* Api Key Helper */
    /*===============================================================*/
    private class ApiKeyManager {
        public String getApiKey() {
            Yousign_setting__c settings = Yousign_setting__c.getOrgDefaults();
            if (settings == null || String.isEmpty(settings.Api_key__c)) {
                throw new YousignApiService.YousignApiServiceException(Yousign_Api_Key_Not_Specified);
            }
            return Yousign_setting__c.getOrgDefaults().Api_key__c;
        }
    }

    private class YousignApiServiceException extends Exception {}
}