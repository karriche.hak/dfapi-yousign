@IsTest
private class YousignFileLoaderTest {

    @IsTest
    private static void fileLoaderTest() {
 		YousignTestDataFactory.initApiKey();
        Test.setMock(HttpCalloutMock.class, new YousignTestDataFactory.FileDownloadMock());
        Contact testContact = YousignTestDataFactory.createContact();
        Opportunity testOpportunity = YousignTestDataFactory.createOpportunity();
        ContentVersion testContentVersion = YousignTestDataFactory.makeContentVersion(0);
        insert testContentVersion;
        ContentDocument contentDocument = getContentDocument();
        ContentDocumentLink testContentDocumentLink = YousignTestDataFactory.makeContentDocumentLink(testOpportunity.Id,
                contentDocument);
        insert testContentDocumentLink;

        Yousign_Procedure__c testProcedure = createProcedure(testOpportunity.Id);
        Yousign_Signatory__c testSignatory = createSignatory(testProcedure.Id, testContact.Id);
        Yousign_Submitted_Document__c testDocument = createSubmittedDocument(testProcedure.Id, contentDocument.Id);


        RestRequest request = new RestRequest();
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(getEventBody('procedure.started'));
        RestContext.request = request;
        Test.startTest();
        YousignRestService.doPost();
        YousignRestService.doGet();
        YousignRestService.YousignRequest ys=new YousignRestService.YousignRequest();
        ys.getComment();
        ys.getEventName();
        YousignFileLoader floader=new YousignFileLoader(testProcedure);
           
            System.enqueueJob(floader);
          Test.stopTest();
    }
    
    private static ContentDocument getContentDocument() {
        ContentDocument document = [
                SELECT Id, Title FROM ContentDocument
        ];

        return document;
    }
    
    private static Yousign_Procedure__c createProcedure(Id oppId) {
        Yousign_Procedure__c testProcedure = YousignTestDataFactory.makeYousignProcedure(0, oppId);
        testProcedure.Status__c = 'Pending';
        testProcedure.Ordered__c = true;
        testProcedure.Yousign_Procedure_Id__c = '/procedures/079d00be-c1db-4974-ae88-3f026a15d525';

        insert testProcedure;
        return testProcedure;
    }
    
     private static Yousign_Signatory__c createSignatory(Id procedureId, Id contactId) {
        Yousign_Signatory__c signatory = YousignTestDataFactory.makeYousignSignatory(0, procedureId, contactId);
        signatory.Status__c = 'Pending';
        signatory.Contact__c = contactId;
        signatory.Role__c = 'Signatory';
        signatory.Yousign_Signatory_Id__c = '/members/46c503db-2ae4-4a3c-92c2-f66a8819f1bc';

        insert signatory;
        return signatory;
    }
 private static String getEventBody(String eventName) {
        return'{' +
                '"procedure":{  ' +
                '"id":"079d00be-c1db-4974-ae88-3f026a15d525",' +
                '"name":"webhook_test_3.11",' +
                '"description":"Proceudre created from Salesforce.",' +
                '"createdAt":"2017-11-03T10:00:54+01:00",' +
                '"updatedAt":"2017-11-03T10:00:54+01:00",' +
                '"expiresAt":"2017-11-28T00:00:00+01:00",' +
                '"deleted":null,' +
                '"deletedAt":null,' +
                '"status":"finished",' +
                '"creator":null,' +
                '"creatorIp":"109.68.43.206",' +
                '"company":"/companies/b97408ca-496b-450e-a111-b0f0560f5d31",' +
                '"template":false,' +
                '"ordered":true,' +
                '"parent":"",' +
                '"metadata":[],' +
                '"members":[  ' +
                '{  ' +
                ' "id":"/members/46c503db-2ae4-4a3c-92c2-f66a8819f1bc",' +
                '"user":null,' +
                '"type":"signer",' +
                '"firstname":"Roman",' +
                '"lastname":"Prokopenko",' +
                '"email":"prokopenkor5@gmail.com",' +
                '"phone":"+380661145648",' +
                '"position":1,' +
                '"createdAt":"2017-11-03T10:00:54+01:00",' +
                '"updatedAt":"2017-11-03T10:02:05+01:00",' +
                '"status":"done",' +
                '"fileObjects":[  ' +
                '{  ' +
                ' "id":"/file_objects/55ff6e46-26cd-4064-aa7c-f30e24c915ff",' +
                '"file":{  ' +
                '"id":"/files/0acc0fb2-1342-480c-90ff-ddcb9b808182",' +
                '"name":"testYousignFile",' +
                '"type":"signable",' +
                '"contentType":"application/pdf",' +
                '"password":null,' +
                '"description":"",' +
                '"createdAt":"2017-10-02T09:27:18+02:00",' +
                '"updatedAt":"2017-11-03T10:00:54+01:00",' +
                '"sha256":null,' +
                '"metadata":[  ],' +
                '"company":"/companies/b97408ca-496b-450e-a111-b0f0560f5d31",' +
                '"creator":null,' +
                '"fileObjects":null' +
                '},' +
                '"page":1,' +
                '"position":"",' +
                '"fieldName":"",' +
                '"mention":"",' +
                '"mention2":null,' +
                '"createdAt":"2017-11-03T10:00:54+01:00",' +
                '"updatedAt":"2017-11-03T10:02:05+01:00",' +
                '"executedAt":null,' +
                '"member":null,' +
                '"revision":"/file_revisions/979334de-80dd-4632-8438-bd23ce43b7ee"' +
                '}' +
                '],' +
                '"procedure":null,' +
                '"comment":null,' +
                '"signature":null,' +
                '"proof":null,' +
                '"comments":[],' +
                '"relatedFiles":null' +
                '}' +
                '],' +
                '"files":[  ' +
                '{  ' +
                '"id":"/files/0acc0fb2-1342-480c-90ff-ddcb9b808182",' +
                '"name":"testYousignFile",' +
                '"type":"signable",' +
                '"contentType":"application/pdf",' +
                '"password":null,' +
                '"description":"",' +
                '"createdAt":"2017-10-02T09:27:18+02:00",' +
                '"updatedAt":"2017-11-03T10:00:54+01:00",' +
                '"sha256":null,' +
                '"metadata":[],' +
                '"company":"/companies/b97408ca-496b-450e-a111-b0f0560f5d31",' +
                '"creator":null,' +
                '"fileObjects":null' +
                '}' +
                '],' +
                '"relatedFilesEnable":false' +
                '},' +
                '"member":{' +
                '"id":"46c503db-2ae4-4a3c-92c2-f66a8819f1bc",' +
                '"user":null,' +
                '"type":"signer",' +
                '"firstname":"Roman",' +
                '"lastname":"Prokopenko",' +
                '"email":"prokopenkor5@gmail.com",' +
                '"phone":"+380661145648",' +
                '"position":1,' +
                '"createdAt":"2017-11-03T10:00:54+01:00",' +
                '"updatedAt":"2017-11-03T10:02:05+01:00",' +
                '"status":"done",' +
                '"fileObjects":[  ' +
                '{  ' +
                ' "id":"/file_objects/55ff6e46-26cd-4064-aa7c-f30e24c915ff",' +
                '"file":{  ' +
                '"id":"/files/0acc0fb2-1342-480c-90ff-ddcb9b808182",' +
                '"name":"testYousignFile",' +
                '"type":"signable",' +
                '"contentType":"application/pdf",' +
                '"password":null,' +
                '"description":"",' +
                '"createdAt":"2017-10-02T09:27:18+02:00",' +
                '"updatedAt":"2017-11-03T10:00:54+01:00",' +
                '"sha256":null,' +
                '"metadata":[],' +
                '"company":"/companies/b97408ca-496b-450e-a111-b0f0560f5d31",' +
                '"creator":null,' +
                '"fileObjects":null' +
                '},' +
                '"page":1,' +
                '"position":"",' +
                '"fieldName":"",' +
                '"mention":"",' +
                '"mention2":null,' +
                '"createdAt":"2017-11-03T10:00:54+01:00",' +
                '"updatedAt":"2017-11-03T10:02:05+01:00",' +
                '"executedAt":null,' +
                '"member":null,' +
                '"revision":"/file_revisions/979334de-80dd-4632-8438-bd23ce43b7ee"' +
                '}' +
                '],' +
                '"procedure":null,' +
                '"comment":null,' +
                '"signature":null,' +
                '"proof":null,' +
                '"comments":[],' +
                '"relatedFiles":null' +
                '},' +
                '"comment":null,' +
                '"eventName":"' + eventName + '"' +
                '}';
    }
    private static Yousign_Submitted_Document__c createSubmittedDocument(Id procedureId, Id contentDocumentId) {
        Yousign_Submitted_Document__c submittedDocument = makeSubmittedDocument(procedureId, contentDocumentId);
        insert submittedDocument;
        return submittedDocument;
    }

    private static Yousign_Submitted_Document__c makeSubmittedDocument(Id procedureId, Id contentDocumentId) {
        Yousign_Submitted_Document__c submittedDocument = YousignTestDataFactory.makeYousignSubmittedDocument(0, procedureId, contentDocumentId);
        submittedDocument.Status__c = 'Pending';
        submittedDocument.type__c ='signable';
        submittedDocument.Yousign_Document_Id__c = '/files/externalId';
        return submittedDocument;
    }

}