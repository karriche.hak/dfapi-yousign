public with sharing class YousignDataModel {
    public virtual class ProcedureConfig implements IProcedureConfig {
        public Map<String, List<ConfigEmailTemplate>> email {get; set;}
        //public List<Map<String,Object>> reminders {get; set;}
        public List<ConfigReminder> reminders {get; set;}
        public Map<String, List<ConfigWebhookTemplate>> webhook {get; set;}


        public ProcedureConfig() {
            email = new Map<String, List<ConfigEmailTemplate>>();
            //reminders = new List<Map<String,Object>>();
            reminders = new List<ConfigReminder>();
            webhook = new Map<String, List<ConfigWebhookTemplate>>();
        }

        public void addReminder(ProcedureConfigReminder reminder, Integer setInervall , Integer setLimit) {
            String companyName;
            String nameSender;
            Yousign_setting__c settings = Yousign_setting__c.getOrgDefaults();
            Map<String,Object> newReminder = new Map<String, Object>();
            if (setInervall < 1) {
                return ;
            }else{
                newReminder.put('interval', setInervall);
            }
            if (setLimit < 1 || setLimit > 90) {
                return;
            }else{
                newReminder.put('limit', setLimit);
            }
            if (settings != null && !String.isEmpty(settings.Company_Name__c)) {
                companyName = settings.Company_Name__c;
            } else {
                companyName = UserInfo.getOrganizationName().capitalize();
            }
            if (settings != null && !String.isEmpty(settings.Sender_Name__c)) {
                nameSender = settings.Sender_Name__c;
            } else {
                nameSender = UserInfo.getFirstName().capitalize()+' '+UserInfo.getLastName().capitalize();
            }
            Map<String, Object> mEmail = reminder.setReminderExecutedEmailConfig(new List< YousignDataModel.ConfigEmailTemplate> {
                    new  YousignDataModel.ConfigEmailTemplate(
                            new String[] { '@members.auto' },
                            companyName + ' - Rappel - Vous avez une signature en attente !',
                            'Bonjour <tag data-tag-type=\"string\" data-tag-name=\"recipient.firstname\"></tag> <tag data-tag-type=\"string\" data-tag-name=\"recipient.lastname\"></tag>,<br /><br />' +
                                    'Vous n\'avez pas encore signé vos documents.<br />' +
                                    'Je vous invite à signer électroniquement vos documents en cliquant sur le bouton ci-dessous :<br /><br /><br />' +
                                    '{{ components.button(\\"Accéder aux documents\\", url) }}<br /><br />' +
                                    'Si vous avez la moindre question, je reste à votre disposition.<br /><br />' +
                                    'Cordialement,<br />'+nameSender+'<br />'+companyName,
                            null
                    )
            });


            newReminder.put('config',mEmail );
            //rg reminders.add(newReminder);
        }
        //add rg
    public void addConfigReminder(Integer setInervall , Integer setLimit) {
            String companyName;
            String nameSender;
            Yousign_setting__c settings = Yousign_setting__c.getOrgDefaults();
            ConfigReminder newReminder = new ConfigReminder();
            if (setInervall < 1) {
                return ;
            }else{
                newReminder.interval=setInervall;
            }
            if (setLimit < 1 || setLimit > 90) {
                return;
            }else{
                newReminder.limite=setLimit;
            }
            if (settings != null && !String.isEmpty(settings.Company_Name__c)) {
                companyName = settings.Company_Name__c;
            } else {
                companyName = UserInfo.getOrganizationName().capitalize();
            }
            if (settings != null && !String.isEmpty(settings.Sender_Name__c)) {
                nameSender = settings.Sender_Name__c;
            } else {
                nameSender = UserInfo.getFirstName().capitalize()+' '+UserInfo.getLastName().capitalize();
            }
            Map<String, List<ConfigEmailTemplate>> configReminderEmail=new Map<String, List<ConfigEmailTemplate>>();
            configReminderEmail.put(
                'reminder.executed',
                new List< YousignDataModel.ConfigEmailTemplate> {
                        new  YousignDataModel.ConfigEmailTemplate(
                            new String[] { '@members.auto' },
                            companyName + ' - Rappel - Vous avez une signature en attente !',
                            'Bonjour <tag data-tag-type=\"string\" data-tag-name=\"recipient.firstname\"></tag> <tag data-tag-type=\"string\" data-tag-name=\"recipient.lastname\"></tag>,<br /><br />' +
                                    'Vous n\'avez pas encore signé vos documents.<br />' +
                                    'Je vous invite à signer électroniquement vos documents en cliquant sur le bouton ci-dessous :<br /><br /><br />' +
                                    '{{ components.button(\\"Accéder aux documents\\", url) }}<br /><br />' +
                                    'Si vous avez la moindre question, je reste à votre disposition.<br /><br />' +
                                    'Cordialement,<br />'+nameSender+'<br />'+companyName,
                            null
                        )
                }
        );
        


            newReminder.config.put('email',configReminderEmail );
            reminders.add(newReminder);
        }
        
    //end rg
    }

    public class ConfigEmailTemplate {
        public List<String> to { get; set; }
        public String subject { get; set; }
        public String message { get; set; }
        public String fromName { get; set; }

        public ConfigEmailTemplate() {
            to = new List<String>();
        }
        public ConfigEmailTemplate(String[] targets) {
            to = new List<String>(targets);

        }


        public ConfigEmailTemplate(
                String[] targets, String title, String content, String fromName
        ) {
            to = new List<String>(targets);
            subject = title;
            message = content;
            this.fromName = fromName;
        }
    }

    public class ConfigWebhookTemplate {
        public String url { get; set; }
        public String method { get; set; }
        public Map<String, String> headers { get; set; }

        public ConfigWebhookTemplate() {
            headers = new Map<String, String>();
        }

        public ConfigWebhookTemplate(String url, String method, Map<String, String> headers) {
            if (headers != null) {
                this.headers = headers;
            } else {
                headers = new Map<String, String>();
            }
            this.url = url;
            this.method = method;
        }
    }

    public class ProcedureConfigReminder {

        public ProcedureConfigReminder() {
            // apply default values
            /*  reminder = new Map<String, Object> {
                      'interval' => 1,
                      'limit' => 5,
                      'config' => '{ "email": { "reminder.executed": [] } }'
              };*/
        }

        public ProcedureConfigReminder(String jsonString) {
            // config = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
        }

        public Map<String,Object> setReminderExecutedEmailConfig(List<ConfigEmailTemplate> configEmailTemplateList) {
            if (configEmailTemplateList == null || configEmailTemplateList.isEmpty()) {
                //reminder.put('config', '{ "email": { "reminder.executed": [] } }');
                return null;
            }
            Map<String, Object> mEmail = new Map<String, Object>();
            Map<String, Object> mReminder = new Map<String, Object>();
            List<Map<String, Object>> lStrings = new List<Map<String, Object>>();
            Map<String, Object> mStrings = new Map<String, Object>();
            String message =  configEmailTemplateList.get(0).message ;
            String subject = configEmailTemplateList.get(0).subject;
            List<String> to = configEmailTemplateList.get(0).to;
            mStrings.put('message',message);
            mStrings.put('subject',subject);
            mStrings.put('to',to);
            lStrings.add(mStrings);
            mReminder.put('reminder.executed',lStrings );
            mEmail.put('email',mReminder );

            return mEmail;
        }
    }
    
    //add by rg
    public class ConfigReminder {
        public Integer interval { get; set; }
        public Integer limite { get; set; }
        public Map<String,Map<String, List<ConfigEmailTemplate>>> config { get; set; }
        public ConfigReminder() {
            config = new Map<String,Map<String, List<ConfigEmailTemplate>>>();
        }

        public ConfigReminder(Integer interval, Integer limite, Map<String,Map<String, List<ConfigEmailTemplate>>> config) {
            if (config != null) {
                this.config = config;
            } else {
                config = new Map<String,Map<String, List<ConfigEmailTemplate>>>();
            }
            this.interval = interval;
            this.limite = limite;
        }
          
    }
    //end rg
    
}