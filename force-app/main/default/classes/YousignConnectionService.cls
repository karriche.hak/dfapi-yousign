public with sharing class YousignConnectionService {

    private final static Integer MAXIMUM_ONE_FILE_WEIGHT = 2621440;
    private final static Integer MAXIMUM_ALL_FILES_WEIGHT = 52428800;

    private final static String Yousign_One_File_Weight_Error_Message = Label.Yousign_One_File_Weight_Error_Message;
    private final static String Yousign_All_Files_Weight_Error_Message = Label.Yousign_All_Files_Weight_Error_Message;

    /**
     * SUBMISSION CREATION
     *
     *
     */
    public static void checkFilesWeight(Set<Id> contentDocumentIds) {
        Integer allFilesWeight = 0;
        List<Schema.SObjectField> contentDocumentFields = new List<Schema.SObjectField>{ContentDocument.Title, ContentDocument.ContentSize};
        DMLManager.FLSCheckResultKeeper contentDocumentFLSCheckResult =
                new DMLManager().getFLSCheckResult(contentDocumentFields, ContentDocument.getSObjectType());
        if (!contentDocumentFLSCheckResult.areAllSelectedFieldsAvailable) {
            throw new YousignConnectionException(contentDocumentFLSCheckResult.errorText);
        }
        List<ContentDocument> contentDocuments = [SELECT Title, ContentSize FROM ContentDocument WHERE id IN :contentDocumentIds];
        if (!contentDocuments.isEmpty()) {
            for (ContentDocument contentDocument : contentDocuments) {
                // Why this limit ? It seems to work with more.
                // if (contentDocument.ContentSize < MAXIMUM_ONE_FILE_WEIGHT) {
                allFilesWeight += contentDocument.ContentSize;
                // } else {
                //     throw new YousignConnectionException(contentDocument.Title + Yousign_One_File_Weight_Error_Message);
                // }
            }
            if (allFilesWeight > MAXIMUM_ALL_FILES_WEIGHT ) {
                throw new YousignConnectionException(Yousign_All_Files_Weight_Error_Message);
            }
        }
    }

    public static void checkFilesWeight(List<Yousign_Submitted_Document__c> selectedDocuments) {
        Set<Id> contentDocumentIds = new Set<Id>();
        for (Yousign_Submitted_Document__c selectedDocument : selectedDocuments) {
            contentDocumentIds.add((Id)selectedDocument.Document_Id__c);
        }
        checkFilesWeight(contentDocumentIds);
    }

    public static Yousign_Procedure__c createYousignProcedure(Id objectId, Yousign_Procedure__c procedure,
            List<Yousign_Submitted_Document__c> submittedDocuments, List<Yousign_Signatory__c> signatories,
            String status, Datetime dateOfSubmission) {

        Savepoint procedureCreationSavepoint = Database.setSavepoint();
        try {
            procedure = createYousignProcedure(procedure, objectId, status, dateOfSubmission);
            signatories = createYousignSignatories(signatories, procedure.Id, procedure.Ordered__c, status, 'Signatory');
            submittedDocuments = createYousignSubmittedDocuments(submittedDocuments, procedure.Id);
            return procedure;
        } catch (DmlException e) {
            Database.rollback(procedureCreationSavepoint);
            throw e;
        }
    }

    private static Yousign_Procedure__c makeYousignProcedure(Yousign_Procedure__c procedure, Id objectId,
            String status, Datetime dateOfSubmission) {

        String sobjectType = objectId.getSObjectType().getDescribe().getName();
        List<Schema.SObjectField> fieldList = Schema.SObjectType.Yousign_Procedure__c.fields.getMap().values();
        for (Integer i=0;i<fieldList.size();i++){
            String typeField = fieldList[i].getDescribe().getType() + '';
            if(typeField == 'REFERENCE'){
                List<Schema.SObjectType> fieldReference = fieldList[i].getDescribe().getReferenceTo();
                String fieldRef = fieldReference[0].getDescribe().getName();
                if (sobjectType == fieldRef){
                    System.debug('Associate '+fieldList[i]+' to '+objectId);
                    procedure.put(fieldList[i],objectId);
                }
            }
        }
        procedure.ObjectReference__c = objectId;
        procedure.Status__c = status;
        procedure.Signing_process_applicant__c = UserInfo.getUserId();
        procedure.Date_of_submission__c = dateOfSubmission;
        procedure.Date_of_cancellation__c = null;

        return procedure;
    }

    private static Yousign_Procedure__c createYousignProcedure(Yousign_Procedure__c procedure, Id objectId,
            String status, Datetime dateOfSubmission) {

        procedure = makeYousignProcedure(procedure, objectId, status, dateOfSubmission);
        new DMLManager().upsertAsUser(procedure);

        return procedure;
    }

    private static Yousign_Signatory__c makeYousignSignatory(Yousign_Signatory__c signatory, Id yousignProcedureId,
            Boolean ordered, String status, String role) {

        if (signatory.Yousign_Procedure__c == null) signatory.Yousign_Procedure__c = yousignProcedureId;
        signatory.Status__c = status;
        if (!ordered) signatory.Order__c = null;
        signatory.Role__c = role;

        return signatory;
    }

    private static List<Yousign_Signatory__c> createYousignSignatories(List<Yousign_Signatory__c> signatories,
            Id yousignProcedureId, Boolean ordered, String status, String role) {

        for (Yousign_Signatory__c signatory : signatories) {
            makeYousignSignatory(signatory, yousignProcedureId, ordered, status, role);
        }
        List<Yousign_Signatory__c> signatoriesWithoutExt = splitFilterYousignSignatory(signatories,'noExt');
        new DMLManager().upsertAsUser(signatoriesWithoutExt);

        return signatories;
    }

    private static Yousign_Submitted_Document__c makeYousignSubmittedDocument(
            Yousign_Submitted_Document__c submittedDocument, Id yousignProcedureId) {

        if (submittedDocument.Yousign_Procedure__c == null) submittedDocument.Yousign_Procedure__c = yousignProcedureId;

        return submittedDocument;
    }

    private static Yousign_Submitted_Document__c createYousignSubmittedDocument(
            Yousign_Submitted_Document__c submittedDocument, Id yousignProcedureId) {

        submittedDocument = makeYousignSubmittedDocument(submittedDocument, yousignProcedureId);
        new DMLManager().upsertAsUser(submittedDocument);

        return submittedDocument;
    }

    private static List<Yousign_Submitted_Document__c> createYousignSubmittedDocuments(
            List<Yousign_Submitted_Document__c> submittedDocuments, Id yousignProcedureId) {

        for (Yousign_Submitted_Document__c submittedDocument : submittedDocuments) {
            makeYousignSubmittedDocument(submittedDocument, yousignProcedureId);
        }
        new DMLManager().upsertAsUser(submittedDocuments);

        return submittedDocuments;
    }

    public static Yousign_Procedure__c cancelProcedure(Yousign_Procedure__c yousignProcedure,
            List<Yousign_Signatory__c> signatories, List<Yousign_Submitted_Document__c> submittedDocuments) {

        Savepoint procedureCreationSavepoint = Database.setSavepoint();
        try {
            yousignProcedure = cancelProcedure(yousignProcedure);
            signatories = cancelSignatories(signatories);
            submittedDocuments = cancelDocuments(submittedDocuments);

            return yousignProcedure;
        } catch (DmlException e) {
            Database.rollback(procedureCreationSavepoint);
            throw e;
        }
    }

    private static Yousign_Procedure__c cancelProcedure(Yousign_Procedure__c yousignProcedure) {
        yousignProcedure.Status__c = 'Cancelled';
        yousignProcedure.Signing_process_applicant__c = UserInfo.getUserId();
        yousignProcedure.Date_of_cancellation__c = DateTime.now().addSeconds(UserInfo.getTimeZone().getOffset(DateTime.now()) / 1000);
        yousignProcedure.Yousign_Procedure_Id__c = null;
        yousignProcedure.Response__c = null;
        new DMLManager().updateAsUser(yousignProcedure);

        return yousignProcedure;
    }

    private static List<Yousign_Signatory__c> cancelSignatories(List<Yousign_Signatory__c> signatories) {
        List<Yousign_Signatory__c> signatoriesToUpdate = new List<Yousign_Signatory__c>();
        for (Yousign_Signatory__c signatory : signatories) {
            signatory.Status__c = 'Cancelled';
            signatory.Yousign_Signatory_Id__c = null;
            if (signatory.Id != null) {
                signatoriesToUpdate.add(signatory);
            }
        }
        new DMLManager().updateAsUser(signatoriesToUpdate);

        return signatories;
    }

    private static List<Yousign_Submitted_Document__c> cancelDocuments(List<Yousign_Submitted_Document__c> documents) {
        List<Yousign_Submitted_Document__c> documentsToUpdate = new List<Yousign_Submitted_Document__c>();
        for (Yousign_Submitted_Document__c document : documents) {
            document.Status__c = null;
            document.Response__c = null;
            document.Error_Message__c = null;
            document.Yousign_Document_Id__c = null;
            if (document.Id != null) {
                documentsToUpdate.add(document);
            }
        }
        new DMLManager().updateAsUser(documentsToUpdate);

        return documents;
    }

    /**
     * SUBMISSION SENDING
     *
     *
     */

    public static void loadFile(YousignApiService service, YousignApiService.FileSubmission fileToInsert) {
        service.createFile(fileToInsert);
        YousignConnectionService.proceedResult(fileToInsert);
    }

    public static void sendSubmission(YousignApiService service, YousignApiService.ProcedureSubmission submission) {
        service.createProcedure(submission);
        YousignConnectionService.proceedResult(submission);
    }

    public static void duplicateProcedure(YousignApiService service, YousignApiService.DuplicateSubmission submission) {
        service.duplicateProcedure(submission);
        YousignConnectionService.proceedResult(submission);
    }

    public static void sendCancelSubmission(YousignApiService service, YousignApiService.CancellationSubmission submission) {
        service.cancelProcedure(submission);
        YousignConnectionService.proceedResult(submission);
    }

    public static void downloadFile(YousignApiService service, YousignApiService.FileDownloadSubmission submission) {
        service.loadFile(submission);
        YousignConnectionService.proceedResult(submission);
    }

    public static List<ContentDocumentLink> getContentDocumentLinks(Id objectId) {
        DMLManager dmlManager = new DMLManager();
        List<Schema.SObjectField> contentDocumentLinkFields = new List<Schema.SObjectField>{ContentDocumentLink.ContentDocumentId};
        DMLManager.FLSCheckResultKeeper contentDocumentLinkFLSCheckResult =
                        dmlManager.getFLSCheckResult(contentDocumentLinkFields, ContentDocumentLink.getSObjectType());
        List<Schema.SObjectField> contentDocumentFields = new List<Schema.SObjectField>{ContentDocument.Title};
        DMLManager.FLSCheckResultKeeper contentDocumentFLSCheckResult =
                        dmlManager.getFLSCheckResult(contentDocumentFields, ContentDocument.getSObjectType());
        if (!contentDocumentLinkFLSCheckResult.areAllSelectedFieldsAvailable) {
            throw new YousignConnectionException(contentDocumentLinkFLSCheckResult.errorText);
        }
        if (!contentDocumentFLSCheckResult.areAllSelectedFieldsAvailable) {
            throw new YousignConnectionException(contentDocumentFLSCheckResult.errorText);
        }
        List<ContentDocumentLink> contentDocumentLinks = [
                SELECT Id
                        , ContentDocumentId
                        , ContentDocument.Title
                FROM ContentDocumentLink
                WHERE LinkedEntityId = :objectId AND ContentDocument.FileType = :Test.isRunningTest()?'UNKNOWN':'PDF'
                ORDER BY SystemModstamp DESC
        ];

        return contentDocumentLinks;
    }

    public static ContentVersion getLatestContentVersion(Id contentDocumentId) {
        List<Schema.SObjectField> contentVersionFields = new List<Schema.SObjectField>{ContentVersion.Title,
                ContentVersion.VersionData, ContentVersion.Description, ContentVersion.ContentDocumentId,
                ContentVersion.ContentUrl, ContentVersion.ContentSize, ContentVersion.FileExtension};
        DMLManager.FLSCheckResultKeeper contentVersionFLSCheckResult =
                        new DMLManager().getFLSCheckResult(contentVersionFields, ContentVersion.getSObjectType());

        if (!contentVersionFLSCheckResult.areAllSelectedFieldsAvailable) {
            throw new YousignConnectionException(contentVersionFLSCheckResult.errorText);
        }
        List<ContentDocument> contentDocument = [
                SELECT Id
                    , LatestPublishedVersionId
                FROM ContentDocument
                WHERE Id = :contentDocumentId
                Limit 1
        ];
        System.debug('contentDocument = '+contentDocument.get(0));

        ID contentVersionId = contentDocument.get(0).LatestPublishedVersionId;
        List<ContentVersion> contentVersions = [
                SELECT Id,
                    Title,
                    VersionData,
                    Description,
                    ContentDocumentId,
                    ContentUrl,
                    ContentSize,
                    FileExtension
                FROM ContentVersion
                WHERE Id = :contentVersionId
        ];
        System.debug('contentVersions = '+contentVersions.get(0));

        return contentVersions.isEmpty() ? null : contentVersions.get(0);
    }

    public static Map<Id, ContentVersion> getLatestContentVersions(Set<Id> contentDocumentIds) {
        List<Schema.SObjectField> contentVersionFields = new List<Schema.SObjectField>{ContentVersion.Title,
                ContentVersion.VersionData, ContentVersion.Description, ContentVersion.ContentDocumentId,
                ContentVersion.ContentUrl, ContentVersion.ContentSize, ContentVersion.FileExtension};
        DMLManager.FLSCheckResultKeeper contentVersionFLSCheckResult =
                        new DMLManager().getFLSCheckResult(contentVersionFields, ContentVersion.getSObjectType());

        if (!contentVersionFLSCheckResult.areAllSelectedFieldsAvailable) {
            throw new YousignConnectionException(contentVersionFLSCheckResult.errorText);
        }

        Map<Id, ContentVersion> result = new Map<Id, ContentVersion>();

        for (ContentVersion cVersion : [
                SELECT Id,
                    Title,
                    VersionData,
                    Description,
                    ContentDocumentId,
                    ContentUrl,
                    ContentSize,
                    FileExtension
                FROM ContentVersion
                WHERE ContentDocumentId IN :contentDocumentIds
                AND IsLatest = true
        ]) {
            result.put(
                cVersion.ContentDocumentId,
                cVersion
            );
        }

        return result;
    }

    private static void proceedResult(YousignApiService.Submission submission) {
        System.debug(submission);
        if (!submission.isSuccess()) {
            throw new YousignConnectionException(submission.getStatus());
        }
    }

    public class YousignConnectionException extends Exception {}

    public static List<Yousign_Signatory__c> splitFilterYousignSignatory(List<Yousign_Signatory__c> signatory, String choice){
        List<Yousign_Signatory__c> result = new List<Yousign_Signatory__c>();
        List<Yousign_Signatory__c> externalSelectedSignatories = new List<Yousign_Signatory__c>();
        List<Yousign_Signatory__c> selectedSignatories = signatory.clone();
        Integer lengthSelectedSignatories = selectedSignatories.size();
        for(Integer i=lengthSelectedSignatories-1;i>=0;i--){
            if (((String)selectedSignatories[i].Contact__c).contains('externe')){
                externalSelectedSignatories.add(selectedSignatories[i]);
                selectedSignatories.remove(i);
            }
        }
        if (choice == 'Ext') result = externalSelectedSignatories;
        if (choice == 'noExt') result = selectedSignatories;
        return result;
    }
}