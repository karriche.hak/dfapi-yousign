public without sharing class YousignFileLoader implements Queueable, Database.AllowsCallouts {
    private Yousign_Procedure__c yousignProcedure;
    private transient YousignApiService service;
    private Static String Yousign_Received_file_is_empty = Label.Yousign_Received_file_is_empty;

    public YousignFileLoader(Yousign_Procedure__c yousignProcedure) {
        this.yousignProcedure = yousignProcedure;
    }

    public void execute(QueueableContext context) {
     
        try {
            this.service = getEnvironment();
            List<Yousign_Submitted_Document__c> nonReceivedDocuments = this.getNonReceivedDocuments();
            
            if (nonReceivedDocuments.isEmpty()) return;
          
            this.retrieveDocument(nonReceivedDocuments.get(0));
        } catch (Exception e) {
          
            this.yousignProcedure.Error_Message__c = e.getMessage();
            new DMLManager().updateAsUser(this.yousignProcedure);
        }
    }

    private List<Yousign_Submitted_Document__c> getNonReceivedDocuments() {
        List<Schema.SObjectField> yousignSubmittedDocumentFields = new List<Schema.SObjectField>{Yousign_Submitted_Document__c.Yousign_Document_Id__c, Yousign_Submitted_Document__c.Document_Id__c, Yousign_Submitted_Document__c.Yousign_Procedure__c};
        DMLManager.FLSCheckResultKeeper yousignSignatoryFLSCheckResult = new DMLManager().getFLSCheckResult(yousignSubmittedDocumentFields , Yousign_Submitted_Document__c.getSObjectType());
        if (!yousignSignatoryFLSCheckResult.areAllSelectedFieldsAvailable) {
            throw new YousignFileLoaderException(yousignSignatoryFLSCheckResult.errorText);
        }
        List<Yousign_Submitted_Document__c> nonReceivedDocuments = [
                SELECT Id
                    , Yousign_Document_Id__c
                    , ysign__Document_Name__c
                    , Document_Id__c
                    , Yousign_Procedure__c
                FROM Yousign_Submitted_Document__c
                WHERE Yousign_Procedure__c = :yousignProcedure.Id
                    AND Status__c != 'Received'
                    AND Status__c != 'Failed'
                    AND type__c ='signable'
        ];

        if (nonReceivedDocuments.size()==0){
                  //create document
                  
        }

        return nonReceivedDocuments;
    }

    private void retrieveDocument(Yousign_Submitted_Document__c submittedDocument) {
        YousignApiService.FileDownloadSubmission downloadSubmission = new YousignApiService.FileDownloadSubmission(submittedDocument);
        this.downloadFile(submittedDocument, downloadSubmission);
        //new DMLManager().updateAsUser(submittedDocument);
        update submittedDocument;
        if (!Test.isRunningTest()) System.enqueueJob(new YousignFileLoader(this.yousignProcedure));
    }

    private void downloadFile(Yousign_Submitted_Document__c submittedDocument,
                                        YousignApiService.FileDownloadSubmission downloadSubmission) {
        try {
            YousignConnectionService.downloadFile(this.service, downloadSubmission);
            ContentVersionCreator creator = new ContentVersionCreator();
            creator.createNewContentVersion(downloadSubmission, submittedDocument);
            submittedDocument.Status__c = 'Received';
        } catch (Exception e) {
            submittedDocument.Status__c = 'Failed';
            
            submittedDocument.Error_Message__c = e.getMessage();
        }
    }

    private without sharing class ContentVersionCreator {

        private ContentVersion createNewContentVersion(YousignApiService.FileDownloadSubmission downloadSubmission,
        Yousign_Submitted_Document__c submiteddocument) {

            if (String.isEmpty(downloadSubmission.getEncodedFileData()))
                    throw new YousignFileLoaderException(Yousign_Received_file_is_empty);

            ContentVersion receivedFile = new ContentVersion();
            receivedFile.ContentDocumentId = submiteddocument.Document_Id__c;
            receivedFile.VersionData = EncodingUtil.base64Decode(downloadSubmission.getEncodedFileData());
            receivedFile.PathOnClient = 'signedFile.pdf';

            new DMLManager().insertAsUser(receivedFile);

            ContentDocument contentDoc=new ContentDocument();
            contentDoc.id=submiteddocument.Document_Id__c ;
            contentDoc.Title=submiteddocument.ysign__Document_Name__c+'- signed - '+System.now().format() ; 
            update contentDoc;
            return receivedFile;
        }
    }

    public static YousignApiService getEnvironment(){
        YousignApiService service;
        Yousign_setting__c settings = Yousign_setting__c.getOrgDefaults();
        if (settings != null && !String.isEmpty(settings.Environment__c)) {
                if (settings.Environment__c == 'PRODUCTION'){
                    service = new YousignApiService(YousignApiService.Environment.PRODUCTION);
                }else {
                    service = new YousignApiService(YousignApiService.Environment.STAGING);
                } 
        } else {
            service = new YousignApiService(YousignApiService.Environment.STAGING);
        }

        return service;
    }

    public class YousignFileLoaderException extends Exception {}
}