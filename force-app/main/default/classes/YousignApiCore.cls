public with sharing class YousignApiCore {

    private final YousignApiCaller apiCaller;
    private static final Map<YousignApiService.Environment, String> endpointPrefixMap;

    static {
        endpointPrefixMap = new Map<YousignApiService.Environment, String>{
                YousignApiService.Environment.PRODUCTION => '',
                YousignApiService.Environment.STAGING => 'staging-'
        };
    }

    /*===============================================================*/
    /* Public Interface */
    /*===============================================================*/
    public YousignApiCore(String apiKey, YousignApiService.Environment environment) {
        String endpointPrefix = endpointPrefixMap.get(environment);
        String yousignEndpoint = 'https://' + endpointPrefix + 'api.yousign.com';
        System.debug(LoggingLevel.ERROR, 'APIKEY : ' + apiKey);
        System.debug(LoggingLevel.ERROR, 'yousignEndpoint : ' + yousignEndpoint);
        this.apiCaller = new YousignApiCaller(apiKey, yousignEndpoint);
    }

    public void createProcedure(YousignApiService.ProcedureSubmission procedureSubmission){
        this.apiCaller.request(procedureSubmission);
    }

    public void updateProcedure(YousignApiService.ProcedureUpdate procedureSubmission){
        this.apiCaller.request(procedureSubmission);
    }

    public void getProcedure(YousignApiService.ProcedureReadSubmission procedureReadSubmission) {
        this.apiCaller.request(procedureReadSubmission);
    }

    public void duplicateProcedure(YousignApiService.DuplicateSubmission duplicateProcedure){
        this.apiCaller.request(duplicateProcedure);
    }

    public void updateMemberSubmission(YousignApiService.UpdateMemberSubmission updateMemberSubmission){
        this.apiCaller.request(updateMemberSubmission);
    }

    public void createMemberSubmission(YousignApiService.CreateMemberSubmission createMemberSubmission){
        this.apiCaller.request(createMemberSubmission);
    }

    public void getTemplates(YousignApiService.TemplateListSubmission templateSubmission){
        this.apiCaller.request(templateSubmission);
    }

    public void createFile(YousignApiService.FileSubmission fileSubmission) {
        this.apiCaller.request(fileSubmission);
    }

    public void cancelProcedure(YousignApiService.CancellationSubmission cancellationSubmission) {
        this.apiCaller.request(cancellationSubmission);
    }

    public void loadFile(YousignApiService.FileDownloadSubmission fileDownloadSubmission) {
        this.apiCaller.request(fileDownloadSubmission);
    }

    /*===============================================================*/
    /* Https executor */
    /*===============================================================*/
    private class YousignApiCaller {

        private final Http http;
        private final String endpoint;
        private final String apiKey;

        public YousignApiCaller(String apiKey, String yousignEndpoint) {
            this.http = new Http();
            this.apiKey = apiKey;
            this.endpoint = yousignEndpoint;
        }

        public HttpResponse request(YousignApiService.Submission submission) {
            try {
                HttpRequest request = this.makeReuqest(submission);
                HttpResponse response = this.http.send(request);
                submission.setResponse(response);
                return response;
            } catch (Exception ex) {
                System.debug(LoggingLevel.ERROR, ex);
                throw new YousignConnectionService.YousignConnectionException(ex.getMessage());
            }
            return null;
        }
        
        private HttpRequest makeReuqest(YousignApiService.Submission submission) {
            String userName = UserInfo.getUserName();
            User activeUser = [
                SELECT Email,ysign__WorkspaceName__c,ysign__WorkspaceId__c
                FROM User
                WHERE Username = :userName
                LIMIT 1
            ];
            HttpRequest request = new HttpRequest();
            request.setEndpoint(this.endpoint + submission.endpointPostfix);
            request.setMethod(submission.requestMethod);
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Authorization', 'Bearer ' + this.apiKey);
            if(activeUser.ysign__WorkspaceId__c!=null && !String.isEmpty(activeUser.ysign__WorkspaceId__c)){
                request.setHeader('X-Yousign-Workspace', activeUser.ysign__WorkspaceId__c);
            }
            String body = submission.getRequestBody();
            
            if (!String.isEmpty(body)) {
                if (body.contains('"limite" :')) body=body.replaceAll('"limite" :','"limit" :');
                request.setBody(body);
            }
            return request;
        }
    }
}