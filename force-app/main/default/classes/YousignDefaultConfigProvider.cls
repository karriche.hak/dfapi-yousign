public with sharing class YousignDefaultConfigProvider extends  YousignDataModel.ProcedureConfig {
    private transient String siteUrl;
    private transient String companyName;
    private transient String nameSender;
    private transient String procedureURL;

    public YousignDefaultConfigProvider(String procedureId) {
        siteUrl = getPublicSiteUrl();

        List<Yousign_Procedure__c> procedures = [SELECT Id, ReminderNbDays__c,Template_id__c FROM Yousign_Procedure__c WHERE Id = :procedureId ];

        if (procedures.isEmpty()) {
            return;
        }

        String reminderNbDays = procedures[0].ReminderNbDays__c;
        loadSettings(procedures[0].Id);
        //add by Rg
        if (procedures[0].Template_id__c!=null && procedures[0].Template_id__c!=''){
            initializesEmailsReminderTemplate(procedures[0].Template_id__c);
        }else {
            initializeEmails();
            initializeReminder(reminderNbDays);
        }
        //end RG
        // rg initializeEmails();
        // rg initializeReminder(reminderNbDays);
        initializeWebhooks();
    }

    private void loadSettings(String procedureId) {
        Yousign_setting__c settings = Yousign_setting__c.getOrgDefaults();
        if (settings != null && !String.isEmpty(settings.Company_Name__c)) {
            companyName = settings.Company_Name__c;
        } else {
            companyName = UserInfo.getOrganizationName().capitalize();
        }
        if (settings != null && !String.isEmpty(settings.Sender_Name__c)) {
            nameSender = settings.Sender_Name__c;
        } else {
            nameSender = UserInfo.getFirstName().capitalize()+' '+UserInfo.getLastName().capitalize();
        }

        CurrentTheme__c currentThemeObject = CurrentTheme__c.getInstance(UserInfo.getUserId());
        String currentTheme = currentThemeObject.theme__c;
        if (procedureId == null) {
            procedureId = '';
        }
        procedureURL = '';
        if (currentTheme == 'lightning'){
            procedureURL = URL.getSalesforceBaseUrl().toExternalForm()+'/lightning/r/Yousign_Procedure__c/'+procedureId+'/view';
        } else {
            procedureURL = URL.getSalesforceBaseUrl().toExternalForm()+'/'+procedureId;
        }
    }
    //add by RG
    private void initializesEmailsReminderTemplate(String TemplateId) {
        YousignApiService yss = new YousignApiService();
        YousignApiService.TemplateListSubmission ts = new YousignApiService.TemplateListSubmission();
        yss.getTemplates(ts);
        for (YousignApiModel.ProcedureOutput po : ts.listProcedureOutput) {
            if (po.id==TemplateId){
                email=po.config.email ;
                reminders=po.config.reminders;
            }
        }
    }
    // end RG    
    private void initializeEmails() {
        // * Procedure de signature refusé
        email.put(
                'procedure.refused',
                new List< YousignDataModel.ConfigEmailTemplate> {
                        new  YousignDataModel.ConfigEmailTemplate(
                                new String[] { '@creator' },
                                companyName + ' - Une procédure a été refusée',
                                'Bonjour <tag data-tag-type=\"string\" data-tag-name=\"recipient.firstname\"></tag> <tag data-tag-type=\"string\" data-tag-name=\"recipient.lastname\"></tag>,<br /><br />' +
                                        'Une procédure de signature a été refusée,<br />' +
                                        'si besoin, vous pouvez consulter le détail des documents en cliquant sur le bouton suivant :<br /><br /><br />' +
                                        '<tag data-tag-type=\"button\" data-tag-name=\"url\" data-tag-title=\"Accéder aux documents\">Accéder aux documents</tag><br /><br />' +
                                        'Cordialement,<br />' + companyName,
                                null
                        ),
                        new  YousignDataModel.ConfigEmailTemplate(
                                new String[] { '@members.auto' },
                                companyName + ' - Une procédure a été refusée',
                                'Bonjour <tag data-tag-type=\"string\" data-tag-name=\"recipient.firstname\"></tag> <tag data-tag-type=\"string\" data-tag-name=\"recipient.lastname\"></tag>,<br /><br />Une procédure de signature a été refusée,<br />' +
                                        'si besoin, vous pouvez consulter le détail des documents en cliquant sur le bouton suivant :<br /><br /><br />' +
                                        '<tag data-tag-type=\"button\" data-tag-name=\"url\" data-tag-title=\"Accéder aux documents\">Accéder aux documents</tag><br /><br />' +
                                        'Cordialement,<br />'+companyName,
                                null
                        )
                }
        );
        // * Procedure de signature fini
        email.put(
                'procedure.finished',
                new List< YousignDataModel.ConfigEmailTemplate> {
                        new  YousignDataModel.ConfigEmailTemplate(
                                new String[] { '@creator' },
                                companyName + ' - La procédure de signature est terminée !',
                                'Bonjour <tag data-tag-type=\"string\" data-tag-name=\"recipient.firstname\"></tag> <tag data-tag-type=\"string\" data-tag-name=\"recipient.lastname\"></tag>,<br /><br />Vos signataires ont terminé la procédure,<br />' +
                                        'vous pouvez suivre son état via le lien suivant :<br /><br /><br />' +
                                        '<tag data-tag-type=\"button\" data-tag-name=\"url\" data-tag-title=\"Accéder à la procédure\">Accéder à la procédure</tag><br /><br />' +
                                        'Cordialement,<br />'+companyName,
                                null
                        ),
                        new  YousignDataModel.ConfigEmailTemplate(
                                new String[] { '@members.auto' },
                                companyName + ' - La procédure de signature est terminée !',
                                'Bonjour <tag data-tag-type=\"string\" data-tag-name=\"recipient.firstname\"></tag> <tag data-tag-type=\"string\" data-tag-name=\"recipient.lastname\"></tag>,<br /><br />' +
                                        'Félicitations ! Les documents ont bien été signés.<br />Vous pouvez y accéder et les télécharger via le lien suivant :<br /><br /><br />' +
                                        '<tag data-tag-type=\"button\" data-tag-name=\"url\" data-tag-title=\"Accéder aux documents signés\">Accéder aux documents signés</tag><br /><br />' +
                                        'Nous vous remercions pour votre confiance.<br /><br />' +
                                        'Cordialement,<br />'+nameSender+'<br />'+companyName,
                                null
                        )
                }
        );
        // * Procedure de signature commencé
        email.put(
                'member.started',
                new List< YousignDataModel.ConfigEmailTemplate> {
                        new  YousignDataModel.ConfigEmailTemplate(
                                new String[] { '@member' },
                                companyName + ' - Vous avez une signature en attente !',
                                'Bonjour <tag data-tag-type=\"string\" data-tag-name=\"recipient.firstname\"></tag> <tag data-tag-type=\"string\" data-tag-name=\"recipient.lastname\"></tag>,<br /><br />' +
                                        'Je vous invite à signer électroniquement vos documents en cliquant sur le bouton ci-dessous :<br /><br /><br />' +
                                        '<tag data-tag-type=\"button\" data-tag-name=\"url\" data-tag-title=\"Accéder aux documents\">Accéder aux documents</tag><br /><br />' +
                                        'Si vous avez la moindre question, je reste à votre disposition.<br /><br />' +
                                        'Cordialement,<br />'+nameSender+'<br />'+companyName,
                                null
                        )
                }
        );
        // * Procedure de signature fini par l'un des signataires
        // config.email.put(
        //     'member.finished',
        //     new List< YousignDataModel.ConfigEmailTemplate> {
        //         new  YousignDataModel.ConfigEmailTemplate(
        //             new String[] { '@creator' },
        //             'Un utilisateur a signé l\'une de vos procédures',
        //             'Bonjour,\\n\\n' +
        //             'Nous vous informons qu\'un signataire vient de signer les documents de la procédure accessible via le lien ci-dessous :\\n<br /><br />' +
        //             '{{ components.button(\\"Accéder aux documents\\", url) }}',
        //             null
        //         )
        //     }
        // );
        // * Procedure de signature commenté par l'un des signataires
        // config.email.put(
        //     'comment.created',
        //     new List< YousignDataModel.ConfigEmailTemplate> {
        //         new  YousignDataModel.ConfigEmailTemplate(
        //             new String[] { '@creator' },
        //             'Une procédure a été commentée',
        //             'Bonjour,<br /><br /> la procédure suivante a été commentée.<br /><br /><br />' +
        //             '{{ components.button(\\"Accéder aux documents\\", url) }}<br /><br />' +
        //             'En vous souhaitant une bonne journée.<br /><br />Cordialement',
        //             null
        //         )
        //     }
        // );
    }

    private void initializeReminder(String reminderNbDays) {
        // * Rappel de la procedure de signature non signé
        if (reminderNbDays != null) {
            YousignDataModel.ProcedureConfigReminder reminder = new  YousignDataModel.ProcedureConfigReminder();
            //addReminder(reminder,Integer.valueOf(reminderNbDays),3);
            addConfigReminder(Integer.valueOf(reminderNbDays),3);
            

        }
    }

    private /*Map<String,List<ysign.YousignDataModel.ConfigWebhookTemplate>>*/ void initializeWebhooks() {
        webhook.put(
                'procedure.started',
                new List< YousignDataModel.ConfigWebhookTemplate> {
                        new  YousignDataModel.ConfigWebhookTemplate(
                                siteUrl + 'services/apexrest/ysign/YousignProcedure/',
                                'POST',
                                null
                        )
                }
        );
        webhook.put(
                'procedure.finished',
                new List< YousignDataModel.ConfigWebhookTemplate> {
                        new  YousignDataModel.ConfigWebhookTemplate(
                                siteUrl + 'services/apexrest/ysign/YousignProcedure/',
                                'POST',
                                null
                        )
                }
        );
        webhook.put(
                'procedure.refused',
                new List< YousignDataModel.ConfigWebhookTemplate> {
                        new  YousignDataModel.ConfigWebhookTemplate(
                                siteUrl + 'services/apexrest/ysign/YousignProcedure/',
                                'POST',
                                null
                        )
                }
        );
        webhook.put(
                'member.finished',
                new List< YousignDataModel.ConfigWebhookTemplate> {
                        new  YousignDataModel.ConfigWebhookTemplate(
                                siteUrl + 'services/apexrest/ysign/YousignProcedure/',
                                'POST',
                                null
                        )
                }
        );

      //  return webhook;
    }

    private String getPublicSiteUrl() {
        Yousign_setting__c settings = Yousign_setting__c.getOrgDefaults();
        if (settings == null || String.isEmpty(settings.Public_Site_Link__c)) {
            throw new YousignApiModel.YousignConfigurationException(Label.Yousign_Public_Site_Link_Not_Specified);
        }
        String result = settings.Public_Site_Link__c;
        if (!result.endsWith('/')) {
            result = result + '/';
        }
        return result;
    }
    
    
}